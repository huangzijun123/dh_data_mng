/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/9/18 上午10:52
 ******************************************************************************/
package com.cetc32;


import com.cetc32.dh.DhApplication;
import com.cetc32.dh.common.bean.UserAreaPermision;
import com.cetc32.dh.entity.DataArea;
import com.cetc32.dh.entity.DataFile;
import com.cetc32.dh.entity.ZQArea;
import com.cetc32.dh.mybatis.CityGeomMapper;
import com.cetc32.dh.mybatis.DataAreaMapper;
import com.cetc32.dh.mybatis.DataFileMapper;
import com.cetc32.dh.mybatis.ZqAreaMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DhApplication.class ,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@WebAppConfiguration
public class DataAreaTest {
    //@Autowired
    CityGeomMapper cityGeomMapper;
    //@Autowired
    ZqAreaMapper zqAreaMapper;
    //@Autowired
    DataFileMapper dataFileMapper;
    //@Autowired
    DataAreaMapper dataAreaMapper;

    //@Test
    public void datafileList(){
        DataFile dataFile =new DataFile();
        UserAreaPermision user =new UserAreaPermision();
        dataFile.setMenuId(147);
        List<String > ids =new ArrayList<>();
        ids.add("150100");
        ids.add("150200");
        ids.add("150300");
        ids.add("150400");
        ids.add("150500");
        ids.add("150600");
        ids.add("150700");
        ids.add("150800");
        ids.add("152500");
        user.setAreaCode(ids);
        System.out.println(dataFileMapper.selectFilesHasPermission(0,100,dataFile,user,user.getPolygon()));
        System.out.println(dataFileMapper.countFilesHasPermission(dataFile,user,user.getPolygon()));
    }

    //@Test
    public void testZqArea(){
        List<String > ids =new ArrayList<>();
        ids .add("01");
        System.out.println("ZQAreas "+zqAreaMapper.selectAreaByZqId(ids));
    }
    //@Test
    public void insertArea(){
        /*DataArea dataArea =new DataArea(100000L,"110000",99L);
        List<DataArea > dataAreas =new ArrayList<>();
        dataAreas.add(dataArea);
        dataAreaMapper.insertArea(dataAreas);*/

    }
    //@Test
    public void TestSelectCityFromAreaCodes(){
        ArrayList<String> list =new ArrayList();
        list.add("110000");
        list.add("150100");
        list.add("131000");
        System.out.println(cityGeomMapper.selectCityInCodes(list));
    }

    //@Test
    public void TestSelectAeraCodeFromCities(){
        ArrayList<String> list =new ArrayList();
        list.add("驻马店市");
        list.add("上海市");
        list.add("深圳市");
        System.out.println(cityGeomMapper.selectCodesInArea(list));
    }
    @Test
    public void zhengzebiaodashi(){
        System.out.println(Pattern.matches("^[\\u4e00-\\u9fa5]{0,20}$","你1好"));
    }
}
