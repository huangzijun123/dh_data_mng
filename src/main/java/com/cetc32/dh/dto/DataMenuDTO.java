/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/

package com.cetc32.dh.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 数据管理编目节点扩展实体类
 *
 * @author: xiao
 * @version: 1.0
 * @date: 2020/10/14
 * 备注:无
 */
public class DataMenuDTO implements Serializable {

    /**
     * 名称
     */
    private String title;

    /**
     * 键
     */
    private String key;

    /**
     * 值
     */
    private String value;

    /**
     * 前端页面url
     */
    private String htmlUrl;

    /**
     * 前端页面图形标识
     */
    private String icon;

    /**
     * 编目是否可更改
     */
    private Boolean disabled;

    /**
     * 是否可以添加子节点
     */
    private Boolean addkids;

  /**
   * 菜单信息描述
   */
    private String discription;


    /**
     * 子编目节点
     */
    private List<DataMenuDTO> children;

    /**
     * 无参构造函数
     */
    public DataMenuDTO() {
    }

    /**
     * 获取名称
     *
     * @return title - 名称
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置名称
     *
     * @param title 名称
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取键
     *
     * @return key - 键
     */
    public String getKey() {
        return key;
    }

    /**
     * 设置键
     *
     * @param key 键
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * 获取值
     *
     * @return value - 值
     */
    public String getValue() {
        return value;
    }

    /**
     * 设置值
     *
     * @return value - 值
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 获取前端页面url
     *
     * @return htmlUrl - 前端页面url
     */
    public String getHtmlUrl() {
        return htmlUrl;
    }

    /**
     * 设置前端页面url
     *
     * @param htmlUrl 前端页面url
     */
    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    /**
     * 获取前端页面图形标识
     *
     * @return icon - 前端页面图形标识
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 设置前端页面图形标识
     *
     * @param icon 前端页面图形标识
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * 获取编目子节点
     *
     * @return - children
     */
    public List<DataMenuDTO> getChildren() {
        return children;
    }

    /**
     * 设置编目子节点
     *
     * @param children
     */
    public void setChildren(List<DataMenuDTO> children) {
        this.children = children;
    }

    /**
     * 获取前端编目可更改信息
     *
     * @return icon - 前端编目更改信息
     */
    public Boolean getDisabled() {
        return disabled;
    }

    /**
     * 设置前端编目更改信息
     *
     * @param disabled 前端页编目更改信息
     */
    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    /**
     * 获取前端编目可更改信息
     *
     * @return icon - 前端编目更改信息
     */
    public Boolean getAddkids() {
        return addkids;
    }

    /**
     * 设置前端编目更改信息
     *
     * @param addkids 前端页编目更改信息
     */
    public void setAddkids(Boolean addkids) {
        this.addkids = addkids;
    }


  public String getDiscription() {
    return discription;
  }

  public void setDiscription(String discription) {
    this.discription = discription;
  }
}
