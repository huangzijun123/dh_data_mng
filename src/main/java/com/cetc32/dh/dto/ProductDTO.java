package com.cetc32.dh.dto;

import com.cetc32.dh.entity.Productdemand;
import com.alibaba.fastjson.JSONObject;
import springfox.documentation.spring.web.json.Json;

import java.util.List;

public class ProductDTO extends Productdemand {
    private String username;

    private Integer userid;

    private List<String> taskdocuments;

    public String demanddata;

    private JSONObject flow;

    public String getDemanddata() {
        return demanddata;
    }

    public void setDemanddata(String demanddata) {
        this.demanddata = demanddata;
    }

    public List<String> getTaskdocuments() {
        return taskdocuments;
    }

    public void setTaskdocuments(List<String> taskdocuments) {
        this.taskdocuments = taskdocuments;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public JSONObject getFlow() {
        return flow;
    }

    public void setFlow(JSONObject flow) {
        this.flow = flow;
    }
}
