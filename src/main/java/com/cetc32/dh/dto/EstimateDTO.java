package com.cetc32.dh.dto;

import com.cetc32.dh.entity.EstimateTask;

public class EstimateDTO extends EstimateTask {
    private String username;

    private Integer userid;
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }
}
