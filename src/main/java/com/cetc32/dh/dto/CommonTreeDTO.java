package com.cetc32.dh.dto;

import java.util.List;

public class CommonTreeDTO<K,V> {
    private List<CommonTreeDTO> children;
    private String title;
    private V value;
    private K key;

    public CommonTreeDTO(String title, V value, K key) {
        this.title = title;
        this.value = value;
        this.key = key;
    }

    public CommonTreeDTO(List<CommonTreeDTO> children, String title, V value, K key) {
        this.children = children;
        this.title = title;
        this.value = value;
        this.key = key;
    }

    public CommonTreeDTO(){}

    public List<CommonTreeDTO> getChildren() {
        return children;
    }

    public void setChildren(List<CommonTreeDTO> children) {
        this.children = children;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }
}
