package com.cetc32.dh.dto;


import java.util.List;

public class AreaCommonDTO {

    private List<AreaCommonDTO> children;
    private String title;
    private String value;
    private String key;

    public AreaCommonDTO(){}

    public List<AreaCommonDTO> getChildren() {
        return children;
    }

    public void setChildren(List<AreaCommonDTO> children) {
        this.children = children;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
