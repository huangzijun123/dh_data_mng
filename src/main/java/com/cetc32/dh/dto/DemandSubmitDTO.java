package com.cetc32.dh.dto;

import com.cetc32.dh.entity.NumberS;
import org.apache.commons.lang3.StringUtils;

public class DemandSubmitDTO extends NumberS {
    private Integer id;

    private String deamndName;

    private String demandStatus;

    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeamndName() {
        return deamndName;
    }

    public void setDeamndName(String deamndName) {
        this.deamndName = deamndName;
    }

    public String getDemandStatus() {
        return demandStatus;
    }

    public void setDemandStatus(String demandStatus) {
        this.demandStatus = demandStatus;
    }
}
