package com.cetc32.dh.dto;

import com.cetc32.dh.entity.DemandSubmit;

public class DemandDTO extends DemandSubmit {
    private String username;

    private Integer userid;

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
