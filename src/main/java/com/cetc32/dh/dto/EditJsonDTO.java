package com.cetc32.dh.dto;

import com.alibaba.fastjson.JSONObject;
import io.netty.handler.codec.json.JsonObjectDecoder;

public class EditJsonDTO {
    private Integer id;
    private JSONObject editOne;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public JSONObject getEditOne() {
        return editOne;
    }

    public void setEditOne(JSONObject editOne) {
        this.editOne = editOne;
    }
}
