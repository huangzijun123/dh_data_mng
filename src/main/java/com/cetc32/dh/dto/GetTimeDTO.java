package com.cetc32.dh.dto;

import java.sql.Timestamp;

public class GetTimeDTO {
    String starttimeget;
    String endtimeget;
    Timestamp starttime;
    Timestamp endtime;

    public String getStarttimeget() {
        return starttimeget;
    }

    public String getEndtimeget() {
        return endtimeget;
    }

    public Timestamp getStarttime() {
        return starttime;
    }

    public Timestamp getEndtime() {
        return endtime;
    }

    public void setStarttimeget(String starttimeget) {
        this.starttimeget = starttimeget;
    }

    public void setEndtimeget(String endtimeget) {
        this.endtimeget = endtimeget;
    }

    public void setStarttime(Timestamp starttime) {
        this.starttime = starttime;
    }

    public void setEndtime(Timestamp endtime) {
        this.endtime = endtime;
    }
}
