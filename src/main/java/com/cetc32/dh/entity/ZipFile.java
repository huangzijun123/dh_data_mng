package com.cetc32.dh.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "zip_file")
public class ZipFile {
    /**
     * 压缩文件id
     */
    @Id
    private Integer id;

    /**
     * 压缩文件名字
     */
    @Column(name = "zip_name")
    private String zipName;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 压缩路径
     */
    @Column(name = "zip_path")
    private String zipPath;

    /**
     * 压缩日期
     */
    @Column(name = "zip_date")
    private Date zipDate;

    /**
     * 获取压缩文件id
     *
     * @return id - 压缩文件id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置压缩文件id
     *
     * @param id 压缩文件id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取压缩文件名字
     *
     * @return zip_name - 压缩文件名字
     */
    public String getZipName() {
        return zipName;
    }

    /**
     * 设置压缩文件名字
     *
     * @param zipName 压缩文件名字
     */
    public void setZipName(String zipName) {
        this.zipName = zipName == null ? null : zipName.trim();
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取压缩路径
     *
     * @return zip_path - 压缩路径
     */
    public String getZipPath() {
        return zipPath;
    }

    /**
     * 设置压缩路径
     *
     * @param zipPath 压缩路径
     */
    public void setZipPath(String zipPath) {
        this.zipPath = zipPath == null ? null : zipPath.trim();
    }

    /**
     * 获取压缩日期
     *
     * @return zip_date - 压缩日期
     */
    public Date getZipDate() {
        return zipDate;
    }

    /**
     * 设置压缩日期
     *
     * @param zipDate 压缩日期
     */
    public void setZipDate(Date zipDate) {
        this.zipDate = zipDate;
    }
}