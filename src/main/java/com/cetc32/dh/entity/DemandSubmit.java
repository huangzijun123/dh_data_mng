package com.cetc32.dh.entity;

import org.springframework.util.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DemandSubmit extends NumberS{
    private Integer id;

    private String projectName;

    private String demandName;

    private String demandDes;

    private String status;

    private String reporter;

    private Integer departmentid;

    private String approver;

    private Date approceTime;

    private String getEndtimeFront;

    private String getApproveFront;

    private String areachoice;

    private Date creattime;


    public String getGetEndtimeFront() {
        return getEndtimeFront;
    }

    public void setGetEndtimeFront(String getEndtimeFront) {
        this.getEndtimeFront = getEndtimeFront;
    }

    public String getGetApproveFront() {
        return getApproveFront;
    }

    public void setGetApproveFront(String getApproveFront) {
        this.getApproveFront = getApproveFront;
    }

    private Date endtime;

    private String demandClassify;

    private String demandAttachment;

    private String area;

    private String areaname;

    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDemandName() {
        return demandName;
    }

    public void setDemandName(String demandName) {
        this.demandName = demandName;
    }

    public String getDemandDes() {
        return demandDes;
    }

    public void setDemandDes(String demandDes) {
        this.demandDes = demandDes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public Integer getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(Integer departmentid) {
        this.departmentid = departmentid;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public Date getApproceTime() {
        return approceTime;
    }

    public void setApproceTime(Date approceTime) {
        this.approceTime = approceTime;
    }

    public Date getEndtime() throws Exception{
        return endtime;
    }

    public void setEndtime(Date endtime) {
        if(!StringUtils.isEmpty(endtime)){
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String endDateStr=df.format(endtime);
            try {
                endtime = df.parse(endDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        this.endtime = endtime;
    }

    public String getDemandClassify() {
        return demandClassify;
    }

    public void setDemandClassify(String demandClassify) {
        this.demandClassify = demandClassify;
    }

    public String getDemandAttachment() {
        return demandAttachment;
    }

    public void setDemandAttachment(String demandAttachment) {
        this.demandAttachment = demandAttachment;
    }

    public String getAreachoice() {
        return areachoice;
    }

    public void setAreachoice(String areachoice) {
        this.areachoice = areachoice;
    }

    public Date getCreattime() {
        return creattime;
    }

    public void setCreattime(Date creattime) {
        this.creattime = creattime;
    }


    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaname() {
        return areaname;
    }

    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }
}
