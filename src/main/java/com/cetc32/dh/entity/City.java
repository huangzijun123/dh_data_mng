/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.entity;

import javax.persistence.Column;

/**
 * 城市数据实体类
 *
 * @author: xiao
 * @version: 1.0
 * @date: 2020/10/14
 * 备注:无
 */
public class City {
    /**
     * 记录号fid
     */
    private String fid;

    /**
     * 省份编码
     */
    @Column(name = "province_code")
    private String provinceCode;

    /**
     * 省名称
     */
    private String province;

    /**
     * 城市编码
     */
    @Column(name = "city_code")
    private String cityCode;

    /**
     * 城市名称
     */
    private String city;

    /**
     * 城市类别
     */
    private String type;

    /**
     * 源fid号
     */
    @Column(name = "orig_fid")
    private String origFid;

    /**
     * 经度坐标
     */
    @Column(name = "point_x")
    private String pointX;

    /**
     * 纬度坐标
     */
    @Column(name = "point_y")
    private String pointY;

    /**
     * 获取记录号fid
     *
     * @return fid - 记录号fid
     */
    public String getFid() {
        return fid;
    }

    /**
     * 设置记录号fid
     *
     * @param fid 记录号fid
     */
    public void setFid(String fid) {
        this.fid = fid == null ? null : fid.trim();
    }

    /**
     * 获取省份编码
     *
     * @return provinceCode - 省份编码
     */
    public String getProvinceCode() {
        return provinceCode;
    }

    /**
     * 设置省份编码
     *
     * @param provinceCode 省份编码
     */
    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode == null ? null : provinceCode.trim();
    }

    /**
     * 获取省名称
     *
     * @return province - 省名称
     */
    public String getProvince() {
        return province;
    }

    /**
     * 设置省名称
     *
     * @param province 省名称
     */
    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    /**
     * 获取城市编码
     *
     * @return cityCode - 城市编码
     */
    public String getCityCode() {
        return cityCode;
    }

    /**
     * 设置城市编码
     *
     * @param cityCode 城市编码
     */
    public void setCityCode(String cityCode) {
        this.cityCode = cityCode == null ? null : cityCode.trim();
    }

    /**
     * 获取城市名称
     *
     * @return city - 城市名称
     */
    public String getCity() {
        return city;
    }

    /**
     * 设置城市名称
     *
     * @param city 城市名称
     */
    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    /**
     * 获取城市类别
     *
     * @return type - 城市类别
     */
    public String getType() {
        return type;
    }

    /**
     * 设置城市类别
     *
     * @param type 城市类别
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 获取源fid号
     *
     * @return origFid - 源fid号
     */
    public String getOrigFid() {
        return origFid;
    }

    /**
     * 设置源fid号
     *
     * @param origFid 源fid号
     */
    public void setOrigFid(String origFid) {
        this.origFid = origFid == null ? null : origFid.trim();
    }

    /**
     * 获取经度坐标
     *
     * @return pointX - 经度坐标
     */
    public String getPointX() {
        return pointX;
    }

    /**
     * 设置经度坐标
     *
     * @param pointX 经度坐标
     */
    public void setPointX(String pointX) {
        this.pointX = pointX == null ? null : pointX.trim();
    }

    /**
     * 获取纬度坐标
     *
     * @return pointY - 纬度坐标
     */
    public String getPointY() {
        return pointY;
    }

    /**
     * 设置纬度坐标
     *
     * @param pointY 纬度坐标
     */
    public void setPointY(String pointY) {
        this.pointY = pointY == null ? null : pointY.trim();
    }
}