/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.entity;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

/**
 * 数据管理轨迹数据实体类
 *
 * @author: xiao
 * @version: 1.0
 * @date: 2020/10/14
 * 备注:无
 */
@Table(name = "data_trace")
public class DataTrace extends NumberS {

  /**
   * 无参构造函数
   */
  public DataTrace() {
  }

  /**
   * @param id
   * @param createTime
   * @param fileTime
   * @param status
   * @param fileConfig
   * @param approveTime
   * @param points
   * @param approver
   * @param userId
   * @param security
   * @param description
   * @param speed
   * @param locsource
   * @param deviceid
   * @param direction
   * @param lat
   * @param linkid
   * @param lon
   * @param enccrylatitude
   * @param encrylongitude
   * @param flag
   * @param submitor
   * @param fileName
   * @param filePath
   * @param fileSize
   * @param startTime
   * @param endTime
   * @param menuId
   * 有参构造函数
   */
  public DataTrace(Integer id, Integer security, Date createTime, String fileTime, String description, Integer userId, String approver, String status,
                   String fileConfig, Date approveTime, String lat, String lon, Integer speed, Integer direction, Integer locsource, Integer coordinateerror,
                   String deviceid, Integer encrylongitude, Integer enccrylatitude, Boolean flag, String points, String linkid, String submitor, Date startTime,
                   Date endTime, String filePath, String fileSize, String fileName, Integer menuId, String td_start, String td_end) {
    this.id = id;
    this.security = security;
    this.createTime = createTime;
    this.fileTime = fileTime;
    this.description = description;
    this.userId = userId;
    this.approver = approver;
    this.status = status;
    this.fileConfig = fileConfig;
    this.approveTime = approveTime;
    this.points = points;
    this.deviceid = deviceid;
    this.enccrylatitude = enccrylatitude;
    this.encrylongitude = encrylongitude;
    this.flag = flag;
    this.speed = speed;
    this.direction = direction;
    this.lat = lat;
    this.lon = lon;
    this.locsource = locsource;
    this.coordinateerror = coordinateerror;
    this.linkid = linkid;
    this.submitor = submitor;
    this.startTime = startTime;
    this.endTime = endTime;
    this.filePath = filePath;
    this.fileSize = fileSize;
    this.fileName = fileName;
    this.menuId = menuId;
    this.td_end = td_end;
    this.td_start = td_start;
  }

  /**
   * 编目id
   */
  @Column(name = "menu_id")
  private Integer menuId;

  /**
   * 纬度
   */
  private String lat;

  /**
   * 经度
   */
  private String lon;

  /**
   * 速度
   */
  private Integer speed;

  /**
   * 方向
   */
  private Integer direction;

  /**
   * 数据源
   */
  private Integer locsource;

  /**
   * 精度
   */
  private Integer coordinateerror;

  /**
   * 创建时间
   */
  @Column(name = "create_time")
  private Date createTime;

  /**
   * 设备ID
   */
  @Column(name = "deviceId")
  private String deviceid;

  /**
   * 偏移经度
   */
  private Integer encrylongitude;

  /**
   * 偏移纬度
   */
  private Integer enccrylatitude;

  /**
   * 点集wkt点位
   */
  private String points;

  /**
   * 标志
   */
  private Boolean flag;

  /**
   * linkID
   */
  private String linkid;

  /**
   * 用户ID
   */
  @Column(name = "user_id")
  private Integer userId;

  /**
   * 唯一标识
   */
  private Integer id;

  /**
   * 审批人
   */
  private String approver;

  /**
   * 审批时间
   */
  @Column(name = "approve_time")
  private Date approveTime;

  /**
   * 时间数组(时间段)
   */
  private String[] timeRange;

  /**
   * 开始时间
   */
  private String startTimeCompare;

  /**
   * 结束时间
   */
  private String endTimeCompare;

  /**
   * 轨迹开始时间
   */
  @Column(name = "start_time")
  private Date startTime;

  /**
   * 轨迹结束时间
   */
  @Column(name = "end_time")
  private Date endTime;

  /**
   * 数据文件路径
   */
  @Column(name = "file_path")
  private String filePath;

  /**
   * 数据文件大小
   */
  @Column(name = "file_size")
  private String fileSize;

  /**
   * 审批状态
   */
  private String status;

  /**
   * 数据标识
   */
  @Column(name = "file_config")
  private String fileConfig;

  /**
   * 文件名字
   */
  @Column(name = "file_name")
  private String fileName;

  /**
   * 数据所属时间
   */
  @Column(name = "file_time")
  private String fileTime;

  /**
   * 数据描述
   */
  private String description;

  /**
   * 安全等级
   */
  private Integer security;

  /**
   * 提交用户姓名
   */
  @Column(name = "submitor")
  private String submitor;

  /**
   * 今日开始时间td_start
   */
  private String td_start;

  /**
   * 今日结束时间td_end
   */
  private String td_end;

  /**
   * 获取时间数组(时间段)
   *
   * @return timeRange - 时间数组(时间段)
   */
  public String[] getTimeRange() {
    return timeRange;
  }

  /**
   * 设置时间数组(时间段)
   *
   * @param timeRange 时间数组(时间段)
   */
  public void setTimeRange(String[] timeRange) {
    this.timeRange = timeRange;
  }

  /**
   * 获取开始时间
   *
   * @return startTimeCompare - 开始时间
   */
  public String getStartTimeCompare() {
    return startTimeCompare;
  }

  /**
   * 设置开始时间
   *
   * @param startTimeCompare 开始时间
   */
  public void setStartTimeCompare(String startTimeCompare) {
    this.startTimeCompare = startTimeCompare;
  }

  /**
   * 获取结束时间
   *
   * @return endTimeCompare - 结束时间
   */
  public String getEndTimeCompare() {
    return endTimeCompare;
  }

  /**
   * 设置结束时间
   *
   * @param endTimeCompare 结束时间
   */
  public void setEndTimeCompare(String endTimeCompare) {
    this.endTimeCompare = endTimeCompare;
  }

  /**
   * 获取今日开始时间
   *
   * @return td_start - 今日开始时间
   */
  public String getTd_start() {
    return td_start;
  }

  /**
   * 设置今日开始时间
   *
   * @param td_start 今日开始时间
   */
  public void setTd_start(String td_start) {
    this.td_start = td_start;
  }

  /**
   * 获取今日结束时间
   *
   * @return td_end - 今日结束时间
   */
  public String getTd_end() {
    return td_end;
  }

  /**
   * 设置今日结束时间
   *
   * @param td_end 今日结束时间
   */
  public void setTd_end(String td_end) {
    this.td_end = td_end;
  }

  /**
   * 获取纬度
   *
   * @return lat - 纬度
   */
  public String getLat() {
    return lat;
  }

  /**
   * 设置纬度
   *
   * @param lat 纬度
   */
  public void setLat(String lat) {
    this.lat = lat == null ? null : lat.trim();
  }

  /**
   * 获取经度
   *
   * @return lon - 经度
   */
  public String getLon() {
    return lon;
  }

  /**
   * 设置经度
   *
   * @param lon 经度
   */
  public void setLon(String lon) {
    this.lon = lon == null ? null : lon.trim();
  }

  /**
   * 获取速度
   *
   * @return speed - 速度
   */
  public Integer getSpeed() {
    return speed;
  }

  /**
   * 设置速度
   *
   * @param speed 速度
   */
  public void setSpeed(Integer speed) {
    this.speed = speed;
  }

  /**
   * 获取方向
   *
   * @return direction - 方向
   */
  public Integer getDirection() {
    return direction;
  }

  /**
   * 设置方向
   *
   * @param direction 方向
   */
  public void setDirection(Integer direction) {
    this.direction = direction;
  }

  /**
   * 获取数据源
   *
   * @return locSource - 数据源
   */
  public Integer getLocsource() {
    return locsource;
  }

  /**
   * 设置数据源
   *
   * @param locsource 数据源
   */
  public void setLocsource(Integer locsource) {
    this.locsource = locsource;
  }

  /**
   * 获取精度
   *
   * @return coordinateError - 精度
   */
  public Integer getCoordinateerror() {
    return coordinateerror;
  }

  /**
   * 设置精度
   *
   * @param coordinateerror 精度
   */
  public void setCoordinateerror(Integer coordinateerror) {
    this.coordinateerror = coordinateerror;
  }

  /**
   * 获取创建时间
   *
   * @return create_time - 创建时间
   */
  public Date getCreateTime() {
    return createTime;
  }

  /**
   * 设置创建时间
   *
   * @param createTime 创建时间
   */
  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  /**
   * 获取设备ID
   *
   * @return deviceId - 设备ID
   */
  public String getDeviceid() {
    return deviceid;
  }

  /**
   * 设置设备ID
   *
   * @param deviceid 设备ID
   */
  public void setDeviceid(String deviceid) {
    this.deviceid = deviceid == null ? null : deviceid.trim();
  }

  /**
   * 获取偏移经度
   *
   * @return encryLongitude - 偏移经度
   */
  public Integer getEncrylongitude() {
    return encrylongitude;
  }

  /**
   * 设置偏移经度
   *
   * @param encrylongitude 偏移经度
   */
  public void setEncrylongitude(Integer encrylongitude) {
    this.encrylongitude = encrylongitude;
  }

  /**
   * 获取偏移纬度
   *
   * @return enccryLatitude - 偏移纬度
   */
  public Integer getEnccrylatitude() {
    return enccrylatitude;
  }

  /**
   * 设置偏移纬度
   *
   * @param enccrylatitude 偏移纬度
   */
  public void setEnccrylatitude(Integer enccrylatitude) {
    this.enccrylatitude = enccrylatitude;
  }

  /**
   * 获取点集wkt点位
   *
   * @return points - 点集wkt点位
   */
  public String getPoints() {
    return points;
  }

  /**
   * 设置点集wkt点位
   *
   * @param points 点集wkt点位
   */
  public void setPoints(String points) {
    this.points = points == null ? null : points.trim();
  }

  /**
   * 获取标志
   *
   * @return flag - 标志
   */
  public Boolean getFlag() {
    return flag;
  }

  /**
   * 设置标志
   *
   * @param flag 标志
   */
  public void setFlag(Boolean flag) {
    this.flag = flag;
  }

  /**
   * 获取linkID
   *
   * @return linkID - linkID
   */
  public String getLinkid() {
    return linkid;
  }

  /**
   * 设置linkID
   *
   * @param linkid linkID
   */
  public void setLinkid(String linkid) {
    this.linkid = linkid == null ? null : linkid.trim();
  }

  /**
   * 获取用户ID
   *
   * @return userId - 用户ID
   */
  public Integer getUserId() {
    return userId;
  }

  /**
   * 设置用户ID
   *
   * @param userId 用户ID
   */
  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  /**
   * 获取唯一标识
   *
   * @return id - 唯一标识
   */
  public Integer getId() {
    return id;
  }

  /**
   * 设置唯一标识
   *
   * @param id 唯一标识
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * 获取审批人
   *
   * @return approver - 审批人
   */
  public String getApprover() {
    return approver;
  }

  /**
   * 设置审批人
   *
   * @param approver 审批人
   */
  public void setApprover(String approver) {
    this.approver = approver == null ? null : approver.trim();
  }

  /**
   * 获取审批时间
   *
   * @return approve_time - 审批时间
   */
  public Date getApproveTime() {
    return approveTime;
  }

  /**
   * 设置审批时间
   *
   * @param approveTime 审批时间
   */
  public void setApproveTime(Date approveTime) {
    this.approveTime = approveTime;
  }

  /**
   * 获取审批状态
   *
   * @return status - 审批状态
   */
  public String getStatus() {
    return status;
  }

  /**
   * 设置审批状态
   *
   * @param status 审批状态
   */
  public void setStatus(String status) {
    this.status = status == null ? null : status.trim();
  }

  /**
   * 获取数据标识
   *
   * @return file_config - 数据标识
   */
  public String getFileConfig() {
    return fileConfig;
  }

  /**
   * 设置数据标识
   *
   * @param fileConfig 数据标识
   */
  public void setFileConfig(String fileConfig) {
    this.fileConfig = fileConfig == null ? null : fileConfig.trim();
  }

  /**
   * 获取数据所属时间
   *
   * @return file_time - 数据所属时间
   */
  public String getFileTime() {
    return fileTime;
  }

  /**
   * 设置数据所属时间
   *
   * @param fileTime 数据所属时间
   */
  public void setFileTime(String fileTime) {
    this.fileTime = fileTime == null ? null : fileTime.trim();
  }

  /**
   * 获取数据描述
   *
   * @return description - 数据描述
   */
  public String getDescription() {
    return description;
  }

  /**
   * 设置数据描述
   *
   * @param description 数据描述
   */
  public void setDescription(String description) {
    this.description = description == null ? null : description.trim();
  }

  /**
   * 获取安全等级
   *
   * @return security - 安全等级
   */
  public Integer getSecurity() {
    return security;
  }

  /**
   * 设置安全等级
   *
   * @param security 安全等级
   */
  public void setSecurity(Integer security) {
    this.security = security;
  }

  /**
   * 获取提交用户姓名
   *
   * @return submitor - 提交用户姓名
   */
  public String getSubmitor() {
    return submitor;
  }

  /**
   * 设置提交用户姓名
   *
   * @param submitor 提交用户姓名
   */
  public void setSubmitor(String submitor) {
    this.submitor = submitor == null ? null : submitor.trim();
  }

  /**
   * 获取文件数据大小
   *
   * @return file_size - 文件数据大小
   */
  public String getFileSize() {
    return fileSize;
  }

  /**
   * 设置文件数据大小
   *
   * @param fileSize 文件数据大小
   */
  public void setFileSize(String fileSize) {
    this.fileSize = fileSize;
  }


  /**
   * 获取文件数据路径
   * <p>
   * file_path 文件数据路径
   */
  public String getFilePath() {
    return filePath;
  }

  /**
   * 设置文件数据路径
   *
   * @param filePath 文件数据大小
   */
  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  /**
   * 获取轨迹数据结束时间
   * <p>
   * end_Time 轨迹数据结束时间
   */
  public Date getEndTime() {
    return endTime;
  }

  /**
   * 设置轨迹数据结束时间
   *
   * @param endTime 轨迹数据结束时间
   */
  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  /**
   * 获取轨迹数据开始时间
   * <p>
   * start_Time 轨迹数据开始时间
   */
  public Date getStartTime() {
    return startTime;
  }


  /**
   * 设置轨迹数据开始时间
   *
   * @param startTime 轨迹数据开始时间
   */
  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  /**
   * 获取文件数据名称
   * <p>
   * file_name 文件数据名称
   */
  public String getFileName() {
    return fileName;
  }

  /**
   * 设置文件数据名称
   *
   * @param fileName 文件数据名称
   */
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  /**
   * 编目id
   *
   * @return menuId 编目id
   */
  public Integer getMenuId() {
    return menuId;
  }

  /**
   * 设置编目id
   *
   * @param menuId 编目id
   */
  public void setMenuId(Integer menuId) {
    this.menuId = menuId;
  }


}
