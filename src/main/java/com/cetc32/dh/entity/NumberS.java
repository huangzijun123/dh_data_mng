package com.cetc32.dh.entity;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Transient;

public class NumberS {
    @Transient
    @ApiModelProperty(hidden = true)
    private Integer numberId;

    @Transient
    Integer page;

    @Transient
    Integer results;

    @Transient
    @ApiModelProperty(hidden = true)
    Integer offset=0;

    public Integer getPage() {
        if(page==null || page<=0)
            return 1;
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getResults() {
        if(results==null || results<=0)
            return Integer.MAX_VALUE;
        return results;
    }

    public void setResults(Integer results) {
        this.results = results;
    }

    public Integer getOffset() {

        return (getPage()-1)*getResults();
    }

    public Integer getNumberId() {
        return numberId;
    }

    public void setNumberId(Integer numberId) {
        this.numberId = numberId;
    }



}
