/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/8/25 下午3:46
 ******************************************************************************/
package com.cetc32.dh.entity;

public class DataPlpCollect  extends DataPlp{
    private String screenshot[];
    private String photos[];

    public String[] getScreenshot() {
        return screenshot;
    }

    public void setScreenshot(String... screenshot) {
        if(null == screenshot || screenshot.length ==0)
            this.screenshot = null;
        this.screenshot = screenshot;
    }

    public String[] getPhoto() {
        return photos;
    }

    public void setPhoto(String... photos) {
        if( photos ==null || photos.length==0) this.photos=null;
        else this.photos = photos;
    }
}
