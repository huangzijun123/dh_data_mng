package com.cetc32.dh.entity;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.util.Date;
import java.util.List;

public class EstimateTask extends NumberS{
    private Integer id;

    private String name;

    private String taskClassify;

    private String taskType;

    private String taskPath;

    private String starttimefront;

    private String endtimefront;

    private Date starttime;

    private Date endtime;

    private String creator;

    private String creattimefront;

    private Date creattime;

    private String status;

    private String approver;

    private Integer demandId;

    private List<String> frontyear;

    private Date approvtime;

    private String report;
    private String reason;

    private String taskdes;

    private Date changetime;
    private JSONObject data;
    private String datas;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaskClassify() {
        return taskClassify;
    }

    public void setTaskClassify(String taskClassify) {
        this.taskClassify = taskClassify;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getTaskPath() {
        return taskPath;
    }

    public void setTaskPath(String taskPath) {
        this.taskPath = taskPath;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreattime() {
        return creattime;
    }

    public void setCreattime(Date creattime) {
        this.creattime = creattime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public Integer getDemandId() {
        return demandId;
    }

    public void setDemandId(Integer demandId) {
        this.demandId = demandId;
    }

    public String getStarttimefront() {
        return starttimefront;
    }

    public void setStarttimefront(String starttimefront) {
        this.starttimefront = starttimefront;
    }

    public String getEndtimefront() {
        return endtimefront;
    }

    public void setEndtimefront(String endtimefront) {
        this.endtimefront = endtimefront;
    }

    public String getCreattimefront() {
        return creattimefront;
    }

    public void setCreattimefront(String creattimefront) {
        this.creattimefront = creattimefront;
    }

    public List<String> getFrontyear() {
        return frontyear;
    }

    public void setFrontyear(List<String> frontyear) {
        this.frontyear = frontyear;
    }

    public Date getApprovtime() {
        return approvtime;
    }

    public void setApprovtime(Date approvtime) {
        this.approvtime = approvtime;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getTaskdes() {
        return taskdes;
    }

    public void setTaskdes(String taskdes) {
        this.taskdes = taskdes;
        this.setContent(taskdes);
    }

    public Date getChanggetime() {
        return changetime;
    }

    public void setChanggetime(Date changgetime) {
        this.changetime = changgetime;
    }

    public String getDatas() {
        if(null == data)
            return null;
        return data.toJSONString();
    }
    public JSONObject getData(){
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public void setDatas(String datas) {
        this.datas = datas;
        try{
            if(null == data)
                data = JSONObject.parseObject(datas);
            else
                data.putAll(JSONObject.parseObject(datas));
        }catch (JSONException e){

        }
    }
    private void setValue(String key ,String value){
        if(null ==data){
            data = new JSONObject();
        }
        data.put(key,value);
    }
    public void setContent(String content){
        setValue("content",content);
    }
    public void setUrl(String url){
        setValue("url",url);
    }
    public void setHttpmethod(String httpmethod){
        setValue("httpmethod",httpmethod);
    }
    public void setParameters(String parameters){
        setValue("parameters",parameters);
    }
    public void setExpectedResult(String expectedResult){
        setValue("expectedResult",expectedResult);
    }

    private String getValue(String key){
        if(null ==data || key==null)
            return null;
        try{
            return data.getString(key);
        }catch(JSONException e){
            return null;
        }
    }

}
