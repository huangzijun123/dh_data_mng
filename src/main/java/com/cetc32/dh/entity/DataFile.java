/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.entity;

import org.apache.commons.lang3.StringUtils;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 数据管理成果数据实体类
 *
 * @author: xiao
 * @version: 1.0
 * @date: 2020/10/14
 * 备注:无
 */
@Table(name = "data_file")
public class DataFile extends NumberS {

    /**
     * 数据id,文件(夹)id
     */
    @Id
    private Long id;

    /**
     * 文件名称
     */
    @Column(name = "file_name")
    private String fileName;

    /**
     * 数据安全等级，文件(夹)安全等级
     */
    private Integer security;

    /**
     * 数据路径，文件(夹)存放路径
     */
    @Column(name = "file_path")
    private String filePath;

    /**
     * 数据时间，文件(夹)创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 时间数组，存放搜索的开始时间和结束时间
     */
    private String[] timeRange;

    /**
     * 开始时间(文件年份时间)
     */
    private String startTime;

    /**
     * 结束时间（文件年份时间）
     */
    private String endTime;

    /**
     * 数据区域，文件(夹)所属区域
     */
    private String region;

    /**
     * 数据区域list，文件(夹)所属区域
     */
    private List<String> regionList;

    /**
     * 数据大小，文件(夹)大小
     */
    @Column(name = "file_size")
    private Long fileSize;

    /**
     * 文件夹中文件个数
     */
    @Column(name = "file_numbers")
    private Integer fileNumbers;

    /**
     * 数据描述，文件(夹)描述
     */
    @Column(name = "file_discription")
    private String fileDiscription;

    /**
     * 提交人
     */
    private String submitor;

    /**
     * 审核人
     */
    private String approver;

    /**
     * 图像地理坐标系（北京54、西安80、WGS－84、2000国家大地坐标系）
     */
    private String gcs;

    /**
     * 图像级别（1-20）
     */
    @Column(name = "scan_level")
    private Integer scanLevel;

    /**
     * 图像比例尺（1:10万 1:25万 1:50万 1:100万）
     */
    private String scale;

    /**
     * 经度（左上）
     */
    private String lan;

    /**
     * 纬度（左上）
     */
    private String lon;

    /**
     * 保留字段
     */
    private String catagory;

    /**
     * 数据日期,文件(夹)所属年份
     */
    @Column(name = "file_time")
    private java.sql.Date fileTime;

    /**
     * 审批状态
     */
    private String status;

    /**
     * 编目Id
     */
    @Column(name = "menu_id")
    private Integer menuId;

    /**
     * 文件标识，数据标识
     */
    @Column(name = "file_config")
    private String fileConfig;

    /**
     * 文件审批数据时间
     */
    @Column(name = "approve_time")
    private Date approveTime;

    /**
     * 范围自定义（点集）
     */
    @Column(name = "area")
    private String area;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 今日开始时间td_start
     */
    private String td_start;

    /**
     * 今日结束时间td_end
     */
    private String td_end;

    /**
     * 图片样本路径
     */
   @Column(name= "img_example")
    private String imgExample;

    /**
     * 数据所有权单位
     */
    @Column(name= "owner_unit")
    private String ownerUnit;

    /**
     * 数据出版单位
     */
    @Column(name= "pub_unit")
    private String pubUnit;

    /**
     * 数据出版日期
     */
    @Column(name= "pub_time")
    private java.sql.Date pubTime;

    /**
     * 参照图式编号
     */
    @Column(name= "ref_imgnum")
    private String refImgnum;

    /**
     * 参照要素分类编码编号
     */
    @Column(name= "ref_codenum")
    private String refCodenum;

    @Column(name = "points")
    private String points;

    /**
     * 无参构造函数
     */
    public DataFile() {
    }

    /**
     * 获取今日开始时间td_start
     *
     * @return td_start - 今日开始时间td_start
     */
    public String getTd_start() {
        return td_start;
    }

    /**
     * 设置今日开始时间td_start
     *
     * @param td_start - 今日开始时间td_start
     */
    public void setTd_start(String td_start) {
        this.td_start = td_start;
    }

    /**
     * 获取今日结束时间td_end
     *
     * @return td_end -今日结束时间td_end
     */
    public String getTd_end() {
        return td_end;
    }

    /**
     * 设置今日结束时间td_end
     *
     * @param td_end - 今日结束时间td_end
     */
    public void setTd_end(String td_end) {
        this.td_end = td_end;
    }

    /**
     * 获取数据id,文件(夹)id
     *
     * @return id - 数据id,文件(夹)id
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置数据id,文件(夹)id
     *
     * @param id 数据id,文件(夹)id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取编目id
     *
     * @return menuId - 编目id
     */
    public Integer getMenuId() {
        return menuId;
    }

    /**
     * 设置编目id
     *
     * @param menuId 编目id
     */
    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }


    /**
     * 获取用户id
     *
     * @return userId - 用户id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取文件名称
     *
     * @return file_name - 文件名称
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * 设置文件名称
     *
     * @param fileName 文件名称
     */
    public void setFileName(String fileName) {
        this.fileName = fileName == null ? null : fileName.trim();
    }

    /**
     * 获取区域自定义范围
     *
     * @return area - 区域自定义范围
     */
    public String getArea() {
        return area;
    }

    /**
     * 设置区域自定义范围
     *
     * @param area 区域自定义范围
     */
    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    /**
     * 获取数据安全等级，文件(夹)安全等级
     *
     * @return security - 数据安全等级，文件(夹)安全等级
     */
    public Integer getSecurity() {
        return security;
    }

    /**
     * 设置数据安全等级，文件(夹)安全等级
     *
     * @param security 数据安全等级，文件(夹)安全等级
     */
    public void setSecurity(Integer security) {
        this.security = security;
    }

    /**
     * 获取数据路径，文件(夹)存放路径
     *
     * @return file_path - 数据路径，文件(夹)存放路径
     */
    public String getFilePath() {
        return filePath;
    }

    public String getDirectoryName(){
        return getFilePath();
    }

    /**
     * 设置数据路径，文件(夹)存放路径
     *
     * @param filePath 数据路径，文件(夹)存放路径
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath == null ? null : filePath.trim();
    }

    public void setDirectoryName(String directoryName){
        setFilePath(directoryName);
    }

    /**
     * 获取数据时间，文件(夹)创建时间
     *
     * @return create_time - 数据时间，文件(夹)创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置数据时间，文件(夹)创建时间
     *
     * @param createTime 数据时间，文件(夹)创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取开始时间
     *
     * @return start_time - 开始时间
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * 设置开始时间
     *
     * @param startTime 开始时间
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取结束时间
     *
     * @return end_time - 结束时间
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime 结束时间
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取数据区域，文件(夹)所属区域
     *
     * @return region - 数据区域，文件(夹)所属区域
     */
    public String getRegion() {
        return region;
    }

    /**
     * 设置数据区域，文件(夹)所属区域
     *
     * @param region 数据区域，文件(夹)所属区域
     */
    public void setRegion(String region) {
        this.region = region == null ? null : region.trim();
    }

    public void setName(String name){
        setRegion(name);
    }
    public String getName(){
        return getRegion();
    }

    /**
     * 获取数据大小，文件(夹)大小
     *
     * @return file_size - 数据大小，文件(夹)大小
     */
    public Long getFileSize() {
        return fileSize;
    }

    /**
     * 设置数据大小，文件(夹)大小
     *
     * @param fileSize 数据大小，文件(夹)大小
     */
    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * 获取文件夹中文件个数
     *
     * @return file_numbers - 文件夹中文件个数
     */
    public Integer getFileNumbers() {
        return fileNumbers;
    }

    /**
     * 设置文件夹中文件个数
     *
     * @param fileNumbers 文件夹中文件个数
     */
    public void setFileNumbers(Integer fileNumbers) {
        this.fileNumbers = fileNumbers;
    }

    /**
     * 获取数据描述，文件(夹)描述
     *
     * @return file_discription - 数据描述，文件(夹)描述
     */
    public String getFileDiscription() {
        return fileDiscription;
    }
    public String getVersionDescription(){
        return getFileDiscription();
    }
    /**
     * 设置数据描述，文件(夹)描述
     *
     * @param fileDiscription 数据描述，文件(夹)描述
     */
    public void setFileDiscription(String fileDiscription) {
        this.fileDiscription = fileDiscription == null ? null : fileDiscription.trim();
    }

    public void setVersionDescription(String versionDescription){
        setFileDiscription(versionDescription);
    }

    /**
     * 获取提交人
     *
     * @return submitor- 提交人
     */
    public String getSubmitor() {
        return submitor;
    }

    /**
     * 设置提交人
     *
     * @param submitor 提交人
     */
    public void setSubmitor(String submitor) {
        this.submitor = submitor == null ? null : submitor.trim();
    }

    /**
     * 获取审批人
     *
     * @return approver - 审批人
     */
    public String getApprover() {
        return approver;
    }

    /**
     * 设置审批人
     *
     * @param approver 审批人
     */
    public void setApprover(String approver) {
        this.approver = approver == null ? null : approver.trim();
    }

    /**
     * 获取图像地理坐标系（北京54、西安80、WGS－84、2000国家大地坐标系）
     *
     * @return gcs - 图像地理坐标系（北京54、西安80、WGS－84、2000国家大地坐标系）
     */
    public String getGcs() {
        return gcs;
    }

    /**
     * 设置图像地理坐标系（北京54、西安80、WGS－84、2000国家大地坐标系）
     *
     * @param gcs 图像地理坐标系（北京54、西安80、WGS－84、2000国家大地坐标系）
     */
    public void setGcs(String gcs) {
        this.gcs = gcs == null ? null : gcs.trim();
    }

    /**
     * 获取图像级别（1-20）
     *
     * @return scan_level - 图像级别（1-20）
     */
    public Integer getScanLevel() {
        return scanLevel;
    }


    /**
     * 设置图像级别（1-20）
     *
     * @param scanLevel 图像级别（1-20）
     */
    public void setScanLevel(Integer scanLevel) {
        this.scanLevel = scanLevel;
    }

    /**
     * 设置图像级别（1-20）
     *
     * @param scanLevel 图像级别（1-20）输入参数String类型
     */
    public void setScanLevel(String scanLevel) {
        if (scanLevel.equals("全部级别") || StringUtils.isEmpty(scanLevel)) {
            this.scanLevel = null;
        } else {
            this.scanLevel = Integer.parseInt(scanLevel);
        }
    }

    /**
     * 获取图像比例尺（1:10万 1:25万 1:50万 1:100万）
     *
     * @return scale - 图像比例尺（1:10万 1:25万 1:50万 1:100万）
     */
    public String getScale() {
        return scale;
    }

    /**
     * 设置图像比例尺（1:10万 1:25万 1:50万 1:100万）
     *
     * @param scale 图像比例尺（1:10万 1:25万 1:50万 1:100万）
     */
    public void setScale(String scale) {
        this.scale = scale == null ? null : scale.trim();
    }

    /**
     * 获取经度（左上）
     *
     * @return lan - 经度（左上）
     */
    public String getLan() {
        return lan;
    }

    /**
     * 设置经度（左上）
     *
     * @param lan 经度（左上）
     */
    public void setLan(String lan) {
        this.lan = lan == null ? null : lan.trim();
    }

    /**
     * 获取纬度（左上）
     *
     * @return lon - 纬度（左上）
     */
    public String getLon() {
        return lon;
    }

    /**
     * 设置纬度（左上）
     *
     * @param lon 纬度（左上）
     */
    public void setLon(String lon) {
        this.lon = lon == null ? null : lon.trim();
    }

    /**
     * 获取保留
     *
     * @return catagory - 保留
     */
    public String getCatagory() {
        return catagory;
    }

    /**
     * 设置保留
     *
     * @param catagory 保留
     */
    public void setCatagory(String catagory) {
        this.catagory = catagory == null ? null : catagory.trim();
    }

    /**
     * 获取数据日期,文件(夹)所属年份
     *
     * @return file_time - 数据日期,文件(夹)所属年份
     */
    public java.sql.Date getFileTime() {
        return fileTime;
    }

    /**
     * 设置数据日期,文件(夹)所属年份
     *
     * @param fileTime 数据日期,文件(夹)所属年份
     */
    public void setFileTime(String fileTime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date d = null;
        try {
            d = format.parse(fileTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        java.sql.Date date = new java.sql.Date(d.getTime());
        this.fileTime = date;
    }

    /**
     * 获取审批状态
     *
     * @return status - 审批状态
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置审批状态
     *
     * @param status 审批状态
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * 获取文件标识，数据标识
     *
     * @return file_config - 文件标识，数据标识
     */
    public String getFileConfig() {
        return fileConfig;
    }

    /**
     * 设置数据标识，文件标识
     *
     * @param fileConfig 数据标识，文件标识
     */
    public void setFileConfig(String fileConfig) {
        this.fileConfig = fileConfig == null ? null : fileConfig.trim();
    }


    /**
     * 获取数据、文件时间
     *
     * @return approve_time
     */
    public Date getApproveTime() {
        return approveTime;
    }

    /**
     * 设置文件数据时间
     *
     * @param approveTime
     */
    public void setApproveTime(Date approveTime) {
        this.approveTime = approveTime;
    }

    /**
     * 获取数据区域list，文件(夹)所属区域
     *
     * @return regionList
     */
    public List<String> getRegionList() {
        return regionList;
    }

    /**
     * 设置数据区域list，文件(夹)所属区域
     *
     * @param regionList
     */
    public void setRegionList(List<String> regionList) {
        this.regionList = regionList;
    }

    /**
     * 获取时间数组
     *
     * @return timeRange
     */
    public String[] getTimeRange() {
        return timeRange;
    }

    /**
     * 设置时间数组
     *
     * @param timeRange
     */
    public void setTimeRange(String[] timeRange) {
        this.timeRange = timeRange;
    }

    /**
     * 获取图片样本路径（名称）
     * @return imgExample
     */
    public String getImgExample() {
        return imgExample;
      }

    /**
     * 设置图片样本路径
     * @param imgExample
     */
    public void setImgExample(String imgExample) {
        this.imgExample = imgExample;
      }

  /**
   * 获取数据所有权单位
   * @return ownerUnit
   */
    public String getOwnerUnit() {
      return ownerUnit;
    }

  /**
   * 设置数据所有权单位
   * @param ownerUnit
   */
    public void setOwnerUnit(String ownerUnit) {
      this.ownerUnit = ownerUnit;
    }

  /**
   * 获取数据出版单位
   * @return pubUnit
   */
    public String getPubUnit() {
      return pubUnit;
    }

  /**
   * 设置数据出版单位
   * @param pubUnit
   */
    public void setPubUnit(String pubUnit) {
      this.pubUnit = pubUnit;
    }

  /**
   * 获取数据出版日期
   * @return pubTime
   */
    public Date getPubTime() {
      return pubTime;
    }

  /**
   * 设置数据出版日期
   * @param pubTime
   */
    public void setPubTime(String pubTime) {
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      java.util.Date d = null;
      try {
        d = format.parse(pubTime);
      } catch (Exception e) {
        e.printStackTrace();
      }
      java.sql.Date date = new java.sql.Date(d.getTime());
      this.pubTime = date;
    }

  /**
   * 获取参照图式编号
   * @return refImgnum
   */
    public String getRefImgnum() {
      return refImgnum;
    }

  /**
   * 设置参照图式编号
   * @param refImgnum
   */
    public void setRefImgnum(String refImgnum) {
      this.refImgnum = refImgnum;
    }

  /**
   * 获取参照要素分类编码编号
   * @return refCodenum
   */
    public String getRefCodenum() {
      return refCodenum;
    }

  /**
   * 设置参照要素分类编码编号
   * @param refCodenum
   */
    public void setRefCodenum(String refCodenum) {
      this.refCodenum = refCodenum;
    }



  /**
     * 返回文件实体类的字符串形式
     */
    public String toString() {
        return "{" +
                "menuId=" + menuId + '\'' +
                "fileName=" + fileName + '\'' +
                ", area='" + area + '\'' +
                ", security=" + security + '\'' +
                ", filePath=" + filePath + '\'' +
                ", createTime=" + createTime + '\'' +
                ", region=" + region + '\'' +
                ", fileSize=" + fileSize + '\'' +
                ", fileNumbers=" + fileNumbers + '\'' +
                ", fileDiscription=" + fileDiscription +
                '}';
    }
}
