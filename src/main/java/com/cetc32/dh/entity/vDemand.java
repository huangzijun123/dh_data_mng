package com.cetc32.dh.entity;


import org.apache.commons.lang3.StringUtils;

import java.util.Date;

public class vDemand extends  NumberS{
    public String classify;
    public String name;
    public String people;
    public String starttime;
    public String endtime;

    public String status;

    public Date sqlstarttime;

    public Date sqlendtime;
    public String sortField;
    public String sortOrder;


    public String getTd_start() {
        return td_start;
    }

    public void setTd_start(String td_start) {
        this.td_start = td_start;
    }

    /**
     * 今日开始时间td_start
     */
    private String td_start;

    public String getTd_end() {
        return td_end;
    }

    public void setTd_end(String td_end) {
        this.td_end = td_end;
    }

    /**
     * 今日结束时间td_end
     */
    private String td_end;

    public Date getSqlstarttime() {
        return sqlstarttime;
    }

    public void setSqlstarttime(Date sqlstarttime) {
        this.sqlstarttime = sqlstarttime;
    }

    public Date getSqlendtime() {
        return sqlendtime;
    }

    public void setSqlendtime(Date sqlendtime) {
        this.sqlendtime = sqlendtime;
    }

    public String getClassify() {
        if(classify==null||StringUtils.isBlank(classify))
            return null;
        return classify;
    }

    public void setClassify(String classify) {
        if(classify!=null&&StringUtils.isNotBlank(classify))
            this.classify = classify;
    }

    public String getName() {
        if(name==null||StringUtils.isBlank(name))
            return null;
        return name;
    }

    public void setName(String name) {
        if(name!=null&&StringUtils.isNotBlank(name))
            this.name = name;
    }

    public String getPeople() {
        if(people==null||StringUtils.isBlank(people))
            return null;
        return people;
    }

    public void setPeople(String people) {
        if(people!=null&&StringUtils.isNotBlank(people))
        this.people = people;
    }

    public String getStarttime() {
        if(starttime==null||StringUtils.isBlank(starttime))
            return null;
        return starttime;
    }

    public void setStarttime(String starttime) {
        if(starttime!=null&&StringUtils.isNotBlank(starttime))
        this.starttime = starttime;
    }

    public String getEndtime() {
        if(endtime==null||StringUtils.isBlank(endtime))
            return null;
        return endtime;
    }

    public void setEndtime(String endtime) {
        if(endtime!=null&&StringUtils.isNotBlank(endtime))
        this.endtime = endtime;
    }

    public String getStatus() {
        if(status==null||StringUtils.isBlank(status))
            return null;
        return status;
    }

    public void setStatus(String status) {
        if(status!=null&&StringUtils.isNotBlank(status))
        this.status = status;
    }

  public String getSortField() {
    return sortField;
  }

  public void setSortField(String sortField) {
    if(StringUtils.isBlank(sortField))
      this.sortField =null;
    else{
      switch(sortField.toLowerCase()){
        case "endtime": this.sortField ="endtime";break;
        case "approcetime":this.sortField= "approce_time";break;
        default:;
      }
    }
  }

  public String getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(String sortOrder) {
      if(StringUtils.isBlank(sortOrder))
        this.sortOrder=null;
      else{
        switch(sortOrder.toLowerCase()){
          case "ascend": this.sortOrder="asc";break;
          case "descend":this.sortOrder= "desc";break;
          default:;
        }
      }

  }
}
