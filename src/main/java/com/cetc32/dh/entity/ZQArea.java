/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/10/8 下午4:36
 ******************************************************************************/
package com.cetc32.dh.entity;

public class ZQArea {
    private String id;
    private String zqName;
    private String polygon;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getZqName() {
        return zqName;
    }

    public void setZqName(String zqName) {
        this.zqName = zqName;
    }

    public String getPolygon() {
        return polygon;
    }

    public void setPolygon(String polygon) {
        this.polygon = polygon;
    }
}
