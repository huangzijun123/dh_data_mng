package com.cetc32.dh.entity;

import org.apache.commons.lang3.StringUtils;

public class vProduct {
    public String classify;
    public String name;
    public String astatus;
    public String creator;

    public String getTd_start() {
        return td_start;
    }

    public void setTd_start(String td_start) {
        this.td_start = td_start;
    }

    /**
     * 今日开始时间td_start
     */
    private String td_start;

    public String getTd_end() {
        return td_end;
    }

    public void setTd_end(String td_end) {
        this.td_end = td_end;
    }

    /**
     * 今日结束时间td_end
     */
    private String td_end;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;


    public String getClassify() {
        if(classify==null||StringUtils.isBlank(classify))
            return null;
        return classify;
    }

    public void setClassify(String classify) {
        if(classify!=null&&StringUtils.isNotBlank(classify))
        this.classify = classify;
    }

    public String getName() {
        if(name==null||StringUtils.isBlank(name))
            return null;
        return name;
    }

    public void setName(String name) {
        if(name!=null&&StringUtils.isNotBlank(name))
        this.name = name;
    }

    public String getStatus() {
        if(astatus==null||StringUtils.isBlank(astatus))
            return null;
        return astatus;
    }

    public void setStatus(String status) {
        if(status!=null&&StringUtils.isNotBlank(status))
            this.astatus = status;
    }

    public String getAstatus() {
        if(astatus==null||StringUtils.isBlank(astatus))
            return null;
        return astatus;
    }

    public void setAstatus(String astatus) {
        if(astatus!=null&&StringUtils.isNotBlank(astatus))
        this.astatus = astatus;
    }

    public String getCreator() {
        if(creator==null||StringUtils.isBlank(creator))
            return null;
        return creator;
    }

    public void setCreator(String creator) {
        if(creator!=null&&StringUtils.isNotBlank(creator))
        this.creator = creator;
    }
}
