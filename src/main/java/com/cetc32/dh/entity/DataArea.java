/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/9/16 下午1:25
 ******************************************************************************/
package com.cetc32.dh.entity;

public class DataArea {
    Long dataId;
    Long menuId;
    String areaCode;
    public DataArea(){}
    public DataArea(Long dataId,String areaCode,Long menuId){
        this.dataId=dataId;
        this.areaCode =areaCode;
        this.menuId = menuId;
    }
    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }
}
