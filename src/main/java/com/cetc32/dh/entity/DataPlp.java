/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.entity;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

/**
 * 数据管理点线面实体类
 *
 * @author: xiao
 * @version: 1.0
 * @date: 2020/10/14
 * 备注:无
 */
@Table(name = "data_plp")
public class DataPlp extends NumberS {

  /**
   * 用户id
   */
  @Column(name = "user_id")
  private Integer userId;

  /**
   * 数据类型-8:点，线：64,面：128
   */
  @Column(name = "event_type")
  private String eventType;

  /**
   * 点集wkt格式
   */
  private String points;

  /**
   * 当前区域
   */
  private String region;

  /**
   * 上传创建时间
   */
  @Column(name = "create_time")
  private Date createTime;

  /**
   * 数据描述
   */
  private String description;

  /**
   * 编目id
   */
  @Column(name = "menu_id")
  private Integer menuId;

  /**
   * 预留字段2
   */
  private String cityName;

  /**
   * 唯一标识
   */
  private Integer id;

  /**
   * 数据安全等级
   */
  private Integer security;

  /**
   * 审批人
   */
  private String approver;

  /**
   * 审批时间
   */
  @Column(name = "approve_time")
  private Date approveTime;

  /**
   * 审批状态
   */
  private String status;

  /**
   * 数据标识
   */
  @Column(name = "file_config")
  private String fileConfig;

  /**
   * 数据名称
   */
  @Column(name = "file_name")
  private String fileName;

  /**
   * 数据所属年份
   */
  @Column(name = "file_time")
  private String fileTime;

  /**
   * 照片流
   */
  @Column(name = "photo_byte")
  private byte[] photoByte;
  /**
   * 照片流
   */
  @Column(name = "screen_shot")
  private byte[] shot;
  /**
   * 提交者
   */
  @Column(name = "submitor")
  private String submitor;

  /**
   * 时间数组
   */
  private String[] timeRange;

  /**
   * 开始时间
   */
  private String startTime;

  /**
   * 结束时间
   */
  private String endTime;

  /**
   * 今日开始时间td_start
   */
  private String td_start;

  /**
   * 今日结束时间td_end
   */
  private String td_end;

  /***/
  private String eventName;
  private String operateState;

  private String type;
  private String uploadTime;
  private String reserve1;
  private String reserve2;
  private String reserve3;

  public String getUploadTime() {
    return uploadTime;
  }

  public void setUploadTime(String uploadTime) {
    this.uploadTime = uploadTime;
  }

  public String getReserve1() {
    return reserve1;
  }

  public void setReserve1(String reserve1) {
    this.reserve1 = reserve1;
  }

  public String getReserve2() {
    return reserve2;
  }

  public void setReserve2(String reserve2) {
    this.reserve2 = reserve2;
  }

  public String getReserve3() {
    return reserve3;
  }

  public void setReserve3(String reserve3) {
    this.reserve3 = reserve3;
  }

  public String getEventName() {
    return eventName;
  }

  public void setEventName(String eventName) {
    this.eventName = eventName;
  }

  public String getOperateState() {
    return operateState;
  }

  public void setOperateState(String operateType) {
    this.operateState = operateType;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
;
  /**
   * 无参构造函数
   */
  public DataPlp() {
  }

  /**
   * @param id
   * @param fileName
   * @param eventType
   * @param createTime
   * @param region
   * @param fileTime
   * @param status
   * @param fileConfig
   * @param approveTime
   * @param photoByte
   * @param points
   * @param approver
   * @param menuId
   * @param cityName
   * @param userId
   * @param security
   * @param description
   * @param submitor    有参构造函数
   */
  public DataPlp(Integer id, String fileName, String eventType, Integer security, Date createTime, String region, String fileTime,
                 String description, Integer userId, String approver, String status, String fileConfig, Date approveTime,
                 String points, Integer menuId, String cityName, byte[] photoByte, String submitor, String[] timeRange, String td_start, String td_end) {
    this.id = id;
    this.fileName = fileName;
    this.eventType = eventType;
    this.security = security;
    this.createTime = createTime;
    this.region = region;
    this.fileTime = fileTime;
    this.description = description;
    this.userId = userId;
    this.approver = approver;
    this.status = status;
    this.fileConfig = fileConfig;
    this.approveTime = approveTime;
    this.photoByte = photoByte;
    this.points = points;
    this.menuId = menuId;
    this.cityName = cityName;
    this.submitor = submitor;
    this.timeRange = timeRange;
    this.td_end = td_end;
    this.td_start = td_start;
  }

  /**
   * 获取时间数组(时间段)
   *
   * @return timeRange - 时间数组
   */
  public String[] getTimeRange() {
    return timeRange;
  }

  /**
   * 设置时间数组(时间段)
   *
   * @param timeRange -时间数组
   */
  public void setTimeRange(String[] timeRange) {
    this.timeRange = timeRange;
  }

  /**
   * 获取开始时间
   *
   * @return startTime - 开始时间
   */
  public String getStartTime() {
    return startTime;
  }

  /**
   * 设置开始时间
   *
   * @param startTime - 开始时间
   */
  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  /**
   * 获取结束时间
   *
   * @return endTime - 结束时间
   */
  public String getEndTime() {
    return endTime;
  }

  /**
   * 设置结束时间
   *
   * @param endTime - 结束时间
   */
  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }

  /**
   * 获取结束时间
   *
   * @return td_start - 今日开始时间
   */
  public String getTd_start() {
    return td_start;
  }

  /**
   * 设置结束时间
   *
   * @param td_start - 今日开始时间
   */
  public void setTd_start(String td_start) {
    this.td_start = td_start;
  }

  /**
   * 获取结束时间
   *
   * @return td_end - 今日结束时间
   */
  public String getTd_end() {
    return td_end;
  }

  /**
   * 设置结束时间
   *
   * @param td_end - 今日结束时间
   */
  public void setTd_end(String td_end) {
    this.td_end = td_end;
  }

  /**
   * 获取用户id
   *
   * @return user_id - 用户id
   */
  public Integer getUserId() {
    return userId;
  }

  /**
   * 设置用户id
   *
   * @param userId 用户id
   */
  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  /**
   * 获取数据类型-8:点，线：64,面：128
   *
   * @return event_type - 数据类型-8:点，线：64,面：128
   */
  public String getEventType() {
    return eventType;
  }

  /**
   * 设置数据类型-8:点，线：64,面：128
   *
   * @param eventType 数据类型-8:点，线：64,面：128
   */
  public void setEventType(String eventType) {
    if(null == eventType || StringUtils.isBlank(eventType)){
      return ;
    }
    eventType= eventType.toUpperCase().trim();
    if(eventType.startsWith("POINT") || eventType.equals("8"))
    {
      eventType ="8";
    }
    else if(eventType.startsWith("LINESTRING")|| eventType.equals("64"))
    {
      eventType ="64";
    }
    else if(eventType.startsWith("POLYGON")|| eventType.equals("128"))
    {
      eventType ="128";
    }else {
      eventType = null;
    }
    this.eventType = eventType;
  }

  /**
   * 获取点集wkt格式
   *
   * @return points - 点集wkt格式
   */
  public String getPoints() {
    return points;
  }

  /**
   * 设置点集wkt格式
   *
   * @param points 点集wkt格式
   */
  public void setPoints(String points) {
    this.points = points == null ? null : points.trim();
  }

  /**
   * 获取当前区域
   *
   * @return region - 当前区域
   */
  public String getRegion() {
    return region;
  }

  /**
   * 设置当前区域
   *
   * @param region 当前区域
   */
  public void setRegion(String region) {
    this.region = region == null ? null : region.trim();
  }

  /**
   * 获取上传创建时间
   *
   * @return create_time - 上传创建时间
   */
  public Date getCreateTime() {
    return createTime;
  }

  /**
   * 设置上传创建时间
   *
   * @param createTime 上传创建时间
   */
  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  /**
   * 获取数据描述
   *
   * @return description - 数据描述
   */
  public String getDescription() {
    return description;
  }

  /**
   * 设置数据描述
   *
   * @param description 数据描述
   */
  public void setDescription(String description) {
    this.description = description == null ? null : description.trim();
  }

  public void setDescribe(String dse){
    this.description =dse;
  }
  /**
   * 编目id
   *
   * @return menuId - 编目id
   */
  public Integer getMenuId() {
    return menuId;
  }

  /**
   * 设置编目id
   *
   * @param menuId 编目id
   */
  public void setMenuId(Integer menuId) {
    this.menuId = menuId;
  }

  /**
   * 获取预留字段2
   *
   * @return cityName - 预留字段2
   */
  public String getCityName() {
    return cityName;
  }

  /**
   * 设置预留字段2
   *
   * @param cityName 预留字段2
   */
  public void setCityName(String cityName) {
    this.cityName = cityName == null ? null : cityName.trim();
  }

  /**
   * 获取唯一标识
   *
   * @return id - 唯一标识
   */
  public Integer getId() {
    return id;
  }

  /**
   * 设置唯一标识
   *
   * @param id 唯一标识
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * 获取数据安全等级
   *
   * @return security - 数据安全等级
   */
  public Integer getSecurity() {
    return security;
  }

  /**
   * 设置数据安全等级
   *
   * @param security 数据安全等级
   */
  public void setSecurity(Integer security) {
    this.security = security;
  }

  /**
   * 获取审批人
   *
   * @return approver - 审批人
   */
  public String getApprover() {
    return approver;
  }

  /**
   * 设置审批人
   *
   * @param approver 审批人
   */
  public void setApprover(String approver) {
    this.approver = approver == null ? null : approver.trim();
  }

  /**
   * 获取审批时间
   *
   * @return approve_time - 审批时间
   */
  public Date getApproveTime() {
    return approveTime;
  }

  /**
   * 设置审批时间
   *
   * @param approveTime 审批时间
   */
  public void setApproveTime(Date approveTime) {
    this.approveTime = approveTime;
  }

  /**
   * 获取审批状态
   *
   * @return status - 审批状态
   */
  public String getStatus() {
    return status;
  }

  /**
   * 设置审批状态
   *
   * @param status 审批状态
   */
  public void setStatus(String status) {
    this.status = status == null ? null : status.trim();
  }

  /**
   * 获取数据标识
   *
   * @return file_config - 数据标识
   */
  public String getFileConfig() {
    return fileConfig;
  }

  /**
   * 设置数据标识
   *
   * @param fileConfig 数据标识
   */
  public void setFileConfig(String fileConfig) {
    this.fileConfig = fileConfig == null ? null : fileConfig.trim();
  }

  /**
   * 获取数据名称
   *
   * @return file_name - 数据名称
   */
  public String getFileName() {
    return fileName;
  }

  /**
   * 设置数据名称
   *
   * @param fileName 数据名称
   */
  public void setFileName(String fileName) {
    this.fileName = fileName == null ? null : fileName.trim();
  }

  /**
   * 获取数据所属年份
   *
   * @return file_time - 数据所属年份
   */
  public String getFileTime() {
    return fileTime;
  }

  /**
   * 设置数据所属年份
   *
   * @param fileTime 数据所属年份
   */
  public void setFileTime(String fileTime) {
    this.fileTime = fileTime == null ? null : fileTime.trim();
  }

  /**
   * 获取照片流
   *
   * @return photo_byte - 照片流
   */
  public byte[] getPhotoByte() {
    return photoByte;
  }

  /**
   * 设置照片流
   *
   * @param photoByte 照片流
   */
  public void setPhotoByte(byte[] photoByte) {
    this.photoByte = photoByte;
  }

  /**
   * 获取提交用户
   *
   * @return submitor - 提交用户
   */
  public String getSubmitor() {
    return submitor;
  }

  /**
   * 设置提交用户
   *
   * @param submitor 提交用户
   */
  public void setSubmitor(String submitor) {
    this.submitor = submitor == null ? null : submitor.trim();
  }

  public byte[] getShot() {
    return shot;
  }

  public void setShot(byte[] shot) {
    this.shot = shot;
  }
}
