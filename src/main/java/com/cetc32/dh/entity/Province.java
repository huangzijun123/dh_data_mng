package com.cetc32.dh.entity;

import javax.persistence.*;

public class Province {
    private String fid;

    @Column(name = "province_code")
    private String provinceCode;

    private String province;

    private String type;

    @Column(name = "orig_fid")
    private String origFid;

    @Column(name = "point_x")
    private String pointX;

    @Column(name = "point_y")
    private String pointY;

    /**
     * @return fid
     */
    public String getFid() {
        return fid;
    }

    /**
     * @param fid
     */
    public void setFid(String fid) {
        this.fid = fid == null ? null : fid.trim();
    }

    /**
     * @return province_code
     */
    public String getProvinceCode() {
        return provinceCode;
    }

    /**
     * @param provinceCode
     */
    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode == null ? null : provinceCode.trim();
    }

    /**
     * @return province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province
     */
    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * @return orig_fid
     */
    public String getOrigFid() {
        return origFid;
    }

    /**
     * @param origFid
     */
    public void setOrigFid(String origFid) {
        this.origFid = origFid == null ? null : origFid.trim();
    }

    /**
     * @return point_x
     */
    public String getPointX() {
        return pointX;
    }

    /**
     * @param pointX
     */
    public void setPointX(String pointX) {
        this.pointX = pointX == null ? null : pointX.trim();
    }

    /**
     * @return point_y
     */
    public String getPointY() {
        return pointY;
    }

    /**
     * @param pointY
     */
    public void setPointY(String pointY) {
        this.pointY = pointY == null ? null : pointY.trim();
    }
}