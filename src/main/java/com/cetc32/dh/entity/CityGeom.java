/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/9/15 下午2:07
 ******************************************************************************/
package com.cetc32.dh.entity;

import com.cetc32.dh.common.bean.TreeNode;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "city_geom")
public class CityGeom extends NumberS implements TreeNode {

    private String city;

    private String geom;

    @Id
    @Column(name = "city_code")
    private String cityCode;

    @Column( name ="province_code")
    private String provinceCode;

    @Column( name ="province")
    private String province;

    @Column( name ="polygon_text")
    private String polygonText;

    public String getGeom() {
        return geom;
    }

    public void setGeom(String geom) {
        this.geom = geom;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = formatStr(city);
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String city_code) {
        this.cityCode = formatStr(city_code);
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String province_code) {
        this.provinceCode = formatStr(province_code);
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPolygonText() {
        return polygonText;
    }

    public void setPolygonText(String polygon_text) {
        this.polygonText = polygon_text;
    }

    @Override
    public String getKey() {
        return cityCode;
    }

    @Override
    public String getValue() {
        return city;
    }

    @Override
    public String getTitle() {
        return city;
    }

    @Override
    public String getParentKey() {
        return provinceCode;
    }

    protected String formatStr(String str){
        if(StringUtils.isBlank(str) )
            return null;
        str =str.trim();
        if(str.equals("%%")||str.equals("%")|| StringUtils.isBlank(str.replaceAll("%",""))){
            return null;
        }
        return str;
    }
}
