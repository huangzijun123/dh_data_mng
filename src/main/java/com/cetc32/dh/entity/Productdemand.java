package com.cetc32.dh.entity;

import java.util.Date;
import java.util.List;

public class Productdemand extends NumberS{
    private Integer id;

    private String name;

    private String demandClassify;

    private Date demandyear;

    private String taskdocument;        //修改数据库，对应区域（region)字段

    private String starttimefront;

    private String endtimefront;

    private Date starttime;

    private Date endtime;

    private String status;

    private String approver;

    private Integer demandid;

    private String creator;

    private List<String> fronttime;

    private String flowresult;

    private Date creattime;

    private Date approvtime;

    private String area;

    private String reason;

    private Date changetime;

    private String tasknum;

    private String taskdetail;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDemandClassify() {
        return demandClassify;
    }

    public void setDemandClassify(String demandClassify) {
        this.demandClassify = demandClassify;
    }

    public Date getDemandyear() {
        return demandyear;
    }

    public void setDemandyear(Date demandyear) {
        this.demandyear = demandyear;
    }

    public String getTaskdocument() {
        return taskdocument;
    }

    public void setTaskdocument(String taskdocument) {
        this.taskdocument = taskdocument;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public Integer getDemandid() {
        return demandid;
    }

    public void setDemandid(Integer demandid) {
        this.demandid = demandid;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getStarttimefront() {
        return starttimefront;
    }

    public void setStarttimefront(String starttimefront) {
        this.starttimefront = starttimefront;
    }

    public String getEndtimefront() {
        return endtimefront;
    }

    public void setEndtimefront(String endtimefront) {
        this.endtimefront = endtimefront;
    }


    public List<String> getFronttime() {
        return fronttime;
    }

    public void setFronttime(List<String> fronttime) {
        this.fronttime = fronttime;
    }

    public String getFlowresult() {
        return flowresult;
    }

    public void setFlowresult(String flowresult) {
        this.flowresult = flowresult;
    }

    public Date getCreattime() {
        return creattime;
    }

    public void setCreattime(Date creattime) {
        this.creattime = creattime;
    }

    public Date getApprovtime() {
        return approvtime;
    }

    public void setApprovtime(Date approvtime) {
        this.approvtime = approvtime;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Date getChangetime() {
        return changetime;
    }

    public void setChangetime(Date changetime) {
        this.changetime = changetime;
    }

    public String getTaskNum() {
        return tasknum;
    }

    public void setTaskNum(String taskNum) {
        this.tasknum = taskNum;
    }

    public String getTaskDetail() {
        return taskdetail;
    }

    public void setTaskDetail(String taskDetail) {
        this.taskdetail = taskDetail;
    }
}