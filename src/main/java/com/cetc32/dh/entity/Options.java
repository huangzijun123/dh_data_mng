/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.entity;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * 数据管理Options实体类
 *
 * @author: xiao
 * @version: 1.0
 * @date: 2020/10/14
 * 备注:无
 */
public class Options {
  /**
   * 自增id
   */
  @Id
  private Integer id;

  /**
   * 名称
   */
  @Column(name = "value_name")
  private String valueName;

  /**
   * 名称值
   */
  private String value;

  /**
   * 数据类别
   */
  private String category;

  /**
   * 获取自增id
   *
   * @return id
   */
  public Integer getId() {
    return id;
  }

  /**
   * 设置自增id
   *
   * @param id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * 获取名称
   *
   * @return valueName 名称
   */
  public String getValueName() {
    return valueName;
  }

  /**
   * 设置名称
   *
   * @param valueName 名称
   */
  public void setValueName(String valueName) {
    this.valueName = valueName == null ? null : valueName.trim();
  }

  /**
   * 获取名称值
   *
   * @return value 名称值
   */
  public String getValue() {
    return value;
  }

  /**
   * 设置名称值
   *
   * @param value 名称值
   */
  public void setValue(String value) {
    this.value = value == null ? null : value.trim();
  }

  /**
   * 获取数据类别
   *
   * @return category 数据类别
   */
  public String getCategory() {
    return category;
  }

  /**
   * 设置数据类别
   *
   * @param category 数据类别
   */
  public void setCategory(String category) {
    this.category = category == null ? null : category.trim();
  }
}
