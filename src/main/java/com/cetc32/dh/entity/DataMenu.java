/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @Title: DataTrace
 * @Description: 数据编目实体
 * @author: youqing
 * @version: 1.0
 * @date: 2018/11/21 13:43
 */
@Table(name = "data_menu")
public class DataMenu implements Serializable {

    /**
     * id
     */
    @Id
    private Long id;

    /**
     * 数据管理编目名称
     */
    @Column(name = "menu_name")
    private String menuName;

    /**
     * 父编目ID
     */
    private Long pid;

    /**
     * 编目信息描述
     */
    private String discription;

    /**
     * 编目路径URL
     */
    private String url;

    /**
     * 编目键
     */
    private String key;

    /**
     * 图标类型icon
     */
    private String icon;

    /**
     * 数据管理编目前端页面路径
     */
    @Column(name = "html_url")
    private String htmlUrl;

    /**
     * 编目允许变更信息
     */
    private Boolean disabled;

    /**
     * 编目允许变更信息
     */
    private Boolean addkids;

    @Column(name = "tags")
    private String tags;

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    /**
     * 无参构造函数
     */
    public DataMenu() {
    }

    /**
     * @param id
     * @param menuName
     * @param pid
     * @param discription
     * @param url
     * @param icon
     * @param key
     * @param addkids     有参构造函数
     */
    public DataMenu(Long id, String menuName, Long pid, String discription, String url, String htmlUrl,
                    Boolean disabled, String icon, String key, Boolean addkids) {
        this.id = id;
        this.menuName = menuName;
        this.pid = pid;
        this.discription = discription;
        this.url = url;
        this.htmlUrl = htmlUrl;
        this.disabled = disabled;
        this.icon = icon;
        this.key = key;
        this.addkids = addkids;
    }

    /**
     * 获取编目id
     *
     * @return 编目id
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置编目id
     *
     * @param id 编目id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取数据管理编目名称
     *
     * @return menu_name - 数据管理编目名称
     */
    public String getMenuName() {
        return menuName;
    }

    /**
     * 设置数据管理编目名称
     *
     * @param menuName 数据管理编目名称
     */
    public void setMenuName(String menuName) {
        this.menuName = menuName == null ? null : menuName.trim();
    }

    /**
     * 获取父编目ID
     *
     * @return pid - 父编目ID
     */
    public Long getPid() {
        return pid;
    }

    /**
     * 设置父编目ID
     *
     * @param pid 父编目ID
     */
    public void setPid(Long pid) {
        this.pid = pid;
    }

    /**
     * 获取信息描述
     *
     * @return discription
     */
    public String getDiscription() {
        return discription;
    }

    /**
     * 设置信息描述
     *
     * @param discription 信息描述
     */
    public void setDiscription(String discription) {
        this.discription = discription == null ? null : discription.trim();
    }

    /**
     * 获取图表类型icon
     *
     * @return icon - 编目icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 设置图表类型icon
     *
     * @param icon 编目icon
     */
    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    /**
     * 获取编目URL
     *
     * @return url - 编目URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置编目URL
     *
     * @param url 编目URL
     */
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    /**
     * 获取编目URL
     *
     * @return key - 编目键key
     */
    public String getKey() {
        return key;
    }

    /**
     * 设置编目键key
     *
     * @param key 编目键key
     */
    public void setKey(String key) {
        this.key = key == null ? null : key.trim();
    }

    /**
     * 获取数据管理数据管理编目前端页面路径
     *
     * @return html_url - 数据管理数据管理编目前端页面路径
     */
    public String getHtmlUrl() {
        return htmlUrl;
    }

    /**
     * 设置数据管理数据管理编目前端页面路径
     *
     * @param htmlUrl 数据管理数据管理编目前端页面路径
     */
    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl == null ? null : htmlUrl.trim();
    }

    /**
     * 获取编目允许变更信息
     *
     * @return disabled - 编目允许变更信息
     */
    public Boolean getDisabled() {
        return disabled;
    }

    /**
     * 设置编目允许变更信息
     *
     * @param disabled 编目允许变更信息
     */
    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }


    /**
     * 获取前端编目可更改信息
     *
     * @return icon - 前端编目更改信息
     */
    public Boolean getAddkids() {
        return addkids;
    }

    /**
     * 设置前端编目更改信息
     *
     * @param addkids 前端页编目更改信息
     */
    public void setAddkids(Boolean addkids) {
        this.addkids = addkids;
    }


   /* public String toString() {
        return "{" +
                "id=" + id +'\'' +
//                "menuName=" + menuName +
//                ", pid='" + pid + '\'' +
//                ", discription=" + discription+'\'' +
//                ", url=" + url +
                '}';
    }*/
}

