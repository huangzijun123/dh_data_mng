package com.cetc32.dh.entity;

import org.apache.commons.lang3.StringUtils;

public class CityInfo extends CityGeom{
    String cityPhonePrefix;
    String remarks;
    String mergerName;
    String levelType;
    String keyWords;
    public void setId(String id){
        setCityCode(id);
    }

    public String getId(){
        return getCityCode();
    }

    public void setParentId(String parentId){
        setProvinceCode(parentId);
    }

    public String getParentId(){
        return getProvinceCode();
    }

    public String getCityPhonePrefix() {
        return cityPhonePrefix;
    }

    public void setCityPhonePrefix(String cityPhonePrefix) {
        this.cityPhonePrefix = formatStr(cityPhonePrefix);
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = formatStr(remarks);
    }

    public String getLevelType() {
        return levelType;
    }

    public void setLevelType(String levelType) {
        this.levelType = levelType;
    }

    public String getMergerName() {
        return mergerName;
    }

    public void setMergerName(String mergerName) {
        this.mergerName = formatStr(mergerName);
    }

    public void setKeyWords(String keyWords){
        this.keyWords = formatStr(keyWords);
    }
    public String getKeyWords(){
        if(StringUtils.isNotBlank(keyWords))
            return "%"+keyWords +"%";
        return null;
    }
}
