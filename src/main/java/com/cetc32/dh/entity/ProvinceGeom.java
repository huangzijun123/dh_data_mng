package com.cetc32.dh.entity;

import javax.persistence.*;

@Table(name = "province_geom")
public class ProvinceGeom {


    private String province;

    @Column(name = "province_code")
    private String provinceCode;

    private Object geom;


  @Column(name = "polygon_text")
  private String polygonText;

  public String getPolygonText() {
    return polygonText;
  }

  public void setPolygonText(String polygonText) {
    this.polygonText = polygonText;
  }


    /**
     * @return province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province
     */
    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    /**
     * @return province_code
     */
    public String getProvinceCode() {
        return provinceCode;
    }

    /**
     * @param provinceCode
     */
    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode == null ? null : provinceCode.trim();
    }

    /**
     * @return geom
     */
    public Object getGeom() {
        return geom;
    }

    /**
     * @param geom
     */
    public void setGeom(Object geom) {
        this.geom = geom;
    }
}
