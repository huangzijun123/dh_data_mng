/**
 * @Description: Servlet初始化
 * @author: youqing
 * @version: 1.0
 * @date: 2020/10/11 10:55
 * 更改描述：
 */
package com.cetc32.dh;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
/**
 * @Title: ServletInitializer
 * @Description:
 * @author: youqing
 * @version: 1.0
 * @date: 2018/10/13 11:19
 */
public class ServletInitializer extends SpringBootServletInitializer {

    /**
     * 指定缓存失效时间
     * @param application
     * @return SpringApplicationBuilder
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(DhApplication.class);
    }

}
