package com.cetc32.dh.utils;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.util.Base64Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;



public class FileUtil {

    @Value("${downPath}")
    String downPath;
    private static Logger logger= LoggerFactory.getLogger(FileUtil.class);
    private static String writePath;
    /**
     * 在basePath下保存上传的文件夹
     *
     * @param basePath
     * @param files
     */
    public static void saveMultiFile(String basePath, MultipartFile[] files) {

        if (files == null || files.length == 0) {
            if (files == null) {
                System.out.println("files为空！！");
                return;
            }
            System.out.println("files长度为0");
            return;
        }
        if (basePath.endsWith(File.separator)) {
            basePath = basePath.substring(0, basePath.length() - 1);
        }
        for (MultipartFile file : files) {
            String filePath = basePath + File.separator + file.getOriginalFilename();
            System.out.println(filePath);
            makeDir(filePath);
            File dest = new File(filePath);
            try {
                file.transferTo(dest);
            } catch (IllegalStateException | IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 确保目录存在，不存在则创建
     *
     * @param filePath
     */
    public static void makeDir(String filePath) {
        if (filePath.lastIndexOf(File.separator) > 0) {
//            String dirPath = filePath.substring(0, filePath.lastIndexOf('/'));
            String dirPath = filePath.substring(0, filePath.lastIndexOf(File.separator));
            File dir = new File(dirPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
        }
    }


    /**
     * 目录文件上传
     *
     * @param files
     * @param filepath
     */
    public static String uploadFile(MultipartFile[] files, String filepath) {
        System.out.println(files.length);
        if (files.length == 0) {
            return null;
        }
        int uploadsSum = 0;
        String FileDir = null;
        for (int i = 0; i < files.length; i++) {
            String flag = "";
            MultipartFile file = files[i];
            String fileName = file.getOriginalFilename().replaceAll("\\\\",File.separator);//获取文件名
            fileName=fileName.replaceAll("/",File.separator);
            String dirName = filepath +File.separator + fileName;
            FileDir = fileName;
            System.out.println(FileDir);
            System.out.println(dirName);
            makeDir(dirName);

            if (true) {        //!file.isEmpty()
                try (BufferedOutputStream out = new BufferedOutputStream(
                        new FileOutputStream(new File(dirName)))) {
                    out.write(file.getBytes());
                    out.flush();
                    flag = "文件上传成功";
                } catch (FileNotFoundException e) {
                    flag = "文件上传失败";
                } catch (IOException e) {
                    flag = "文件上传失败";
                }
            }
            if (flag.equals("文件上传成功")) {
                uploadsSum++;
            }
        }
        if (uploadsSum == files.length) {
            if (FileDir.contains(File.separator)){
                return FileDir.substring(0,FileDir.indexOf(File.separator));
            }
            return FileDir;

        }
        return FileDir+"上传失败";
    }

//filePaths可以为空，为空的话，所有的文件同级压缩，否则以保留原文件夹的组织形式压缩
  public static String fileTozip(List<String> fileNames,List<String> filePaths, String directory,String zipName) {
    String zipFileName = FileUtil.nameAddTime(zipName) + ".zip";
    String strZipPath = directory +File.separator+ zipFileName;
    makeDir(strZipPath);
    File zipFile = new File(strZipPath);
    ZipEntry zipEntry = null;
    ZipOutputStream zipStream = null;
    FileInputStream zipSource = null;
    BufferedInputStream bufferStream = null;
    try {
      //构造最终压缩包的输出流
      zipStream = new ZipOutputStream(new FileOutputStream(zipFile));
      for (int i = 0; i < fileNames.size(); i++)  {
        String fileName = fileNames.get(i);        //从entry获取key
        System.out.println("bbbbbbbbbb"+fileName);
        File file = new File(fileName);
        //TODO:未对文件不存在时进行操作，后期优化。
        if (file.exists()) {
          zipSource = new FileInputStream(file);//将需要压缩的文件格式化为输入流
          /**
           * 压缩条目不是具体独立的文件，而是压缩包文件列表中的列表项，称为条目，就像索引一样这里的name就是文件名,
           * 文件名和之前的重复就会导致文件被覆盖
           */
          if(filePaths!=null&&filePaths.get(i)!=null){
            zipEntry = new ZipEntry(fileNames.get(i).replace(filePaths.get(i), ""));//在压缩目录中文件的名字
          }else{
            zipEntry = new ZipEntry(fileNames.get(i));//在压缩目录中文件的名字
          }
          zipStream.putNextEntry(zipEntry);//定位该压缩条目位置，开始写入文件到压缩包中
          bufferStream = new BufferedInputStream(zipSource, 1024 * 10);
          int read = 0;
          byte[] buf = new byte[1024 * 10];
          while ((read = bufferStream.read(buf, 0, 1024 * 10)) != -1) {
            zipStream.write(buf, 0, read);
          }
        } else {
          deleteDir(strZipPath);
          return file.getName() + "不存在！文件压缩失败！";
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //关闭流
      try {
        if (null != bufferStream) bufferStream.close();
        if (null != zipStream) {
          zipStream.flush();
          zipStream.close();
        }
        if (null != zipSource) zipSource.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    //判断系统压缩文件是否存在：true-把该压缩文件通过流输出给客户端后删除该压缩文件  false-未处理
    if (new File(strZipPath).exists()) {
      return zipFileName;
    }else {
      return  zipName+"不存在，文件压缩失败！";
    }

  }



  /**
     * 文件下载
     *
     * @param response
     * @param filename
     * @param path
     */
    public static void downloadFile(HttpServletResponse response, String filename, String path){
        if (filename != null) {
            FileInputStream is = null;
            BufferedInputStream bs = null;
            OutputStream os = null;
            try {
                File file = new File(path);
                if (file.exists()) {
                    //设置Headers
                    response.setHeader("Content-Type","application/octet-stream");
                    //设置下载的文件的名称-该方式已解决中文乱码问题
                    response.setHeader("Content-Disposition","attachment;filename=" +  new String( filename.getBytes("gb2312"), "ISO8859-1" ));
                    is = new FileInputStream(file);
                    bs =new BufferedInputStream(is);
                    os = response.getOutputStream();
                    byte[] buffer = new byte[1024];
                    int len = 0;
                    while((len = bs.read(buffer)) != -1){
                        os.write(buffer,0,len);
                    }
                }else{
                    String error = Base64Util.encode("下载的文件资源不存在");
                }
            }catch(IOException ex){
                ex.printStackTrace();
            }finally {
                try{
                    if(is != null){
                        is.close();
                    }
                    if( bs != null ){
                        bs.close();
                    }
                    if( os != null){
                        os.flush();
                        os.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }



  /**
   * * 根据文件的起始未知和要读的字节书
   * * @param downloadFileName 要下载的文件名
   * * @param start 起始位置
   * * @param size 要读取的文件大小
   * * @param outputStream 文件输出的位置
   * */
  public static void download(String downloadFileName,long start,long size,OutputStream outputStream) {
    try{
      //缓冲字节输出流
      BufferedOutputStream out=new BufferedOutputStream(outputStream);
      //注意FILEDIR为文件的根目录
      RandomAccessFile raf=new RandomAccessFile(new String(downloadFileName.getBytes("utf-8")), "r");
      write(start, size, raf, out);
    }catch (Exception e) {
      e.printStackTrace();
    }
  }

  /** * 将文件写入输出流
   * * @param start 文件起始位置（例如:RandomAccessFile.seek(5)是在第六的字节开始读的）
   * * @param size 文件的大小
   * * @param raf 随机输入流
   * * @param out 输出流 */
  private static void write(long start,long size,RandomAccessFile raf,BufferedOutputStream out){
    try {
      byte[] buffer=new byte[1024];
      int byteRead=-1;
      long readLength=0;
      raf.seek(start);
      while(readLength<size-1024){
        byteRead=raf.read(buffer, 0, 1024);
        readLength+=1024;
        out.write(buffer,0,byteRead);
      }
      if (readLength<=size) {
        byteRead=raf.read(buffer, 0, (int)(size-readLength));
        out.write(buffer,0,byteRead);
      }
      out.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }finally {
      try{
        if (raf!=null) {
          raf.close();
        }
      }catch (IOException ex) {

      }
      try {
        if (out!=null) {
          out.close();
        }
      } catch (IOException ex) {

      }
    }
  }


  public static void doBreakDown(String downloadFileName,HttpServletRequest request,HttpServletResponse response) throws IOException{
    //要下下载的文件名
    String name=downloadFileName.substring(downloadFileName.lastIndexOf(File.separator));
    response.reset();
    //告诉客户端支持的下载方式
    response.setHeader("Accept-Ranges", "bytes");
    String[] sizes=getLength(request, response);
    File file=new File(downloadFileName);
    long start=0L;
    long size=file.length();
    if (sizes==null) {
      //如果为空则Range的值为空，即没有指定下载哪一段文件
      size=file.length();
    }else if (sizes.length==1) {
      start=Long.parseLong(sizes[0])-1L;
      size=size-Long.parseLong(sizes[0]+1L);
      String contentRange = new StringBuffer(request.getHeader("Range")).append("/").append(size).toString();
      contentRange = contentRange.replaceAll("=", " ");
      response.setHeader("Content-Range", contentRange);

    }else if (sizes.length==2) {
      //因为RandomAccessFile.seek()是从start的下一个字节开始读的，所以需要-1
      start = Long.parseLong(sizes[0]) - 1L;
      String contentRange = new StringBuffer(request.getHeader("Range")).append("/").append(size).toString();;
      contentRange = contentRange.replaceAll("=", " ");
      response.setHeader("Content-Range", contentRange);
      size = Long.parseLong(sizes[1]) - start + 1L;
    }else {
      return;
    }
    //下载的文件名
    response.setHeader("Content-disposition", String.format("attachment; filename=\"%s\"", name));
    //告诉客户端文件的大小
    response.addHeader("Content-Length", String.valueOf(size));
    FileUtil.download(downloadFileName, start, size, response.getOutputStream());
  }


  private static String[] getLength(HttpServletRequest request, HttpServletResponse response){
    String[] sizes = null;
    String range = request.getHeader("Range");
    System.out.println("客户端请求:	"+range);
    if (range != null && !"".equals(range.trim())) {
      //SC_PARTIAL_CONTENT 状态码(206)说明服务器已经完成了部分GET请求的资源。
      response.setStatus(javax.servlet.http.HttpServletResponse.SC_PARTIAL_CONTENT);
      range = range.replaceAll("bytes=", "").trim();
      sizes = range.split("-");
    }
    return sizes;
  }






    /**
     * 判断是否为目录
     * @param file
     * @return boolean
     */
    public static boolean isDirectory(String file) {
        File f = new File(file);
        return f.isDirectory();
    }


    /**
     * 目录重命名，提供目录的完整路径
     * @param old_dir
     * @param new_dir
     * @return boolean
     */
    public static boolean renameTo(String old_dir,String new_dir) {
        File old_File = new File(old_dir);
        return old_File.renameTo(new File(new_dir));

    }


  public static String nameAdapt(String name) {
      String new_name = name;
      new_name=new_name.replace("\\",File.separator);
      new_name=new_name.replace("/",File.separator);
      return new_name;
  }


    /**
     * 拷贝目录文件到新路径
     * @param oldPath
     * @param newPath
     * @return boolean
     */
    public static boolean copyFolder(String oldPath, String newPath) {

        try {
            (new File(newPath)).mkdirs(); //如果文件夹不存在 则建立新文件夹
            File a = new File(oldPath);
            String[] file = a.list();
            File temp = null;
            for (int i = 0; i < file.length; i++) {
                if(oldPath.endsWith(File.separator)){
                    temp=new File(oldPath+file[i]);
                    System.out.println("#################################");
                    System.out.println(temp);
                }
                else{
                    temp=new File(oldPath+File.separator+file[i]);
                    System.out.println("#################################");
                    System.out.println(temp);
                }

                if(temp.isFile()){
                    FileInputStream input = new FileInputStream(temp);
                    FileOutputStream output = new FileOutputStream(newPath +File.separator+
                            (temp.getName()).toString());
                    byte[] b = new byte[1024 * 5];
                    int len;
                    while ( (len = input.read(b)) != -1) {
                        output.write(b, 0, len);
                    }
                    output.flush();
                    output.close();
                    input.close();
                }
                if(temp.isDirectory()){//如果是子文件夹
                    copyFolder(oldPath+File.separator+file[i],newPath+File.separator+file[i]);
                }
            }
            System.out.println("复制整个文件夹内容操作成功");
            return true;
        }
        catch (Exception e) {
            System.out.println("复制整个文件夹内容操作出错");
            e.printStackTrace();
            return false;

        }
    }

    /**
     * 删除目录
     * @param dirPath
     */
    public static void deleteDir(String dirPath)
    {
        File file = new File(dirPath);
        if(file.isFile())
        {
            file.delete();
        }else
        {
            File[] files = file.listFiles();
            if(files == null)
            {
                file.delete();
            }else
            {
                for (int i = 0; i < files.length; i++)
                {
                    deleteDir(files[i].getAbsolutePath());
                }
                file.delete();
            }
        }
    }

    /**
     * 将文件内容写到新的文件中
     * @param fileName
     * @param savepath
     * @return String
     */
    public static String writeFile(String fileName, String savepath) {
        System.out.println(fileName);
//        File readerfile = new File(fileName);
        String readerfile = fileName;
        String outFile = savepath +  File.separator +fileName.split(File.separator)[fileName.split(File.separator).length-1];
        makeDir(outFile);
//        File writerfile = new File(outFile);
        String writerfile =outFile;

        FileInputStream fileInputStream = null;
        BufferedInputStream bufferedInputStream = null;
        FileOutputStream fileOutputStream = null;
        BufferedOutputStream bufferedOutputStream = null;
        try {
            // 将读取文件对象加载进字符读取对象中
            fileInputStream = new FileInputStream(readerfile);
            // 将字符读取对象加载进字符读取缓冲流对象
            bufferedInputStream = new BufferedInputStream(fileInputStream);
            // 将写入的文件对象加载进字符写入对象中
            fileOutputStream = new FileOutputStream(writerfile);
            // 将字符写入对象加载进字符写入缓冲流对象中
            bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            // 按行获取读取缓冲流中的数据
            int bytes=-1;

            while ((bytes = bufferedInputStream.read())!=-1) {
                bufferedOutputStream.write(bytes);
            }
            return (fileName.split(File.separator)[fileName.split(File.separator).length-1]+"下载成功！");
        } catch (IOException e) {
            e.printStackTrace();
            return fileName.split(File.separator)[fileName.split(File.separator).length-1]+"下载失败！";
        } finally {
            // 关闭文件流对象
            try {
                if (bufferedInputStream != null)
                    bufferedInputStream.close();
                if (bufferedOutputStream != null)
                    bufferedOutputStream.close();
                if (fileOutputStream != null)
                    fileOutputStream.close();
                if (fileInputStream != null)
                    fileInputStream.close();
            } catch (IOException e2) {
                e2.printStackTrace();
                return fileName.split(File.separator)[fileName.split(File.separator).length-1]+"下载失败！";
            }
        }

    }


    /**
     * 文件名后缀前添加一个时间戳
     */
    public static String  nameAddTime(String fileName) {
        int index = fileName.lastIndexOf(".");
        String nowTimeStr; // 当前时间
        Date date = new Date();
        SimpleDateFormat sif = new SimpleDateFormat("yyyyMMddHHmmss");
        nowTimeStr = sif.format(date);

//      fileName = fileName.substring(0, index) + "_" + nowTimeStr + fileName.substring(index);

        fileName=fileName+"_"+nowTimeStr;

        return fileName;
    }


    /**
     * 获取目录中所有的文件名
     * @param  directoryPath
     */
    public static List<String> getAllFile(String directoryPath) {
        List<String> list = new ArrayList<String>();
        File baseFile = new File(directoryPath);
        if (baseFile.isFile() || !baseFile.exists()) {
            return list;
        }
        File[] files = baseFile.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                list.addAll(getAllFile(file.getAbsolutePath()));
            } else {
                list.add(file.getAbsolutePath());
            }
        }

        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        System.out.println("###############################!!!!!!!!!");
        return list;
    }

    /**
     * 转换文件大小
     *
     * @param fileS
     * @return
     */
    public static String FormetFileSize(long fileS) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        String wrongSize = "0B";
        if (fileS == 0) {
            return wrongSize;
        }
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format((double) fileS / 1024) + "KB";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format((double) fileS / 1048576) + "MB";
        } else {
            fileSizeString = df.format((double) fileS / 1073741824) + "GB";
        }
        return fileSizeString;
    }



    /*****************code by hubin,start***************************************/

    public static String downloadAllAttachment(String filePath, HttpServletRequest request, HttpServletResponse response) throws Exception {
        File dirFile = new File(filePath) ;
        ArrayList<String> allFilePath = Dir(dirFile);
        List<File> filesList = new ArrayList<>();

        File[] files=new File[allFilePath.size()];
        String path;
        for (int j = 0; j < allFilePath.size(); j++) {
            path=allFilePath.get(j);
            File file = new File(path);
            files[j]=file;
            filesList.add(file);
        }
        return	downLoadFiles(filesList,filePath,request,response);

    }

    //获取文件夹下的所有文件的路径
    public static ArrayList<String> Dir(File dirFile) throws Exception {
        ArrayList<String> dirStrArr = new ArrayList<String>();
        if (dirFile.exists()) {
            //直接取出利用listFiles()把当前路径下的所有文件夹、文件存放到一个文件数组
            File files[] = dirFile.listFiles();
            for (File file : files) {
                //如果传递过来的参数dirFile是以文件分隔符，也就是/或者\结尾，则如此构造
                if (dirFile.getPath().endsWith(File.separator)) {
                    dirStrArr.add(dirFile.getPath() + file.getName());
                } else {
                    //否则，如果没有文件分隔符，则补上一个文件分隔符，再加上文件名，才是路径
                    dirStrArr.add(dirFile.getPath() + File.separator
                            + file.getName());
                }
            }
        }
        return dirStrArr;
    }

    //下载文件
    public static String downLoadFiles(List<File> files,String filePath ,HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            //这里的文件你可以自定义是.rar还是.zip
            filePath = filePath+"打包后.zip";
            File file = new File(filePath);
            if (!file.exists()){
                file.createNewFile();
            }else{
                file.delete();
            }
            response.reset();
            //创建文件输出流
            FileOutputStream fous = new FileOutputStream(file);
            /**打包的方法用到ZipOutputStream这样一个输出流,所以这里把输出流转换一下*/
            ZipOutputStream zipOut = new ZipOutputStream(fous);
            /**这个方法接受的就是一个所要打包文件的集合，还有一个ZipOutputStream*/
            zipFiles(files, zipOut);
            zipOut.close();
            fous.close();
            return filePath;

        }catch (Exception e) {
            e.printStackTrace();
            //return "文件下载出错" ;
        }
        return "文件下载出错";
    }

    //把接受的全部文件打成压缩包
    public static void zipFiles(List files,ZipOutputStream outputStream) {

        int size = files.size();

        for(int i = 0; i < size; i++) {

            File file = (File) files.get(i);

            zipFile(file, outputStream);
        }
    }

    //将单个文件打包
    public static void zipFile(File inputFile, ZipOutputStream ouputStream) {

        try {
            if (inputFile.exists()) {
                if (inputFile.isFile()) {
                    FileInputStream IN = new FileInputStream(inputFile);
                    BufferedInputStream bins = new BufferedInputStream(IN, 512);
                    //org.apache.tools.zip.ZipEntry
                    ZipEntry entry = new ZipEntry(inputFile.getName());
                    ouputStream.putNextEntry(entry);
                    // 向压缩文件中输出数据
                    int nNumber;
                    byte[] buffer = new byte[512];
                    while ((nNumber = bins.read(buffer)) != -1) {
                        ouputStream.write(buffer, 0, nNumber);
                    }
                    //关闭创建的流对象
                    bins.close();
                    IN.close();
                } else {
                    try {
                        File[] files = inputFile.listFiles();
                        for (int i = 0; i < files.length; i++) {
                            zipFile(files[i], ouputStream);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    public static void saveFile(String data,String abslutePath) throws RuntimeException{
        if(abslutePath==null ||abslutePath.trim().length()==0){
            logger.error("Error File Path to keep set");

        }
        File folder=new File(abslutePath.substring(0,abslutePath.lastIndexOf(File.separator)));
        folder.mkdirs();
        FileWriter fileWriter = null;
        PrintWriter printWriter =null;
        try{
            File txtFile = new File(abslutePath);
            fileWriter = new FileWriter(txtFile,false);
            printWriter = new PrintWriter(fileWriter);
            printWriter.println(new String(data.getBytes(),"utf-8"));
            printWriter.flush();
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
            if(null!= printWriter)
                printWriter.close();
            try{
                if(null!=fileWriter)
                    fileWriter.close();
            }catch (Exception e1){
                e1.printStackTrace();
            }
            throw  new RuntimeException("File Write Error");
        }

    }

    /**
     * 写入文件
     *
     * @param value,resolvePath
     *         写入的字段，路径
     * @return 无返回值
     */
    public static void wirteText(String value,String resolvePath) throws IOException{
        saveFile(value,resolvePath);
    }

    /**
     * 读入文件
     *
     * @param loadPath
     *         读入的路径
     * @return 返回读取的字段
     */
    public static String readText(String loadPath) throws IOException{
        if(loadPath==null ||loadPath.trim().length()==0){
            logger.error("Error File Path to load set");
            return null;
        }
        File file = new File(loadPath);
        String fileContent = "";
        if(file.isFile()&&file.exists()){
            InputStreamReader read = new InputStreamReader(new FileInputStream(file),"UTF-8");
            BufferedReader reader =new BufferedReader(read);
            String line;
            while ((line=reader.readLine())!=null){
                fileContent+=line+"\r\n";
            }
            read.close();
        }
        return fileContent;

    }
    /*****************code by hubin,end***************************************/
}

