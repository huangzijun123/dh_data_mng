package com.cetc32.dh.mybatis;



import com.cetc32.dh.dto.DemandSubmitDTO;
import com.cetc32.dh.entity.DemandSubmit;
import com.cetc32.dh.entity.vDemand;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DemandSubmitMapper {

    List<DemandSubmit> findAll();

    int deleteByPrimaryKey(Integer id);

    int insert(DemandSubmit record);

    int insertSelective(DemandSubmit record);

    DemandSubmit selectByPrimaryKey(Integer id);

    List<DemandSubmit> selectByLimit(@Param("offset") Integer offset,@Param("limit") Integer limit);

    int countDemand();

    int updateByPrimaryKeySelective(DemandSubmit record);

    int updateByPrimaryKey(DemandSubmit record);

    List<DemandSubmit> findByKeyWord(String keyword);

    /**
     * 根据ID 更新提交的信息
     * @param name 更新的数据集合，通常demandSubmit一次查询只包含submitor或者approver中的一个。
     * @return 反馈查询到的数据
     * */
    List<DemandSubmit> selectMine(String name);

    /**
     * 根据当前用户统计所有任务个数
     * @param name 通常demandSubmit一次查询只包含submitor或者approver中的一个。
     * @return 反馈查询到的数据个数
     * */
    Integer countMine(String name);

    /**
     * 根据当前用户统计所有任务个数
     * @param demandSubmit 通常demandSubmit一次查询只包含submitor或者approver中的一个。
     * @return 反馈查询到的数据个数
     * */
    Integer countByStatusAndUser(DemandSubmit demandSubmit);

    /**
     * 根据当前用户统计所有任务个数
     * @param demandSubmit 通常demandSubmit一次查询只包含submitor或者approver中的一个。
     * @return 反馈查询到的数据记录
     * */
    List<DemandSubmit> selectByStatusAndUser(DemandSubmit demandSubmit);

    List<DemandSubmit> queryFilesByObj(vDemand vdemand);

    Long  countFilesByObj(vDemand vDemand);

    List<DemandSubmitDTO> searchbystatus(DemandSubmitDTO demandSubmitDTO);

    List<String> alldemandclassify();
}
