/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.mybatis;

import com.cetc32.dh.common.bean.UserAreaPermision;
import com.cetc32.dh.entity.DataFile;
import com.cetc32.webutil.common.bean.LoginUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.Date;
import java.util.List;

/**
 * @Title: DataFileMapper
 * @Description:
 * @author: xiao
 * @version: 1.0
 * @date: 2020/11/21 11:19
 */
@Repository
public interface DataFileMapper extends Mapper<DataFile> {

  /**
   * 统计所有的文件数据个数
   *
   * @return 返回统计结果
   */
  public Integer countAll();

  /**
   * 插入一个文件数据
   *
   * @param dataFile 文件数据
   * @return 返回插入结果
   */
  public Integer insertOne(DataFile dataFile);


  /**
   * 成果数据上报
   *
   * @param dataFile
   * @return Integer
   */
  public Integer insertGain(DataFile dataFile);

  /**
   * 更新一个文件数据
   *
   * @param dataFile 文件数据
   * @return 返回更新结果
   */
  public Integer updateById(DataFile dataFile);

  /**
   * 根据id查询文件数据
   *
   * @param id 文件id
   * @return 返回查询结果
   */
  public DataFile queryById(Long id);

  /**
   * 根据时间查询文件数据
   *
   * @param time 文件时间
   * @return 返回查询结果
   */
  public List<DataFile> queryByTime(Date time);

  /**
   * 根据区域查询文件数据
   *
   * @param region 文件所属区域
   * @return 返回查询结果
   */
  public List<DataFile> queryByRegion(String region);

  /**
   * 根据安全等级查询文件数据
   *
   * @param security 文件安全等级
   * @return 返回查询结果
   */
  public List<DataFile> queryAllByFileSecurity(Integer security);

  /**
   * 根据安状态和用户查询文件数据
   *
   * @param dataFile 文件数据
   * @return 返回查询结果
   */
  public List<DataFile> selectByStatusAndUser(DataFile dataFile);

  /**
   * 根据安全状态和用户统计文件数据
   *
   * @param dataFile 文件数据
   * @return 返回统计结果
   */
  public Integer countByStatusAndUser(DataFile dataFile);

  /**
   * 根据输入条件查询文件数据
   *
   * @param offset   偏移量
   * @param limit    每页显示条数
   * @param dataFile 文件数据
   * @return 返回查询结果
   */
  public List<DataFile> queryFilesByObj(@Param("offset") Integer offset, @Param("limit") Integer limit, DataFile dataFile);

  /**
   * 根据输入条件统计文件数据量
   *
   * @param dataFile 文件数据
   * @return 返回统计结果
   */
  public Integer countFilesByObj(@Param("dataFile") DataFile dataFile);

  /**
   * 根据id删除一个文件数据
   *
   * @param id 文件id
   * @return 返回删除结果
   */
  public Integer deleteById(Long id);

  /**
   * 删除编目menuId下的所有文件数据
   *
   * @param menuId 编目id
   * @return 返回删除结果
   */
  public Integer deleteByMenuId(Integer menuId);

  /**
   * 查询编目menuId下的所有文件数据
   *
   * @param menuId 编目id
   * @return 返回查询结果
   */
  public List<DataFile> queryByMenuId(Integer menuId);

  /**
   * 统计编目menuId下的所有文件数据的大小
   *
   * @param menuId 编目id
   * @return 返回结果
   */
  public Long fileSizeCount(Integer menuId);


  /**
   * 统计编目menuId下的所有文件个数
   *
   * @param menuId 编目id
   * @return 返回结果
   */
  public Integer fileNumCount(Integer menuId);

  /***
   *  基于当前用户的dataFile获取
   * @param dataFile 搜索参数
   * @param user  用户信息
   * @param limit 个数
   * @param offset 偏移量
   * **/
  public List<DataFile> selectFilesHasPermission(@Param("offset") Integer offset, @Param("limit") Integer limit,
                                                 @Param("data")DataFile dataFile, @Param("user")UserAreaPermision user,
                                                 @Param("polygons") List<String > polygons);

  /***
   *  基于当前用户的dataFile个数获取
   * @param dataFile 搜索参数
   * @param user  用户信息
   * **/
  public Long countFilesHasPermission(@Param("data")DataFile dataFile,
                                      @Param("user") UserAreaPermision user,
                                      @Param("polygons") List<String > polygons);
}
