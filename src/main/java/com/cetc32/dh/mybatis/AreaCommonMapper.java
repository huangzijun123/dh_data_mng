package com.cetc32.dh.mybatis;

import com.cetc32.dh.entity.CityGeom;
import com.cetc32.dh.entity.CityInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AreaCommonMapper {
    public Integer insertOrUpdateCity(CityInfo cityInfo);
    public List<CityInfo> selectCityByQuery(CityInfo cityInfo );
    public Integer countCityByQuery(CityInfo cityInfo);
    public Integer deleteById(@Param("id")String id);
}
