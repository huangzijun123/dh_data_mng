package com.cetc32.dh.mybatis;


import com.cetc32.dh.entity.EstimateTask;
import com.cetc32.dh.entity.vEstimate;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstimateTaskMapper {

    List<EstimateTask> findAll();
    int deleteByPrimaryKey(Integer id);

    int insert(EstimateTask record);

    int insertSelective(EstimateTask record);



    EstimateTask selectByPrimaryKey(Integer id);

    List<EstimateTask> selectByLimit(@Param("offset") Integer offset,@Param("limit") Integer limit);

    int countEstimate();


    int updateByPrimaryKeySelective(EstimateTask record);

    int updateByPrimaryKey(EstimateTask record);

    List<EstimateTask> findByKeyWord(String keyword);

    /**
     * 根据ID 更新提交的信息
     * @param name 更新的数据集合，通常demandSubmit一次查询只包含submitor或者approver中的一个。
     * @return 反馈查询到的数据
     * */
    List<EstimateTask> selectMine(String name);

    /**
     * 根据当前用户统计所有任务个数
     * @param name 通常estimateTask一次查询只包含submitor或者approver中的一个。
     * @return 反馈查询到的数据个数
     * */
    Integer countMine(String name);

    /**
     * 根据当前用户统计所有任务个数
     * @param estimateTask 通常estimateTask一次查询只包含submitor或者approver中的一个。
     * @return 反馈查询到的数据个数
     * */
    Integer countByStatusAndUser(EstimateTask estimateTask);

    /**
     * 根据当前用户统计所有任务个数
     * @param estimateTask 通常estimateTask一次查询只包含submitor或者approver中的一个。
     * @return 反馈查询到的数据记录
     * */
    List<EstimateTask> selectByStatusAndUser(EstimateTask estimateTask);

    List<EstimateTask> queryFilesByObj(vEstimate vestimate);

    List<String> allservice(String classify);

    List<String> alltaskclassify();
}