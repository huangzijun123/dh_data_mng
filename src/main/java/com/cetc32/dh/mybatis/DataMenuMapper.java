/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.mybatis;

import com.cetc32.dh.entity.DataMenu;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import java.util.List;

/**
 * @Title: DataMenuMapper
 * @Description:
 * @author: xiao
 * @version: 1.0
 * @date: 2020/11/21 11:19
 */
@Repository
public interface DataMenuMapper extends Mapper<DataMenu> {

  /**
   * 统计所有的编目数据个数
   *
   * @return 返回统计结果
   */
  public Integer countAll();

  /**
   * 插入一个编目数据
   *
   * @param dataMenu 编目数据
   * @return 返回插入结果
   */
  public Integer insertOne(DataMenu dataMenu);

  /**
   * 更新一个编目数据
   *
   * @param dataMenu 编目数据
   * @return 返回更新结果
   */
  public Integer updateById(DataMenu dataMenu);

  /**
   * 根据id查询编目数据
   *
   * @param id 编目id
   * @return 返回查询结果
   */
  public DataMenu queryById(Long id);

  /**
   * 根据id删除一个编目数据
   *
   * @param id 编目id
   * @return 返回删除结果
   */
  public Integer deleteById(Long id);

  /**
   * 查询所有父节点为pid的编目数据
   *
   * @param pid 编目父节点pid
   * @return 返回查询结果
   */
  public DataMenu queryByPid(Long pid);

  /**
   * 查询所有编目数据
   *
   * @return 返回查询结果
   */
  public List<DataMenu> selectAll();

  /**
   * 查询所有以id为父节点的编目数据
   *
   * @return 返回查询结果
   */
  public List<DataMenu> queryByPIdSatisfyId(Long id);

  /**
   * 查询编目键为key编目数据
   *
   * @return 返回查询结果
   */
  public DataMenu queryByKey(String key);
}
