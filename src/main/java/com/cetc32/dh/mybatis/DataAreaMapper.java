/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/9/16 下午1:36
 ******************************************************************************/
package com.cetc32.dh.mybatis;

import com.cetc32.dh.entity.DataArea;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
@Repository
public interface DataAreaMapper {
    public Integer insertArea(List<DataArea> areaList);
    public Integer deleteAreaByDataId(@Param("dataId")Long dataId);
    public List<HashMap<Integer,Integer>> countByAreaCodes(List<String> areaCodes);
}
