/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.mybatis;

import com.cetc32.dh.entity.DataTrace;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import java.util.Date;
import java.util.List;

/**
 * @Title: DataTraceMapper
 * @Description:
 * @author: xiao
 * @version: 1.0
 * @date: 2020/11/21 11:19
 */
@Repository
public interface DataTraceMapper extends Mapper<DataTrace> {

  /**
   * 统计所有的文件数据个数
   *
   * @return 返回统计结果
   */
  public Integer countAll();

  /**
   * 插入一个轨迹数据
   *
   * @param dataTrace 轨迹数据
   * @return 返回插入结果
   */
  public Integer insertOne(DataTrace dataTrace);

  /**
   * 更新一个轨迹数据
   *
   * @param dataTrace 轨迹数据
   * @return 返回更新结果
   */
  public Integer updateById(DataTrace dataTrace);

  /**
   * 删除一个轨迹数据
   *
   * @param id 轨迹数据id
   * @return 返回删除结果
   */
  public Integer deleteById(Integer id);

  /**
   * 根据id查询轨迹数据
   *
   * @param id 轨迹id
   * @return 返回查询结果
   */
  public DataTrace queryById(Integer id);

  /**
   * 根据时间查询轨迹数据
   *
   * @param time 轨迹数据时间
   * @return 返回查询结果
   */
  public List<DataTrace> queryByTime(Date time);

  /**
   * 根据区域查询轨迹数据
   *
   * @param region 轨迹数据所属区域
   * @return 返回查询结果
   */
  public List<DataTrace> queryByRegion(String region);

  /**
   * 根据安全等级查询轨迹数据
   *
   * @param security 轨迹数据安全等级
   * @return 返回查询结果
   */
  public List<DataTrace> queryAllByFileSecurity(Integer security);

  /**
   * 根据安状态和用户查询轨迹数据
   *
   * @param dataTrace 轨迹数据
   * @return 返回查询结果
   */
  public List<DataTrace> selectByStatusAndUser(DataTrace dataTrace);

  /**
   * 根据安全状态和用户统计轨迹数据
   *
   * @param dataTrace 轨迹数据
   * @return 返回统计结果
   */
  public Integer countByStatusAndUser(DataTrace dataTrace);

  /**
   * 根据输入条件查询轨迹数据
   *
   * @param offset    偏移量
   * @param limit     每页显示条数
   * @param dataTrace 轨迹数据
   * @return 返回查询结果
   */
  public List<DataTrace> queryFilesByObj(@Param("offset") Integer offset, @Param("limit") Integer limit, DataTrace dataTrace);

  /**
   * 根据输入条件查询轨迹数据个数
   *
   * @param dataTrace 轨迹数据
   * @return 返回查询结果
   */
  public Integer countFilesByObj(@Param("dataTrace") DataTrace dataTrace);
}
