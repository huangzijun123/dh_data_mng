package com.cetc32.dh.mybatis;

import com.cetc32.dh.entity.Province;
import com.cetc32.dh.entity.ZipFile;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface ZipFileMapper extends Mapper<ZipFile> {
  public Integer insertOne(ZipFile  zipFile);
  public List<ZipFile> queryByUserId(Integer userId) ;
  public ZipFile queryById(Integer id) ;


}
