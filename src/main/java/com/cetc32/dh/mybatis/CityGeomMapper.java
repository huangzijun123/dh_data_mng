/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/9/15 下午2:06
 ******************************************************************************/
package com.cetc32.dh.mybatis;

import com.cetc32.dh.entity.CityGeom;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface CityGeomMapper extends Mapper<CityGeom> {
    public Integer insertOrUpdateCity(CityGeom city);
    public Integer deleteByCityCode(@Param("cityCode")String cityCode);
    public List<CityGeom> selectCityTree(@Param("proCode")String proCode) ;

    public String selectCityInCodes( List<String>  areaCodes);

    List<String> selectCodesInArea(List<String> cities);

    public int updateByName(CityGeom cityGeom);

    public CityGeom queryByName(String name);

    public List<String> queryAreaCodeByCity(@Param("areaName")String areaName);

    public List<String> queryAreaCodeByProvince(@Param("areaName")String areaName);

    public List<String> queryCityNameByProvince(@Param("areaName") String areaName);

    public List<CityGeom> selectCityInPolygon(@Param("polygon")String polygon);

  /**
   * 判断多边形polygon2是否在多边形polygon1中
   *
   * @return 返回判断结果
   * 备注:无
   */
  public Boolean judgePolygonContain(@Param("polygon1") String polygon1, @Param("polygon2") String polygon2);

  /**
   * 判断某个点是否在一个面中
   *
   * @return 返回判断结果
   * 备注:无
   */
  public Boolean judgePointContain(@Param("point") String point, @Param("polygon") String polygon);
}
