package com.cetc32.dh.mybatis;

import com.cetc32.dh.entity.ProvinceGeom;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface ProvinceGeomMapper extends Mapper<ProvinceGeom> {
  public int insertOne(ProvinceGeom provinceGeom);
  public int updateByName(ProvinceGeom provinceGeom);
  public List<ProvinceGeom> selectAll();
  public ProvinceGeom queryByName(String name);
  public Boolean judgePolygonDisjoint(@Param("polygon1") String polygon1, @Param("polygon2") String polygon2);



}
