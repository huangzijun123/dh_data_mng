package com.cetc32.dh.mybatis;

import com.cetc32.dh.entity.Province;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface ProvinceMapper extends Mapper<Province> {

  public List<Province> selectByDistinct() ;

  public List<Province> selectByCode(String code);

}
