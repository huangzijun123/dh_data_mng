/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/10/8 下午4:35
 ******************************************************************************/
package com.cetc32.dh.mybatis;

import com.cetc32.dh.entity.ZQArea;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ZqAreaMapper {
    List<ZQArea> selectByZqId(@Param("ids") List<String> ids);
    List<String> selectAreaByZqId(@Param("ids") List<String> ids);
}
