package com.cetc32.dh.mybatis;

import com.cetc32.dh.entity.Productdemand;
import com.cetc32.dh.entity.vProduct;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductdemandMapper {

    List<Productdemand> findAll();

    int deleteByPrimaryKey(Integer id);

    int insert(Productdemand record);

    int insertSelective(Productdemand record);

    Productdemand selectByPrimaryKey(Integer id);

    List<Productdemand> selectByLimit(@Param("offset") Integer offset,@Param("limit") Integer limit);

    int countProduct();

    int updateByPrimaryKeySelective(Productdemand record);

    int updateByPrimaryKey(Productdemand record);

    List<Productdemand> findByKeyWord(String keyword);

    /**
     * 根据ID 更新提交的信息
     * @param name 更新的数据集合，通常productdemand一次查询只包含submitor或者approver中的一个。
     * @return 反馈查询到的数据
     * */
    List<Productdemand> selectMine(String name);

    /**
     * 根据当前用户统计所有任务个数
     * @param name 通常productdemand一次查询只包含submitor或者approver中的一个。
     * @return 反馈查询到的数据个数
     * */
    Integer countMine(String name);

    /**
     * 根据当前用户统计所有任务个数
     * @param productdemand 通常productdemand一次查询只包含submitor或者approver中的一个。
     * @return 反馈查询到的数据个数
     * */
    Integer countByStatusAndUser(Productdemand productdemand);

    /**
     * 根据当前用户统计所有任务个数
     * @param productdemand 通常productdemand一次查询只包含submitor或者approver中的一个。
     * @return 反馈查询到的数据记录
     * */
    List<Productdemand> selectByStatusAndUser(Productdemand productdemand);

    List<Productdemand> queryFilesByObj(vProduct vproduct);
}