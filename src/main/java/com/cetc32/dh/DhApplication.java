/**
 * @Description: DH项目Application入口
 * @author: youqing
 * @version: 1.0
 * @date: 2020/10/11 10:55
 * 更改描述：
 */
package com.cetc32.dh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;
/**
 * @Title: DhApplication
 * @Description:
 * @author: youqing
 * @version: 1.0
 * @date: 2020/10/13 11:19
 */
@SpringBootApplication(scanBasePackages = {"com.cetc32.dh","com.cetc32.webutil.client"})
@MapperScan(basePackages = "com.cetc32.dh.mybatis")

public class DhApplication {

    public static void main(String[] args) {
        SpringApplication.run(DhApplication.class, args);
    }

}
