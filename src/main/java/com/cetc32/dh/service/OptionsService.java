/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.service;

import com.cetc32.dh.entity.Options;

import java.util.List;

/**
 * @Title: OptionsService
 * @Description:
 * @author: xiao
 * @version: 1.0
 * @date: 2020/11/21 11:19
 */
public interface OptionsService {

  /**
   * 根据数据类别查找数据
   *
   * @param category 数据类别
   * @return 返回查询结果
   */
  public List<Options> selectByCategory(String category);
}
