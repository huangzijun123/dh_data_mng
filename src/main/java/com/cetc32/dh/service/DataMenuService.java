/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.service;

import com.cetc32.dh.dto.DataMenuDTO;
import com.cetc32.dh.entity.DataMenu;

import java.util.List;

/**
 * @Title: DataMenuService
 * @Description:
 * @author: xiao
 * @version: 1.0
 * @date: 2020/11/13 11:19
 */
public interface DataMenuService {

  /**
   * 统计所有的编目数据个数
   *
   * @return 返回统计结果
   */
  public Integer count();

  /**
   * 插入一个编目数据
   *
   * @param dataMenu 编目数据
   * @return 返回插入结果
   */
  public Integer insertDataMenu(DataMenu dataMenu);

  /**
   * 根据id查询编目数据
   *
   * @param id 编目id
   * @return 返回查询结果
   */
  public DataMenu queryById(Long id);

  /**
   * 根据id删除一个编目数据
   *
   * @param id 编目id
   * @return 返回删除结果
   */
  public Integer deleteById(Long id);

  /**
   * 根据父节点pid查询编目
   *
   * @param pid 父节点
   * @return 返回查询结果
   */
  public DataMenu queryByPid(Long pid);

  /**
   * 查询所有以id为父节点的编目
   *
   * @param id 编目id
   * @return 返回查询结果
   */
  public List<DataMenu> queryByPIdSatisfyId(Long id);

  /**
   * 更新编目数据
   *
   * @param dataMenu 编目数据
   * @return 返回更新结果
   */
  public Integer updatebyId(DataMenu dataMenu);

  /**
   * 查询编目id节点下的编目树信息
   *
   * @param id 编目id
   * @return 返回查询结果
   */
  public DataMenuDTO getMenuTree(Long id);

  /**
   * 删除编目id下的树
   *
   * @param id 编目id
   * @return 返回删除结果
   */
  public Integer deleteMenuTree(Long id);

  /**
   * 新增一个编目数据
   *
   * @param dataMenu 编目数据
   * @return 返回新增结果
   */
  public String addDataMenu(DataMenu dataMenu);

  /**
   * 查询所有的编目数据
   *
   * @return 返回查询结果
   */
  public List<DataMenu> selectAll();


  /**
   * 统计编目id下子树中所有节点的数目
   *
   * @param id 编目id
   * @return 返回统计结果
   */
  public Integer countMenuChild(Long id);

  /**
   * 查询编目键为key编目数据
   *
   * @return 返回查询结果
   */
  public DataMenu queryByKey(String key);

}
