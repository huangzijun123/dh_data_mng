package com.cetc32.dh.service;

import com.cetc32.dh.entity.ZipFile;

import java.util.List;

public interface ZipFileService {

  public Integer insertOne(ZipFile zipFile);

  public List<ZipFile> queryByUserId(Integer userId) ;

  public ZipFile queryById(Integer id) ;
}
