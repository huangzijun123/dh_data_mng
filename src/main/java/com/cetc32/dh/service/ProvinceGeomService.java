package com.cetc32.dh.service;

import com.cetc32.dh.entity.ProvinceGeom;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


public interface ProvinceGeomService {
  public int insertOne(ProvinceGeom provinceGeom);
  public int updateByName(ProvinceGeom provinceGeom);
  public List<ProvinceGeom> selectAll();
  public ProvinceGeom queryByName(String name);
  public Boolean judgePolygonDisjoint(String polygon1,String polygon2);
}
