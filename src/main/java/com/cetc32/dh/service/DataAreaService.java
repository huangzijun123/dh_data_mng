/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/9/17 下午4:55
 ******************************************************************************/
package com.cetc32.dh.service;

import com.cetc32.dh.entity.DataArea;

import java.util.HashMap;
import java.util.List;

public interface DataAreaService {
    Integer insertArea(List<DataArea> areaList);
    Integer deleteAreaByDataId(Long dataId);
    Integer updateArea(Long dataId,List<DataArea> list);
    List<HashMap<Integer,Integer> > countByAreaCodes(List<String> areaCode);
}
