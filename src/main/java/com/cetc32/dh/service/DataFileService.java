/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.service;

import com.cetc32.dh.entity.DataFile;

import java.util.Date;
import java.util.List;

/**
 * @Title: DataFileService
 * @Description:
 * @author: youqing
 * @version: 1.0
 * @date: 2020/11/21 11:19
 */
public interface DataFileService {

  /**
   * 统计所有的文件数据个数
   *
   * @return 返回统计结果
   */
  public Integer count();

  /**
   * 插入一个文件数据
   *
   * @param dataFile 文件数据
   * @return 返回插入结果
   */
  public Integer insertDataFile(DataFile dataFile);

  public Integer insertGain(DataFile dataFile);

  /**
   * 更新一个文件数据
   *
   * @param dataFile 文件数据
   * @return 返回更新结果
   */
  public Integer updatebyId(DataFile dataFile);

  /**
   * 根据id删除一个文件数据
   *
   * @param id 文件id
   * @return 返回删除结果
   */
  public Integer deleteById(Long id);

  /**
   * 根据id查询文件数据
   *
   * @param id 文件id
   * @return 返回查询结果
   */
  public DataFile queryById(Long id);

  /**
   * 根据时间查询文件数据
   *
   * @param time 文件时间
   * @return 返回查询结果
   */
  public List<DataFile> queryByTime(Date time);

  /**
   * 根据区域查询文件数据
   *
   * @param region 文件所属区域
   * @return 返回查询结果
   */
  public List<DataFile> queryByRegion(String region);

  /**
   * 根据安全等级查询文件数据
   *
   * @param security 文件安全等级
   * @return 返回查询结果
   */
  public List<DataFile> queryAllByFileSecurity(Integer security);

  /**
   * 根据安状态和用户查询文件数据
   *
   * @param dataFile 文件数据
   * @return 返回查询结果
   */
  public List<DataFile> selectByStatusAndUser(DataFile dataFile);

  /**
   * 根据安全状态和用户统计文件数据
   *
   * @param dataFile 文件数据
   * @return 返回统计结果
   */
  public Integer countByStatusAndUser(DataFile dataFile);

  /**
   * 根据输入条件查询文件数据
   *
   * @param offset   偏移量
   * @param limit    每页显示条数
   * @param dataFile 文件数据
   * @return 返回查询结果
   */
  public List<DataFile> queryFilesByObj(Integer offset, Integer limit, DataFile dataFile);

  /**
   * 根据输入条件统计文件数据量
   *
   * @param dataFile 文件数据
   * @return 返回统计结果
   */
  public Integer countFilesByObj(DataFile dataFile);

  /**
   * 删除编目menuId下的所有文件数据
   *
   * @param menuId 编目id
   * @return 返回删除结果
   */
  public Integer deleteByMenuId(Integer menuId);


  /**
   * 查询编目menuId下的所有文件数据
   *
   * @param menuId 编目id
   * @return 返回查询结果
   */
  public List<DataFile> queryByMenuId(Integer menuId);

  /**
   * 统计编目menuId下的所有文件数据的大小
   *
   * @param menuId 编目id
   * @return 返回结果
   */
  public Long fileSizeCount(Integer menuId);


  /**
   * 统计编目menuId下的所有文件个数
   *
   * @param menuId 编目id
   * @return 返回结果
   */
  public Integer fileNumCount(Integer menuId);

//    /**
//     * 根据时间和状态（提交或审批）
//     * @param date 日期
//     * @param status 状态
//     * @return 返回结果
//     * */
//    public Integer countTodayData(String date,String status);
  /**
   * 根据dataFile实体获取对应DataFile
   * @param pageNum  页码
   * @param limit 请求的资源数量
   * @param data 请求数据
   * @return 反馈查询到的数据
   * **/
  public List<DataFile> selectAuthorizedData(Integer limit ,Integer pageNum,DataFile data);

  /**
   * 根据dataFile实体获取对应DataFile
   * @param data 请求数据
   * @return 反馈查询到的数据总数
   * **/
  public Long countAuthorizedData(DataFile data);
}
