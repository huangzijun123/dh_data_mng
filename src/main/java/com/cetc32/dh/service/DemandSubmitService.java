package com.cetc32.dh.service;
import com.cetc32.dh.dto.DemandSubmitDTO;
import com.cetc32.dh.entity.DemandSubmit;
import com.cetc32.dh.entity.vDemand;

import java.util.List;

public interface DemandSubmitService {
    public List<DemandSubmit> findAll();
    public int deleteByPrimaryKey(Integer id);
    public int insert(DemandSubmit demandSubmit);
    public int insertSelective(DemandSubmit demandSubmit);
    public DemandSubmit selectByPrimaryKey(Integer id);
    public List<DemandSubmit> selectByLimit(Integer offset,Integer limit);
    public int countDemand();
    public int updateByPrimaryKeySelective(DemandSubmit demandSubmit);
    public int updateByPrimaryKey(DemandSubmit demandSubmit);
    public List<DemandSubmit> findByKeyWord(String keyword);

    /**
     * 查询当前登陆用户提交的导入申请
     * @param name 待查询的数据
     * @return 反馈查询到的结果
     * **/
    public List<DemandSubmit> selectMySubmit(String name);

    /**
     * 查询当前登陆用户的审批过的以及未审批的提交信息
     * @param name 待查询的数据
     * @return 反馈查询到的结果
     * **/
    public List<DemandSubmit> selectMyApprove(String name);

    /**
     * 根据当前用户统计所有任务个数
     * @param name 通常demandSubmit一次查询只包含submitor。
     * @return 反馈查询到的数据个数
     * */
    Integer countMineSubmit(String name);

    /**
     * 根据当前用户统计所有任务个数
     * @param name 通常demandSubmit一次查询只包含approver。
     * @return 反馈查询到的数据个数
     * */
    Integer countMineApprov(String name);

    /**
     * 查询当前登陆用户需要审批的提交信息
     * @param demandSubmit 待查询的数据
     * @return 反馈查询到的结果
     * **/
    public List<DemandSubmit> selectReadyApprove(DemandSubmit demandSubmit);

    /**
     * 根据当前用户统计所有任务个数
     * @param demandSubmit 通常demandSubmit一次查询只包含submitor。
     * @return 反馈查询到的数据个数
     * */
    Integer countReadyApprove(DemandSubmit demandSubmit);

    List<DemandSubmit> queryFilesByObj(vDemand vdemand);

    Long countFilesByObj(vDemand vdemand);

    List<DemandSubmitDTO> searchbystatus(DemandSubmitDTO demandSubmitDTO);

    List<String> alldemandclassify();
}
