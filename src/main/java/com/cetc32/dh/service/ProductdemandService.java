package com.cetc32.dh.service;

import com.cetc32.dh.entity.Productdemand;
import com.cetc32.dh.entity.vProduct;

import java.util.List;
import java.util.Map;

public interface ProductdemandService {

    public List<Productdemand> findAll();
    public int deleteByPrimaryKey(Integer id);
    public int insert(Productdemand productdemand);
    public int insertSelective(Productdemand productdemand);
    public Productdemand selectByPrimaryKey(Integer id);
    public List<Productdemand> selectByLimit(Integer offset, Integer limit);
    public int countProduct();
    public int updateByPrimaryKeySelective(Productdemand record);
    public int updateByPrimaryKey(Productdemand record);
    public List<Productdemand> findByKeyWord(String keyword);

    /**
     * 查询当前登陆用户提交的导入申请
     * @param name 待查询的数据
     * @return 反馈查询到的结果
     * **/
    public List<Productdemand> selectMySubmit(String name);

    /**
     * 查询当前登陆用户的审批过的以及未审批的提交信息
     * @param name 待查询的数据
     * @return 反馈查询到的结果
     * **/
    public List<Productdemand> selectMyApprove(String name);

    /**
     * 根据当前用户统计所有任务个数
     * @param name 通常productdemand一次查询只包含submitor。
     * @return 反馈查询到的数据个数
     * */
    Integer countMineSubmit(String name);

    /**
     * 根据当前用户统计所有任务个数
     * @param name 通常productdemand一次查询只包含approver。
     * @return 反馈查询到的数据个数
     * */
    Integer countMineApprov(String name);

    /**
     * 查询当前登陆用户需要审批的提交信息
     * @param productdemand 待查询的数据
     * @return 反馈查询到的结果
     * **/
    public List<Productdemand> selectReadyApprove(Productdemand productdemand);

    /**
     * 根据当前用户统计所有任务个数
     * @param productdemand 通常productdemand一次查询只包含submitor。
     * @return 反馈查询到的数据个数
     * */
    Integer countReadyApprove(Productdemand productdemand);

    List<Productdemand> queryFilesByObj(vProduct vproduct);

    Map<String,Object> PackageData(List<String> data);
}
