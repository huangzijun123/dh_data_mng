package com.cetc32.dh.service;

import com.cetc32.dh.entity.CityInfo;

import java.util.List;

public interface CityInfoService {
    public Integer insertUpdateCityInfo(CityInfo cityInfo);
    public Integer deleteCityInfo(String cityCode);
    public List<CityInfo> selectCityInfoByParam(CityInfo cityInfo);
    public Integer countCityInfoByParam(CityInfo cityInfo);
}
