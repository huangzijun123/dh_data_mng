package com.cetc32.dh.service;


import com.cetc32.dh.entity.EstimateTask;
import com.cetc32.dh.entity.vEstimate;

import java.util.List;

public interface EstimateTaskService {
    public List<EstimateTask> findAll();
    public int deleteByPrimaryKey(Integer id);
    public int insert(EstimateTask estimateTask);
    public int insertSelective(EstimateTask estimateTask);
    public EstimateTask selectByPrimaryKey(Integer id);
    public List<EstimateTask> selectByLimit(Integer offset, Integer limit);
    public int countEstimate();
    public int updateByPrimaryKeySelective(EstimateTask estimateTask);
    public int updateByPrimaryKey(EstimateTask estimateTask);
    public List<EstimateTask> findByKeyWord(String keyword);

    /**
     * 查询当前登陆用户提交的导入申请
     * @param name 待查询的数据
     * @return 反馈查询到的结果
     * **/
    public List<EstimateTask> selectMySubmit(String name);

    /**
     * 查询当前登陆用户的审批过的以及未审批的提交信息
     * @param name 待查询的数据
     * @return 反馈查询到的结果
     * **/
    public List<EstimateTask> selectMyApprove(String name);

    /**
     * 根据当前用户统计所有任务个数
     * @param name 通常estimateTask一次查询只包含submitor。
     * @return 反馈查询到的数据个数
     * */
    Integer countMineSubmit(String name);

    /**
     * 根据当前用户统计所有任务个数
     * @param name 通常estimateTask一次查询只包含approver。
     * @return 反馈查询到的数据个数
     * */
    Integer countMineApprov(String name);

    /**
     * 查询当前登陆用户需要审批的提交信息
     * @param estimateTask 待查询的数据
     * @return 反馈查询到的结果
     * **/
    public List<EstimateTask> selectReadyApprove(EstimateTask estimateTask);

    /**
     * 根据当前用户统计所有任务个数
     * @param estimateTask 通常estimateTask一次查询只包含submitor。
     * @return 反馈查询到的数据个数
     * */
    Integer countReadyApprove(EstimateTask estimateTask);

    List<EstimateTask> queryFilesByObj(vEstimate vestimate);

    List<String> allservice(String classify);

    List<String> alltaskclassify();
}
