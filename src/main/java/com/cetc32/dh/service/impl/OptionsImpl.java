/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.service.impl;

import com.cetc32.dh.entity.Options;
import com.cetc32.dh.mybatis.OptionsMapper;
import com.cetc32.dh.service.OptionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Title: OptionsServiceImpl
 * @Description:
 * @author: xiao
 * @version: 1.0
 * @date: 2020/11/21 11:19
 */
@Service
public class OptionsImpl implements OptionsService {

  @Autowired
  public OptionsMapper optionsMapper;

  /**
   * 根据数据类别查找数据
   *
   * @param category 数据类别
   * @return 返回查询结果
   */
  @Override
  public List<Options> selectByCategory(String category) {
    return optionsMapper.selectByCategory(category);
  }
}
