package com.cetc32.dh.service.impl;

import com.cetc32.dh.entity.ZipFile;
import com.cetc32.dh.mybatis.ZipFileMapper;
import com.cetc32.dh.service.ZipFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZipFileServiceImpl implements ZipFileService {
  @Autowired
  ZipFileMapper zipFileMapper;

  @Override
  public Integer insertOne(ZipFile zipFile) {
    return zipFileMapper.insertOne(zipFile);
  }

  @Override
  public List<ZipFile> queryByUserId(Integer userId) {
    return zipFileMapper.queryByUserId(userId);
  }

  @Override
  public ZipFile queryById(Integer id) {
    return zipFileMapper.queryById(id);
  }


}
