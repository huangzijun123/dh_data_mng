package com.cetc32.dh.service.impl;

import com.cetc32.dh.entity.EstimateTask;
import com.cetc32.dh.entity.vEstimate;
import com.cetc32.dh.mybatis.EstimateTaskMapper;
import com.cetc32.dh.service.EstimateTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstimateTaskServiceImpl implements EstimateTaskService {
    @Autowired
    EstimateTaskMapper estimateTaskMapper;
    public List<EstimateTask> findAll(){ return estimateTaskMapper.findAll();}

    public int deleteByPrimaryKey(Integer id){
        if(id==null)
            return -1;
        return estimateTaskMapper.deleteByPrimaryKey(id);
    }

    public int insert(EstimateTask estimateTask){
        return estimateTaskMapper.insert(estimateTask);
    }

    public int insertSelective(EstimateTask estimateTask){
        return estimateTaskMapper.insertSelective(estimateTask);
    }

    public EstimateTask selectByPrimaryKey(Integer id){
        if(id==null)
            return null;
        return estimateTaskMapper.selectByPrimaryKey(id);
    }

    public List<EstimateTask> selectByLimit(Integer offset, Integer limit){
        return estimateTaskMapper.selectByLimit(offset, limit);
    }

    public int countEstimate() { return estimateTaskMapper.countEstimate();}

    public int updateByPrimaryKeySelective(EstimateTask estimateTask){
        return estimateTaskMapper.updateByPrimaryKeySelective(estimateTask);
    }

    public int updateByPrimaryKey(EstimateTask estimateTask){
        return estimateTaskMapper.updateByPrimaryKey(estimateTask);
    }

    public List<EstimateTask> findByKeyWord(String keyword){
        keyword = "%" + keyword + "%";
        List<EstimateTask> search = estimateTaskMapper.findByKeyWord(keyword);
        return search;
    }

    /**
     * 查询当前登陆用户提交的导入申请
     *
     * @param name 待查询的数据
     * @return 反馈查询到的结果
     **/
    @Override
    public List<EstimateTask> selectMySubmit(String name) {
        return estimateTaskMapper.selectMine(name);
    }

    /**
     * 查询当前登陆用户的审批过的以及未审批的提交信息
     *
     * @param name 待查询的数据
     * @return 反馈查询到的结果
     **/
    @Override
    public List<EstimateTask> selectMyApprove(String name) {
        return estimateTaskMapper.selectMine(name);
    }


    /**
     * 根据当前用户统计所有任务个数
     *
     * @param name 通常estimateTask一次查询只包含submitor。
     * @return 反馈查询到的数据个数
     */
    public Integer countMineSubmit(String name) {
        return estimateTaskMapper.countMine(name);
    }

    /**
     * 根据当前用户统计所有任务个数
     *
     * @param name 通常estimateTask一次查询只包含approver。
     * @return 反馈查询到的数据个数
     */
    public Integer countMineApprov(String name) {
        return estimateTaskMapper.countMine(name);
    }

    /**
     * 查询当前登陆用户需要审批的提交信息
     *
     * @param estimateTask 待查询的数据
     * @return 反馈查询到的结果
     **/
    @Override
    public List<EstimateTask> selectReadyApprove(EstimateTask estimateTask) {

        return estimateTaskMapper.selectByStatusAndUser(estimateTask);
    }

    /**
     * 根据当前用户统计所有任务个数
     *
     * @param estimateTask 通常estimateTask一次查询只包含submitor。
     * @return 反馈查询到的数据个数
     */
    @Override
    public Integer countReadyApprove(EstimateTask estimateTask) {
        return estimateTaskMapper.countByStatusAndUser(estimateTask);
    }


    @Override
    public List<EstimateTask> queryFilesByObj(vEstimate vEstimate){
        return estimateTaskMapper.queryFilesByObj(vEstimate);
    }

    @Override
    public List<String> allservice(String classify){
        return estimateTaskMapper.allservice(classify);
    }

    @Override
    public List<String> alltaskclassify(){
        return estimateTaskMapper.alltaskclassify();
    }
}
