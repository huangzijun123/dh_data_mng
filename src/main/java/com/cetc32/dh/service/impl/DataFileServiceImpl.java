/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.service.impl;

import com.cetc32.dh.common.bean.CommonVariable;
import com.cetc32.dh.common.bean.UserAreaPermision;
import com.cetc32.dh.entity.DataArea;
import com.cetc32.dh.entity.DataFile;
import com.cetc32.dh.entity.DataMenu;
import com.cetc32.dh.entity.ZQArea;
import com.cetc32.dh.mybatis.*;
import com.cetc32.dh.service.CityGeomService;
import com.cetc32.dh.service.DataFileService;
import com.cetc32.dh.service.DataMenuService;
import com.cetc32.dh.utils.FileUtil;
import com.cetc32.webutil.common.bean.LoginUser;
import com.cetc32.webutil.common.util.SecurityUserUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @Title: DataFileServiceImpl
 * @Description:
 * @author: youqing
 * @version: 1.0
 * @date: 2020/11/21 11:04
 */
@Service
public class DataFileServiceImpl implements DataFileService {

  @Autowired
  public DataFileMapper dataFileMapper;

  @Autowired
  public DataMenuMapper dataMenuMapper;

  @Autowired
  public DataMenuService dataMenuService;

  @Autowired
  public DataAreaMapper dataAreaMapper;

  @Autowired
  public ZqAreaMapper zqAreaMapper;

  @Autowired
  public CityGeomService cityGeomService;

  /**
   * 统计所有的文件数据个数
   *
   * @return 返回统计结果
   */
  @Override
  public Integer count() {
    return dataFileMapper.countAll();
  }

  /**
   * 插入一个文件数据
   *
   * @param dataFile 文件数据
   * @return 返回插入结果
   */
  @Override
  public Integer insertDataFile(DataFile dataFile) {
    List<DataArea> dataAreas =new ArrayList<>();
    packageDataFile(dataFile);
    Integer id =dataFileMapper.insertOne(dataFile);
    if(dataFile.getRegionList()!=null){
      for(String s:dataFile.getRegionList()){
        dataAreas .add(new DataArea(Long.valueOf(dataFile.getId()),
                s,
                Long.valueOf(dataFile.getMenuId())));
      }
      if(dataAreas.size()>0){
        try{
          dataAreaMapper.insertArea(dataAreas);
        }catch (Exception e){
          e.printStackTrace();
        }
      }
    }
    return id;
  }
  /************************************************************************
   * 封装前端提交的DataFile实体类型，校验RegionList中区域信息是否是通用的areaCode
   * 如果是这提取其对应CityName ,如果校验RegionList中区域信息为省市，则转化为areaCode
   * @param dataFile  需要封装的datafile数据实体
   * @return 无
   * **********************************************************************/
  public  void packageDataFile(DataFile dataFile) {
    List<String> rgList =dataFile.getRegionList();
    if (rgList != null && rgList.size()>0) {
      if(Pattern.matches("^\\d{6}$",  //校验6位数字编码
              rgList.get((new Random().nextInt(rgList.size()))).trim())){ //随即选取一个进行校验
        dataFile.setRegion(cityGeomService.selectCityInCodes(rgList));
      }else if(Pattern.matches("^[\\u4e00-\\u9fa5]{0,20}$",
              rgList.get((new Random().nextInt(rgList.size()))).trim())){//校验数字编码失败，则表示可能为文字
        dataFile.setRegion(rgList.toString()
                .replace("[", "")
                .replace("]", ""));
        //根据城市名称获取城市areaCode
        dataFile.setRegionList(cityGeomService.selectCodesInArea(rgList));
      }
    }else  if(null != dataFile.getRegion()){
        String[] regions = dataFile.getRegion().split(",");
      if(Pattern.matches("^\\d{6}$",  //校验6位数字编码
              regions[(new Random().nextInt(regions.length))].trim())){ //随即选取一个进行校验
        //rgList.addAll(Arrays.asList(regions));
        dataFile.setRegionList(Arrays.asList(regions));
        //如果是区域编码这需要先转换汉字后将areacode存储到RegionList
        dataFile.setRegion(cityGeomService.selectCityInCodes(dataFile.getRegionList()));
      }else if(Pattern.matches("^[\\u4e00-\\u9fa5]{0,20}$",
              regions[(new Random().nextInt(regions.length))].trim())){//校验数字编码失败，则表示可能为文字
        //根据城市名称获取城市areaCode
        dataFile.setRegionList(cityGeomService.selectCodesInArea(Arrays.asList(regions)));
      }

    }
    dataFile.setStatus("未审批");
    dataFile.setSubmitor(SecurityUserUtil.getUser().getUsername());
  }

  /**
   * 成果数据上报
   *
   * @param dataFile
   * @return Integer
   */
  @Override
  public Integer insertGain(DataFile dataFile) {
    return dataFileMapper.insertGain(dataFile);
  }


  /**
   * 更新一个文件数据
   *
   * @param dataFile 文件数据
   * @return 返回更新结果
   */
  @Override
  public Integer updatebyId(DataFile dataFile) {
    return dataFileMapper.updateById(dataFile);
  }

  /**
   * 根据id删除一个文件数据
   *
   * @param id 文件id
   * @return 返回删除结果
   */
  @Override
  public Integer deleteById(Long id) {
    return dataFileMapper.deleteById(id);
  }

  /**
   * 根据id查询文件数据
   *
   * @param id 文件id
   * @return 返回查询结果
   */
  @Override
  public DataFile queryById(Long id) {
    return dataFileMapper.queryById(id);
  }

  /**
   * 根据时间查询文件数据
   *
   * @param time 文件时间
   * @return 返回查询结果
   */
  @Override
  public List<DataFile> queryByTime(Date time) {
    return dataFileMapper.queryByTime(time);
  }

  /**
   * 根据区域查询文件数据
   *
   * @param region 文件所属区域
   * @return 返回查询结果
   */
  @Override
  public List<DataFile> queryByRegion(String region) {
    return dataFileMapper.queryByRegion(region);
  }

  /**
   * 根据安全等级查询文件数据
   *
   * @param security 文件安全等级
   * @return 返回查询结果
   */
  @Override
  public List<DataFile> queryAllByFileSecurity(Integer security) {
    return dataFileMapper.queryAllByFileSecurity(security);
  }

  /**
   * 根据安状态和用户查询文件数据
   *
   * @param dataFile 文件数据
   * @return 返回查询结果
   */
  @Override
  public List<DataFile> selectByStatusAndUser(DataFile dataFile) {

    return dataFileMapper.selectByStatusAndUser(dataFile);
  }

  /**
   * 删除编目menuId下的所有文件数据
   *
   * @param menuId 编目id
   * @return 返回删除结果
   */
  @Override
  public Integer deleteByMenuId(Integer menuId) {
    return dataFileMapper.deleteByMenuId(menuId);
  }


  /**
   * 查询编目menuId下的所有文件数据
   *
   * @param menuId 编目id
   * @return 返回查询结果
   */
  @Override
  public List<DataFile> queryByMenuId(Integer menuId){
    return dataFileMapper.queryByMenuId(menuId);
  }

  /**
   * 根据安全状态和用户统计文件数据
   *
   * @param dataFile 文件数据
   * @return 返回统计结果
   */
  @Override
  public Integer countByStatusAndUser(DataFile dataFile) {
    return dataFileMapper.countByStatusAndUser(dataFile);
  }


  /**
   * 根据输入条件查询文件数据
   *
   * @param offset   偏移量
   * @param limit    每页显示条数
   * @param dataFile 文件数据
   * @return 返回查询结果
   */
  @Override
  public List<DataFile> queryFilesByObj(Integer offset, Integer limit, DataFile dataFile) {
    System.out.println(dataFile);
    //文件名称
    if (dataFile.getFileName() != null) {
      if (dataFile.getFileName().contains("%")) {
        return new ArrayList<>();   //处理特殊字符%
      } else if (StringUtils.isNotBlank(dataFile.getFileName())) {
        String fileName = "%" + dataFile.getFileName().trim() + "%";
        dataFile.setFileName(fileName);
      } else {
        dataFile.setFileName(null);
      }
    }

    //数据标识
    if (dataFile.getFileConfig() != null) {
      if (dataFile.getFileConfig().contains("%")) {
        return new ArrayList<>();
      } else if (StringUtils.isNotBlank(dataFile.getFileConfig())) {
        String fileConfig = "%" + dataFile.getFileConfig() + "%";
        dataFile.setFileConfig(fileConfig);
      } else {
        dataFile.setFileConfig(null);
      }
    }

    //区域
    if (dataFile.getRegion() != null) {
      if (dataFile.getRegion().contains("%")) {
        return new ArrayList<>();
      } else if (StringUtils.isNotBlank(dataFile.getRegion())) {

        String polygon = dataFile.getRegion().toUpperCase().trim();
        if(polygon.startsWith("POLYGON")){
          dataFile.setPoints(polygon);
          dataFile.setRegion(null);
        }else{
          String fileRegion = "%" + dataFile.getRegion() + "%";
          dataFile.setRegion(fileRegion);
        }
      } else {
        dataFile.setRegion(null);
      }
    }

    //审批状态
    if (StringUtils.isBlank(dataFile.getStatus()) || dataFile.getStatus().equals("全部审批状态")) {
      dataFile.setStatus(null);
    }
    //图像地理坐标系
    if (StringUtils.isBlank(dataFile.getGcs()) || dataFile.getGcs().equals("全部坐标系")) {
      dataFile.setGcs(null);
    }
    //图像比例尺
    if (StringUtils.isBlank(dataFile.getScale()) || dataFile.getScale().equals("全部比例尺")) {
      dataFile.setScale(null);
    }

    //经度
    if (dataFile.getLan() != null) {
      if (dataFile.getLan().contains("%")) {
        return new ArrayList<>();
      } else if (StringUtils.isNotBlank(dataFile.getLan())) {
        String lan = "%" + dataFile.getLan() + "%";
        dataFile.setLan(lan);
      } else {
        dataFile.setLan(null);
      }
    }

    //纬度
    if (dataFile.getLon() != null) {
      if (dataFile.getLon().contains("%")) {
        return new ArrayList<>();
      } else if (StringUtils.isNotBlank(dataFile.getLon())) {
        String lon = "%" + dataFile.getLon() + "%";
        dataFile.setLon(lon);
      } else {
        dataFile.setLon(null);
      }
    }

    return dataFileMapper.queryFilesByObj(offset, limit, dataFile);
  }

  /**
   * 根据输入条件统计文件数据量
   *
   * @param dataFile 文件数据
   * @return 返回统计结果
   */
  @Override
  public Integer countFilesByObj(DataFile dataFile) {

    System.out.println(dataFile);
    //文件名称
    if (dataFile.getFileName() != null) {
      if (dataFile.getFileName().contains("%")) {
        return 0;   //处理特殊字符%
      } else if (StringUtils.isNotBlank(dataFile.getFileName())) {
        String fileName = "%" + dataFile.getFileName().trim() + "%";
        dataFile.setFileName(fileName);
      } else {
        dataFile.setFileName(null);
      }
    }

    //数据标识
    if (dataFile.getFileConfig() != null) {
      if (dataFile.getFileConfig().contains("%")) {
        return 0;
      } else if (StringUtils.isNotBlank(dataFile.getFileConfig())) {
        String fileConfig = "%" + dataFile.getFileConfig() + "%";
        dataFile.setFileConfig(fileConfig);
      } else {
        dataFile.setFileConfig(null);
      }
    }

    //区域
    if (dataFile.getRegion() != null) {
      if (dataFile.getRegion().contains("%")) {
        return 0;
      } else if (StringUtils.isNotBlank(dataFile.getRegion())) {
        String fileRegion = "%" + dataFile.getRegion() + "%";
        dataFile.setRegion(fileRegion);
      } else {
        dataFile.setRegion(null);
      }
    }

    //审批状态
    if (StringUtils.isBlank(dataFile.getStatus()) || dataFile.getStatus().equals("全部审批状态")) {
      dataFile.setStatus(null);
    }
    //图像地理坐标系
    if (StringUtils.isBlank(dataFile.getGcs()) || dataFile.getGcs().equals("全部坐标系")) {
      dataFile.setGcs(null);
    }
    //图像比例尺
    if (StringUtils.isBlank(dataFile.getScale()) || dataFile.getScale().equals("全部比例尺")) {
      dataFile.setScale(null);
    }

    //经度
    if (dataFile.getLan() != null) {
      if (dataFile.getLan().contains("%")) {
        return 0;
      } else if (StringUtils.isNotBlank(dataFile.getLan())) {
        String lan = "%" + dataFile.getLan() + "%";
        dataFile.setLan(lan);
      } else {
        dataFile.setLan(null);
      }
    }

    //纬度
    if (dataFile.getLon() != null) {
      if (dataFile.getLon().contains("%")) {
        return 0;
      } else if (StringUtils.isNotBlank(dataFile.getLon())) {
        String lon = "%" + dataFile.getLon() + "%";
        dataFile.setLon(lon);
      } else {
        dataFile.setLon(null);
      }
    }
    return dataFileMapper.countFilesByObj(dataFile);
  }

  /**
   * 统计编目menuId下的所有文件数据的大小
   *
   * @param menuId 编目id
   * @return 返回结果
   */
  @Override
  public Long fileSizeCount(Integer menuId){
    Long size = 0L;
    DataMenu dataMenu = dataMenuService.queryById(menuId.longValue());
    List<DataMenu> dataMenuList = dataMenuService.queryByPIdSatisfyId(menuId.longValue());
    for (DataMenu dataMenuChild:dataMenuList) {
      size=size+fileSizeCount(dataMenuChild.getId().intValue());
    }
    size=size+ dataFileMapper.fileSizeCount(menuId);
    return size;
  }

  /**
   * 统计编目menuId下的所有文件个数
   *
   * @param menuId 编目id
   * @return 返回结果
   */
  @Override
  public Integer fileNumCount(Integer menuId){
    Integer num = 0;
    DataMenu dataMenu = dataMenuService.queryById(menuId.longValue());
    List<DataMenu> dataMenuList = dataMenuService.queryByPIdSatisfyId(menuId.longValue());
    for (DataMenu dataMenuChild:dataMenuList) {
      num=num+fileNumCount(dataMenuChild.getId().intValue());
    }
    num=num+ dataFileMapper.fileNumCount(menuId);
    return num;
  }

  /**
   * 根据dataFile实体获取对应DataFile
   *
   * @param limit   请求的资源数量
   * @param pageNum 页码
   * @param data    请求数据
   * @return 反馈查询到的数据
   **/
  @Override
  public List<DataFile> selectAuthorizedData(Integer limit, Integer pageNum, DataFile data) {
    if(null==limit || limit <=0)
      return  null;
    if(pageNum<=0)
      pageNum=1;
    Integer offset = limit *(pageNum -1);
    UserAreaPermision user = getUserAreaPermision();
    if (user == null) return null;
    dataFileMapper.selectFilesHasPermission(offset,limit,data,user,user.getPolygon());
    return null;
  }

  private UserAreaPermision getUserAreaPermision() {
    UserAreaPermision user = new UserAreaPermision();
    LoginUser loginUser = SecurityUserUtil.getUser();
    if(null == loginUser)
      return null;
    List<String > areaCodes = loginUser.getAreacode();
    if(areaCodes.get(0).length()> CommonVariable.SOUTH_ZQ_CODE.length()) {
      user.setAreaCode(SecurityUserUtil.getUser().getAreacode());
    } else {
      user.setPolygon(zqAreaMapper.selectAreaByZqId(loginUser.getAreacode()));
    }
    return user;
  }

  /**
   * 根据dataFile实体获取对应DataFile
   *
   * @param data 请求数据
   * @return 反馈查询到的数据总数
   **/
  @Override
  public Long countAuthorizedData(DataFile data) {
    UserAreaPermision user = getUserAreaPermision();
    if (user == null) return null;
    return dataFileMapper.countFilesHasPermission(data,user,user.getPolygon());
  }
}

