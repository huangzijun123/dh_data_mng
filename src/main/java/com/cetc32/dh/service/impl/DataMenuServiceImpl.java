/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.service.impl;

import com.cetc32.dh.dto.DataMenuDTO;
import com.cetc32.dh.entity.DataMenu;
import com.cetc32.dh.mybatis.DataFileMapper;
import com.cetc32.dh.mybatis.DataMenuMapper;
import com.cetc32.dh.service.DataFileService;
import com.cetc32.dh.service.DataMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Title: DataMenuServiceImpl
 * @Description:
 * @author: youqing
 * @version: 1.0
 * @date: 2020/11/21 11:04
 */
@Service
public class DataMenuServiceImpl implements DataMenuService {

  @Autowired
  public DataMenuMapper dataMenuMapper;

  @Autowired
  public DataFileMapper dataFileMapper;

  @Autowired
  public DataFileService dataFileService;

  /**
   * 统计所有的编目数据个数
   *
   * @return 返回统计结果
   */
  @Override
  public Integer count() {
    return dataMenuMapper.countAll();
  }

  /**
   * 插入一个编目数据
   *
   * @param dataMenu 编目数据
   * @return 返回插入结果
   */
  @Override
  public Integer insertDataMenu(DataMenu dataMenu) {
    return dataMenuMapper.insertOne(dataMenu);
  }

  /**
   * 更新编目节点
   *
   * @param dataMenu 编目数据
   * @return 更新信息
   */
  @Override
  public Integer updatebyId(DataMenu dataMenu) {
    return dataMenuMapper.updateById(dataMenu);
  }

  /**
   * 根据id查询编目数据
   *
   * @param id 编目id
   * @return 返回查询结果
   */
  @Override
  public DataMenu queryById(Long id) {
    return dataMenuMapper.queryById(id);
  }

  /**
   * 根据id删除一个编目数据
   *
   * @param id 编目id
   * @return 返回删除结果
   */
  @Override
  public Integer deleteById(Long id) {
    return dataMenuMapper.deleteById(id);
  }

  /**
   * 根据父节点pid查询编目
   *
   * @param pid 父节点
   * @return 返回查询结果
   */
  @Override
  public DataMenu queryByPid(Long pid) {
    return dataMenuMapper.queryByPid(pid);
  }

  /**
   * 查询所有以id为父节点的编目
   *
   * @param id 编目id
   * @return 返回查询结果
   */
  @Override
  public List<DataMenu> queryByPIdSatisfyId(Long id) {
    return dataMenuMapper.queryByPIdSatisfyId(id);
  }


  /**
   * 查询编目id节点下的编目树信息
   *
   * @param id 编目id
   * @return 返回查询结果
   */
  @Override
  public DataMenuDTO getMenuTree(Long id) {

    ArrayList<DataMenuDTO> sub = new ArrayList();
    DataMenu dataMenu = queryById(id);
    DataMenuDTO dataMenuDTO = new DataMenuDTO();
//        BeanUtils.copyProperties(dataMenu,dataMenuDTO);
    dataMenuDTO.setTitle(dataMenu.getMenuName());
    dataMenuDTO.setValue(id.toString());
    dataMenuDTO.setKey(dataMenu.getKey());
    dataMenuDTO.setHtmlUrl(dataMenu.getHtmlUrl());
    dataMenuDTO.setIcon(dataMenu.getIcon());
    dataMenuDTO.setDisabled(dataMenu.getDisabled());
    dataMenuDTO.setAddkids(dataMenu.getAddkids());
    dataMenuDTO.setDiscription(dataMenu.getDiscription());
    List<DataMenu> childList = queryByPIdSatisfyId(id);
    if (childList.size() == 0) {
//            dataMenuDTO.setChildren(null);
      return dataMenuDTO;
    }
    dataMenuDTO.setChildren(sub);

    for (int i = 0; i < childList.size(); i++) {
      Long myId = childList.get(i).getId();
      dataMenuDTO.getChildren().add(getMenuTree(myId));
    }
    return dataMenuDTO;
  }

  /**
   * 删除编目id下的树
   *
   * @param id 编目id
   * @return 返回删除结果
   */
  @Override
  public Integer deleteMenuTree(Long id) {
    Integer count = 0;
    List<DataMenu> childList = queryByPIdSatisfyId(id);
    for (int i = 0; i < childList.size(); i++) {
      count += deleteMenuTree(childList.get(i).getId());
    }
    dataFileService.deleteByMenuId(id.intValue());
    count += deleteById(id);
    return count;
  }

  /**
   * 统计编目id下子树中所有节点的数目
   *
   * @param id 编目id
   * @return 返回统计结果
   */
  @Override
  public Integer countMenuChild(Long id) {
    Integer count = 0;
    List<DataMenu> childList = queryByPIdSatisfyId(id);
    for (int i = 0; i < childList.size(); i++) {
      count += countMenuChild(childList.get(i).getId());
    }
    count += childList.size();
    return count;
  }

  /**
   * 新增一个编目数据
   *
   * @param dataMenu 编目数据
   * @return 返回新增结果
   */
  @Override
  public String addDataMenu(DataMenu dataMenu) {
    String message = "添加成功";
    Integer count = insertDataMenu(dataMenu);
    if (count != 1) {
      message = "添加失败";
    }
    return message;
  }

  /**
   * 查询所有的编目数据
   *
   * @return 返回查询结果
   */
  public List<DataMenu> selectAll() {
    return dataMenuMapper.selectAll();
  }

  /**
   * 查询编目键为key编目数据
   *
   * @return 返回查询结果
   */
  public DataMenu queryByKey(String key) {
    return dataMenuMapper.queryByKey(key);
  }


}
