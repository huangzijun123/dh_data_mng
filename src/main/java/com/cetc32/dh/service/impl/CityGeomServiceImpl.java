package com.cetc32.dh.service.impl;

import com.cetc32.dh.common.bean.TreeData;
import com.cetc32.dh.entity.CityGeom;
import com.cetc32.dh.entity.DataFile;
import com.cetc32.dh.entity.DataPlp;
import com.cetc32.dh.entity.ProvinceGeom;
import com.cetc32.dh.mybatis.CityGeomMapper;
import com.cetc32.dh.service.CityGeomService;
import com.cetc32.dh.service.DataFileService;
import com.cetc32.dh.service.DataPlpService;
import com.cetc32.dh.service.ProvinceGeomService;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

@Service
public class CityGeomServiceImpl implements CityGeomService {
  @Autowired
  public CityGeomMapper cityGeomMapper;
  @Autowired
  public ProvinceGeomService provinceGeomService;
  @Autowired
  public DataFileService dataFileService;
  @Autowired
  public DataPlpService dataPlpService;


  @Override
  public List<TreeData> selectTreeData(String proCode) {
    List<CityGeom> cityGeoms  = cityGeomMapper.selectCityTree(proCode);
    List<TreeData> data = new ArrayList<>();
    for(CityGeom city : cityGeoms){
      //TreeData root =new TreeData(city);
      //System.out.println(data);
      if(data.size()>0){
        TreeData parent = null;
        for(TreeData leaf:data){
          parent =leaf.addChildren(city);
        }
        if(null == parent){
          data.add(new TreeData(city));
        }
      }else {
        data.add(new TreeData(city));
      }
    }
    return data;
  }

  @Override
  public int updateByName(CityGeom cityGeom){
    return cityGeomMapper.updateByName(cityGeom);
  }

  @Override
  public CityGeom queryByName(String name){
    return cityGeomMapper.queryByName(name);
  }

  @Override
  public  String readFileContent(String fileName) {
    File file = new File(fileName);
    BufferedReader reader = null;
    StringBuffer sbf = new StringBuffer();
    try {
      reader = new BufferedReader(new FileReader(file));
      String tempStr;
      while ((tempStr = reader.readLine()) != null) {
        sbf.append(tempStr);
      }
      reader.close();
      return sbf.toString();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e1) {
          e1.printStackTrace();
        }
      }
    }
    return sbf.toString();
  }


  @Override
  public Map<String,String> areaToPolygon(List<String> areaNames){
    Map<String, String> areaPlg = new HashMap<>();
    //ProvinceGeom provinceGeom;
    CityGeom cityGeom;
    for (int i = 0; i < areaNames.size(); i++) {
      /*provinceGeom = cityGeom.queryByName(areaNames.get(i));
      if (provinceGeom != null) {
        areaPlg.put(areaNames.get(i), provinceGeom.getPolygonText());
      } else {*/
        cityGeom = cityGeomMapper.queryByName(areaNames.get(i));
        if (cityGeom != null) {
          areaPlg.put(areaNames.get(i), cityGeom.getPolygonText());
        }else{
          areaPlg.put(null, areaNames.get(i));
        }
      //}
    }
    return areaPlg;
  }

  @Override
  public Set<DataFile> menuAndPgonCom(Integer menuId, List<String> polygons){
    Boolean jdg_polygon = true;
    ProvinceGeom provinceGeom;
    CityGeom cityGeom;
    Set<DataFile> resultFileList = new HashSet<>();
    List<String> regionList = new ArrayList<>();
    List<DataFile> dataFileList = new ArrayList<>();
    dataFileList = dataFileService.queryByMenuId(menuId);
    for (int i = 0; i < dataFileList.size(); i++) {
      for (int num =0;num< polygons.size();num++){
        jdg_polygon = true;
        if (dataFileList.get(i).getRegion() != null && !dataFileList.get(i).getRegion().trim().isEmpty()) {
          regionList = Arrays.asList(dataFileList.get(i).getRegion().split(","));
          for (int j = 0; j < regionList.size(); j++) {
//          System.out.println(regionList.get(j));
            provinceGeom = provinceGeomService.queryByName(StringUtils.trim(regionList.get(j)));
            if (provinceGeom != null && provinceGeom.getPolygonText() != null && polygons.get(num).contains("POLYGON")) {
              jdg_polygon = provinceGeomService.judgePolygonDisjoint(provinceGeom.getPolygonText(), polygons.get(num));
              if (!jdg_polygon) {
                resultFileList.add(dataFileList.get(i));
                break;
              }
            }
            cityGeom = cityGeomMapper.queryByName(StringUtils.trim(regionList.get(j)));
            if (cityGeom != null && cityGeom.getPolygonText() != null && polygons.get(num).contains("POLYGON")) {
              jdg_polygon = provinceGeomService.judgePolygonDisjoint(cityGeom.getPolygonText(), polygons.get(num));
              if (!jdg_polygon) {
                resultFileList.add(dataFileList.get(i));
                break;
              }
            }
          }
        }
      }
    }
    return resultFileList;
  }

  @Override
  public List<CityGeom> selectCityInPolygon(String polygon) {
    return cityGeomMapper.selectCityInPolygon(polygon);
  }

  @Override
  public Set<DataPlp> menuAndPgonPlm(List<String> polygons) {
    Boolean jdg_polygon = true;
    String area;
    Set<DataPlp> resultFileList = new HashSet<>();
    List<DataPlp> dataPlpList = new ArrayList<>();
    DataPlp dataplp = new DataPlp();
    dataplp.setEventType("POLYGON");
    dataPlpList = dataPlpService.queryFilesByObj(0,Integer.MAX_VALUE,dataplp);
    for (int i = 0; i < dataPlpList.size(); i++) {
      area = dataPlpList.get(i).getPoints();
      if (area!=null&&area.toLowerCase().contains("polygon")) {
        for (int num = 0; num < polygons.size(); num++) {
          jdg_polygon = true;
          jdg_polygon = provinceGeomService.judgePolygonDisjoint(area, polygons.get(num));
          if (!jdg_polygon) {
            resultFileList.add(dataPlpList.get(i));
            break;
          }
        }
      }

    }
    return resultFileList;
  }

  @Override
  public List<CityGeom> areaFindPolygon(List<String> ares) {

    ArrayList<CityGeom> cityGeoms =new ArrayList<>();
    for(String s:ares){
      CityGeom cityGeom =cityGeomMapper.queryByName(s);
      if(null != cityGeom){
        cityGeoms.add(cityGeom);
      }
    }
    return cityGeoms;
  }

  @Override
  public String selectCityInCodes(List<String> areaCodes) {
    if(areaCodes ==null || areaCodes.size() ==0)
      return null;
    else return cityGeomMapper.selectCityInCodes(areaCodes);
  }

  @Override
  public List<String> selectCodesInArea(List<String> cities) {
    if(cities ==null || cities.size() ==0)
      return null;
    else return cityGeomMapper.selectCodesInArea(cities);
  }


  @Override
  public Boolean judgePolygonContain(String polygon1, String polygon2) {
    // pointss ="POLYGON((98.31768 46.16992,127.59814 45.80590,108.78794 34.13706,93.2099 35.04692,98.31768 46.16992,98.31768 46.16992))";
    return cityGeomMapper.judgePolygonContain(polygon1, polygon2);
  }

  /**
   * 判断某个点是否在一个面中
   *
   * @return 返回判断结果
   * 备注:无
   */
  @Override
  public Boolean judgePointContain(String point, String polygon) {
    // pointss ="POLYGON((98.31768 46.16992,127.59814 45.80590,108.78794 34.13706,93.2099 35.04692,98.31768 46.16992,98.31768 46.16992))";
    return cityGeomMapper.judgePointContain(point, polygon);
  }

  @Override
  public List<String> queryAreaCodeByCity(String areaName){
    return cityGeomMapper.queryAreaCodeByCity(areaName);
  }

  @Override
  public List<String> queryAreaCodeByProvince(String areaName){
    return cityGeomMapper.queryAreaCodeByProvince(areaName);
  }

  public List<String> queryCityNameByProvince(String areaName){
    return cityGeomMapper.queryCityNameByProvince(areaName);
  }

}
