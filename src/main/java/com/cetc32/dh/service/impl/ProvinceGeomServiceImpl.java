package com.cetc32.dh.service.impl;

import com.cetc32.dh.entity.ProvinceGeom;
import com.cetc32.dh.mybatis.ProvinceGeomMapper;
import com.cetc32.dh.service.ProvinceGeomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProvinceGeomServiceImpl implements ProvinceGeomService{

  @Autowired
  ProvinceGeomMapper provinceGeomMapper;

  public int insertOne(ProvinceGeom provinceGeom){
    return provinceGeomMapper.insertOne(provinceGeom);
  }

  public int updateByName(ProvinceGeom provinceGeom){
    return provinceGeomMapper.updateByName(provinceGeom);
  }

  public List<ProvinceGeom> selectAll(){return provinceGeomMapper.selectAll();}

  public ProvinceGeom queryByName(String name){return provinceGeomMapper.queryByName(name);}

  public Boolean judgePolygonDisjoint(String polygon1,String polygon2){
    return provinceGeomMapper.judgePolygonDisjoint(polygon1,polygon2);
  }





}
