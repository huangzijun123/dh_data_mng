package com.cetc32.dh.service.impl;

import com.cetc32.dh.dto.LineBoth;
import com.cetc32.dh.dto.TwoNode;
import com.cetc32.dh.entity.Productdemand;
import com.cetc32.dh.entity.vProduct;
import com.cetc32.dh.mybatis.ProductdemandMapper;
import com.cetc32.dh.service.ProductdemandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductdemandServiceImpl implements ProductdemandService {
    @Autowired
    ProductdemandMapper productdemandMapper;


    public List<Productdemand> findAll() { return productdemandMapper.findAll();}

    public int deleteByPrimaryKey(Integer id){
        if(id==null)
            return -1;
        return productdemandMapper.deleteByPrimaryKey(id);
    }

    public int insert(Productdemand productdemand){
        return productdemandMapper.insert(productdemand);
    }

    public int insertSelective(Productdemand record){
        return productdemandMapper.insertSelective(record);
    }

    public Productdemand selectByPrimaryKey(Integer id){
        if(id==null)
            return null;
        return productdemandMapper.selectByPrimaryKey(id);
    }

    public List<Productdemand> selectByLimit(Integer offset, Integer limit){
        return productdemandMapper.selectByLimit(offset, limit);
    }

    public int countProduct() { return productdemandMapper.countProduct();}

    public int updateByPrimaryKeySelective(Productdemand productdemand){
        return productdemandMapper.updateByPrimaryKeySelective(productdemand);
    }

    public int updateByPrimaryKey(Productdemand productdemand){
        return productdemandMapper.updateByPrimaryKey(productdemand);
    }

    public List<Productdemand> findByKeyWord(String keyword){
        keyword ="%"+keyword+"%";
        List<Productdemand> search_by_keyword = productdemandMapper.findByKeyWord(keyword);
        return  search_by_keyword;
    }

    /**
     * 查询当前登陆用户提交的导入申请
     *
     * @param name 待查询的数据
     * @return 反馈查询到的结果
     **/
    @Override
    public List<Productdemand> selectMySubmit(String name) {
        return productdemandMapper.selectMine(name);
    }

    /**
     * 查询当前登陆用户的审批过的以及未审批的提交信息
     *
     * @param name 待查询的数据
     * @return 反馈查询到的结果
     **/
    @Override
    public List<Productdemand> selectMyApprove(String name) {
        return productdemandMapper.selectMine(name);
    }


    /**
     * 根据当前用户统计所有任务个数
     *
     * @param name 通常productdemand一次查询只包含submitor。
     * @return 反馈查询到的数据个数
     */
    public Integer countMineSubmit(String name) {
        return productdemandMapper.countMine(name);
    }

    /**
     * 根据当前用户统计所有任务个数
     *
     * @param name 通常productdemand一次查询只包含approver。
     * @return 反馈查询到的数据个数
     */
    public Integer countMineApprov(String name) {
        return productdemandMapper.countMine(name);
    }

    /**
     * 查询当前登陆用户需要审批的提交信息
     *
     * @param productdemand 待查询的数据
     * @return 反馈查询到的结果
     **/
    @Override
    public List<Productdemand> selectReadyApprove(Productdemand productdemand) {

        return productdemandMapper.selectByStatusAndUser(productdemand);
    }

    /**
     * 根据当前用户统计所有任务个数
     *
     * @param productdemand 通常productdemand一次查询只包含submitor。
     * @return 反馈查询到的数据个数
     */
    @Override
    public Integer countReadyApprove(Productdemand productdemand) {
        return productdemandMapper.countByStatusAndUser(productdemand);
    }

    @Override
    public List<Productdemand> queryFilesByObj(vProduct vProduct){
        return productdemandMapper.queryFilesByObj(vProduct);
    }

//    @Override
//    public Map<String,Object> PackageData(List<String> data){
//        List<TwoNode> twoNodes = new ArrayList<>();
//        List<LineBoth> lineBoths = new ArrayList<>();
//        List<AreaCommon> areaCommonList = areaCommonService.getAreasByIdList(data);
//        for(int i=0;i<areaCommonList.size();i++){
//            String node = data.get(i);
//            //find name
//
//            String name =areaCommonList.get(i).getName();
//            TwoNode twoNode = new TwoNode();
//            twoNode.setNodeId(node);
//            twoNode.setName(name);
//            twoNodes.add(twoNode);
//            if(i+1<data.size()){
//                LineBoth lineBoth = new LineBoth();
//                lineBoth.setSourceNodeId(data.get(i));
//                lineBoth.setTargetNodeId(data.get(i+1));
//                lineBoths.add(lineBoth);
//            }
//        }
//        Map<String,Object> map = new HashMap<>();
//        map.put("list1",twoNodes);
//        map.put("connlist",lineBoths);
//        return map;
//    }
    @Override
    public Map<String,Object> PackageData(List<String> data){
        List<TwoNode> twoNodes = new ArrayList<>();
        List<LineBoth> lineBoths = new ArrayList<>();
        for(int i = 0 ;i<data.size();i++){
            TwoNode twoNode = new TwoNode();
            twoNode.setNodeId(data.get(i));
            twoNode.setName(data.get(i));
            twoNodes.add(twoNode);

            LineBoth lineBothstart = new LineBoth();
            lineBothstart.setSourceNodeId("开始");
            lineBothstart.setTargetNodeId(data.get(i));
            lineBoths.add(lineBothstart);

            LineBoth lineBothend = new LineBoth();
            lineBothend.setSourceNodeId(data.get(i));
            lineBothend.setTargetNodeId("结束");
            lineBoths.add(lineBothend);

        }
        Map<String,Object> map = new HashMap<>();
        map.put("list1",twoNodes);
        map.put("connlist",lineBoths);
        return map;
    }

}
