/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.service.impl;

import com.cetc32.dh.beans.DataCollected;
import com.cetc32.dh.entity.DataPlp;
import com.cetc32.dh.mybatis.DataPlpMapper;
import com.cetc32.dh.service.DataPlpService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Title: DataPlpServiceImpl
 * @Description:
 * @author: youqing
 * @version: 1.0
 * @date: 2020/11/21 11:19
 */
@Service
public class DataPlpServiceImpl implements DataPlpService {

  @Autowired
  DataPlpMapper dataPlpMapper;

  @Override
  public Integer insertCollected(DataCollected data) {
    return dataPlpMapper.insertCollected(data);
  }

  @Override
  public List<DataCollected> selectPloygon(String status,Date startTime, Date endTime, String polygon) {
    return dataPlpMapper.selectPloygon(status,startTime, endTime, polygon);
  }

  /**
   * 统计所有的点线面数据个数
   *
   * @return 返回统计结果
   */
  @Override
  public Integer countAll() {
    return dataPlpMapper.countAll();
  }

  /**
   * 查询所有的点线面数据个数
   *
   * @return 返回统计结果
   */
  @Override
  public List<DataPlp> selectAll(){
    return  dataPlpMapper.selectAll();
  }


  /**
   * 插入一个点线面数据
   *
   * @param dataPlp 插入的点线面数据
   * @return 返回是否插入成功
   */
  @Override
  public Integer insertOne(DataPlp dataPlp) {
    return dataPlpMapper.insertOne(dataPlp);
  }

  /**
   * 根据id更新点线面数据记录
   *
   * @param dataPlp 点线面数据
   * @return 返回是否更新成功
   */
  @Override
  public Integer updateById(DataPlp dataPlp) {
    return dataPlpMapper.updateById(dataPlp);
  }


  /**
   * 根据id查询点线面数据记录
   *
   * @param id 点线面数据的id
   * @return 返回查询结果
   */
  @Override
  public Integer deleteById(Integer id) {
    return dataPlpMapper.deleteById(id);
  }

  /**
   * 根据文件id点线面数据
   *
   * @param id
   * @return DataPlp
   */
  @Override
  public DataPlp queryById(Integer id) {
    return dataPlpMapper.queryById(id);
  }

  /**
   * 根据状态和审批用户查询点线面数据
   *
   * @param dataPlp 点线面数据
   * @return 返回查询结果
   */
  @Override
  public List<DataPlp> selectByStatusAndUser(DataPlp dataPlp) {
    return dataPlpMapper.selectByStatusAndUser(dataPlp);
  }

  /**
   * 统计满足状态和审批用户条件的点线面数据
   *
   * @param dataPlp 点线面数据
   * @return 返回查询结果
   */
  @Override
  public Integer countByStatusAndUser(DataPlp dataPlp) {
    return null;
  }

  /**
   * 根据输入的条件动态查询点线面数据
   *
   * @param offset 偏移量
   * @param limit  每页显示的条数
   * @return 返回查询结果
   */
  @Override
  public List<DataPlp> queryFilesByObj(Integer offset, Integer limit, DataPlp dataPlp) {
    System.out.println(dataPlp);


    //区域
    if (dataPlp.getRegion() != null) {
      if (dataPlp.getRegion().contains("%")) {
        return new ArrayList<>();
      } else if(StringUtils.isNotBlank(dataPlp.getRegion())) {
        String fileRegion = "%" + dataPlp.getRegion() + "%";
        dataPlp.setRegion(fileRegion);
      }else{
        dataPlp.setRegion(null);
      }
    }
    if (StringUtils.isBlank(dataPlp.getStatus()) || dataPlp.getStatus().equals("全部审批状态")) {
      dataPlp.setStatus(null);
    }
    if (dataPlp.getEventType() != null) {
      if (StringUtils.isBlank(dataPlp.getEventType()) || dataPlp.getEventType().equals("全部数据类型")) {
        dataPlp.setEventType(null);
      }
    }

    return dataPlpMapper.queryFilesByObj(offset, limit, dataPlp);
  }

  /**
   * 根据输入的条件动态统计查询的点线面数据
   *
   * @param dataPlp 点线面数据
   * @return 返回查询结果
   */
  @Override
  public Integer countFilesByObj(DataPlp dataPlp) {

    System.out.println(dataPlp);
    //区域
    if (dataPlp.getRegion() != null) {
      if (dataPlp.getRegion().contains("%")) {
        return 0;
      } else if(StringUtils.isNotBlank(dataPlp.getRegion())) {
        String fileRegion = "%" + dataPlp.getRegion() + "%";
        dataPlp.setRegion(fileRegion);
      }else{
        dataPlp.setRegion(null);
      }
    }
    if (StringUtils.isBlank(dataPlp.getStatus()) || dataPlp.getStatus().equals("全部审批状态")) {
      dataPlp.setStatus(null);
    }
    if (dataPlp.getEventType() != null) {
      if (StringUtils.isBlank(dataPlp.getEventType()) || dataPlp.getEventType().equals("全部数据类型")) {
        dataPlp.setEventType(null);
      }
    }
    return dataPlpMapper.countFilesByObj(dataPlp);
  }
}
