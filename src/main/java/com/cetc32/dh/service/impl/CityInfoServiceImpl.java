package com.cetc32.dh.service.impl;

import com.cetc32.dh.entity.CityInfo;
import com.cetc32.dh.mybatis.AreaCommonMapper;
import com.cetc32.dh.mybatis.CityGeomMapper;
import com.cetc32.dh.service.CityInfoService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityInfoServiceImpl implements CityInfoService {
    @Autowired
    CityGeomMapper cityGeomMapper;
    @Autowired
    AreaCommonMapper areaCommonMapper;
    @Override
    public Integer insertUpdateCityInfo(CityInfo cityInfo) {
        if(cityInfo ==null )
            return -1;
        if(cityInfo.getCityCode()== null || cityInfo.getProvinceCode() ==null)
            return -1;
        try{
            cityGeomMapper.insertOrUpdateCity(cityInfo);
            areaCommonMapper.insertOrUpdateCity(cityInfo);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Integer deleteCityInfo(String cityCode) {
        if(StringUtils.isNotBlank(cityCode)){
            try{
                cityGeomMapper.deleteByCityCode(cityCode);
                areaCommonMapper.deleteById(cityCode);
            }catch (Exception e){

            }
        }
        return -1;
    }

    @Override
    public List<CityInfo> selectCityInfoByParam(CityInfo cityInfo) {
        if(cityInfo !=null){
            if(null != cityInfo.getRemarks()){
                cityInfo.setRemarks("%"+cityInfo.getRemarks()+"%");
            }
        }
        return areaCommonMapper.selectCityByQuery(cityInfo);
    }

    @Override
    public Integer countCityInfoByParam(CityInfo cityInfo) {
        if(cityInfo !=null){
            if(null != cityInfo.getRemarks()){
                cityInfo.setRemarks("%"+cityInfo.getRemarks()+"%");
            }
        }
        return areaCommonMapper.countCityByQuery(cityInfo);
    }
}
