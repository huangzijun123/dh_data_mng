/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/9/17 下午4:56
 ******************************************************************************/
package com.cetc32.dh.service.impl;

import com.cetc32.dh.entity.DataArea;
import com.cetc32.dh.mybatis.DataAreaMapper;
import com.cetc32.dh.service.DataAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class DataAreaServiceImpl implements DataAreaService {
    @Autowired
    DataAreaMapper dataAreaMapper;

    @Override
    public Integer insertArea(List<DataArea> areaList) {
        if(areaList==null || areaList.size()==0)
            return 0;
        return dataAreaMapper.insertArea(areaList);
    }

    @Override
    public Integer deleteAreaByDataId(Long dataId) {
        return dataAreaMapper.deleteAreaByDataId(dataId);
    }

    @Override
    public Integer updateArea(Long dataId, List<DataArea> list) {
        dataAreaMapper.deleteAreaByDataId(dataId);
        if(null == list || list.size() == 0)
            return 0;
        return dataAreaMapper.insertArea(list);
    }
    @Override
    public List<HashMap<Integer, Integer>> countByAreaCodes(List<String> areaCode){
        return dataAreaMapper.countByAreaCodes(areaCode);
    }
}
