package com.cetc32.dh.service.impl;

import com.cetc32.dh.dto.DemandSubmitDTO;
import com.cetc32.dh.entity.DemandSubmit;
import com.cetc32.dh.entity.vDemand;
import com.cetc32.dh.mybatis.DemandSubmitMapper;
import com.cetc32.dh.service.DemandSubmitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DemandSubmitServiceImppl implements DemandSubmitService {
    @Autowired
    DemandSubmitMapper demandSubmitMapper;

    public List<DemandSubmit> findAll(){
        return demandSubmitMapper.findAll();
    }

    public int deleteByPrimaryKey(Integer id){
        if(id==null)
            return -1;
        return demandSubmitMapper.deleteByPrimaryKey(id);
    }

    public int insert(DemandSubmit demandSubmit){
        return demandSubmitMapper.insert(demandSubmit);
    }

    public int insertSelective(DemandSubmit demandSubmit){
        return demandSubmitMapper.insertSelective(demandSubmit);
    }

    public DemandSubmit selectByPrimaryKey(Integer id){
        if(id==null)
            return null;
        return demandSubmitMapper.selectByPrimaryKey(id);
    }

    public List<DemandSubmit> selectByLimit(Integer offset, Integer limit){
        return demandSubmitMapper.selectByLimit(offset,limit);
    }

    public int countDemand(){
        return demandSubmitMapper.countDemand();
    }

    public int updateByPrimaryKeySelective(DemandSubmit demandSubmit){
        return demandSubmitMapper.updateByPrimaryKeySelective(demandSubmit);
    }

    public int updateByPrimaryKey(DemandSubmit demandSubmit){
        return demandSubmitMapper.updateByPrimaryKey(demandSubmit);
    }

    public List<DemandSubmit> findByKeyWord(String keyword){
        keyword = "%" + keyword + "%";
        List<DemandSubmit> search = demandSubmitMapper.findByKeyWord(keyword);
        return search;
    }

    /**
     * 查询当前登陆用户提交的导入申请
     *
     * @param name 待查询的数据
     * @return 反馈查询到的结果
     **/
    @Override
    public List<DemandSubmit> selectMySubmit(String name) {
        return demandSubmitMapper.selectMine(name);
    }

    /**
     * 查询当前登陆用户的审批过的以及未审批的提交信息
     *
     * @param name 待查询的数据
     * @return 反馈查询到的结果
     **/
    @Override
    public List<DemandSubmit> selectMyApprove(String name) {
        return demandSubmitMapper.selectMine(name);
    }


    /**
     * 根据当前用户统计所有任务个数
     *
     * @param name 通常demandSubmit一次查询只包含submitor。
     * @return 反馈查询到的数据个数
     */
    public Integer countMineSubmit(String name) {
        return demandSubmitMapper.countMine(name);
    }

    /**
     * 根据当前用户统计所有任务个数
     *
     * @param name 通常demandSubmit一次查询只包含approver。
     * @return 反馈查询到的数据个数
     */
    public Integer countMineApprov(String name) {
        return demandSubmitMapper.countMine(name);
    }

    /**
     * 查询当前登陆用户需要审批的提交信息
     *
     * @param demandSubmit 待查询的数据
     * @return 反馈查询到的结果
     **/
    @Override
    public List<DemandSubmit> selectReadyApprove(DemandSubmit demandSubmit) {

        return demandSubmitMapper.selectByStatusAndUser(demandSubmit);
    }

    /**
     * 根据当前用户统计所有任务个数
     *
     * @param demandSubmit 通常demandSubmit一次查询只包含submitor。
     * @return 反馈查询到的数据个数
     */
    @Override
    public Integer countReadyApprove(DemandSubmit demandSubmit) {
        return demandSubmitMapper.countByStatusAndUser(demandSubmit);
    }

    @Override
    public List<DemandSubmit> queryFilesByObj(vDemand vDemand){
        return demandSubmitMapper.queryFilesByObj(vDemand);
    }

    @Override
    public Long countFilesByObj(vDemand vDemand){
        return demandSubmitMapper.countFilesByObj(vDemand);
    }

    @Override
    public  List<DemandSubmitDTO> searchbystatus(DemandSubmitDTO demandSubmitDTO){
        return demandSubmitMapper.searchbystatus(demandSubmitDTO);
    }

    @Override
    public List<String> alldemandclassify(){
        return demandSubmitMapper.alldemandclassify();
    }
}
