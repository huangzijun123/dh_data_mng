/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.service.impl;

import com.cetc32.dh.entity.DataTrace;
import com.cetc32.dh.mybatis.DataTraceMapper;
import com.cetc32.dh.service.DataTraceService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Title: DataTraceServiceImpl
 * @Description:
 * @author: youqing
 * @version: 1.0
 * @date: 2020/11/21 11:19
 */
@Service
public class DataTraceServiceImpl implements DataTraceService {

  @Autowired
  DataTraceMapper dataTraceMapper;

  /**
   * 统计所有的文件数据个数
   *
   * @return 返回统计结果
   */
  @Override
  public Integer countAll() {
    return dataTraceMapper.countAll();
  }

  /**
   * 插入一个轨迹数据
   *
   * @param dataTrace 轨迹数据
   * @return 返回插入结果
   */
  @Override
  public Integer insertOne(DataTrace dataTrace) {
    return dataTraceMapper.insertOne(dataTrace);
  }

  /**
   * 更新一个轨迹数据
   *
   * @param dataTrace 轨迹数据
   * @return 返回更新结果
   */
  @Override
  public Integer updateById(DataTrace dataTrace) {
    return dataTraceMapper.updateById(dataTrace);
  }


  /**
   * 删除一个轨迹数据
   *
   * @param id 轨迹数据id
   * @return 返回删除结果
   */
  @Override
  public Integer deleteById(Integer id) {
    return dataTraceMapper.deleteById(id);
  }

  /**
   * 根据id查询轨迹数据
   *
   * @param id 轨迹id
   * @return 返回查询结果
   */
  @Override
  public DataTrace queryById(Integer id) {
    return dataTraceMapper.queryById(id);
  }

  /**
   * 根据安状态和用户查询轨迹数据
   *
   * @param dataTrace 轨迹数据
   * @return 返回查询结果
   */
  @Override
  public List<DataTrace> selectByStatusAndUser(DataTrace dataTrace) {
    return dataTraceMapper.selectByStatusAndUser(dataTrace);
  }

  /**
   * 根据安全状态和用户统计轨迹数据
   *
   * @param dataTrace 轨迹数据
   * @return 返回统计结果
   */
  @Override
  public Integer countByStatusAndUser(DataTrace dataTrace) {
    return dataTraceMapper.countByStatusAndUser(dataTrace);
  }


  /**
   * 根据输入条件查询文件数据
   *
   * @param offset    偏移量
   * @param limit     每页显示条数
   * @param dataTrace 轨迹数据
   * @return 返回查询结果
   */
  @Override
  public List<DataTrace> queryFilesByObj(Integer offset, Integer limit, DataTrace dataTrace) {

    if (StringUtils.isBlank(dataTrace.getStatus()) || dataTrace.getStatus().equals("全部审批状态")) {
      dataTrace.setStatus(null);
    }
    //数据标识
    if (dataTrace.getFileConfig() != null) {
      if (dataTrace.getFileConfig().contains("%")) {
        return new ArrayList<>();
      } else if(StringUtils.isNotBlank(dataTrace.getFileConfig())) {
        String fileConfig = "%"+dataTrace.getFileConfig().trim()+"%";
        dataTrace.setFileConfig(fileConfig);
      }else{
        dataTrace.setFileConfig(null);
      }
    }
    return dataTraceMapper.queryFilesByObj(offset, limit, dataTrace);
  }

  /**
   * 根据输入条件统计轨迹数据量
   *
   * @param dataTrace 轨迹数据
   * @return 返回统计结果
   */
  @Override
  public Integer countFilesByObj(DataTrace dataTrace) {

    if (StringUtils.isBlank(dataTrace.getStatus()) || dataTrace.getStatus().equals("全部审批状态")) {
      dataTrace.setStatus(null);
    }
    //数据标识
    if (dataTrace.getFileConfig() != null) {
      if (dataTrace.getFileConfig().contains("%")) {
        return 0;
      } else if(StringUtils.isNotBlank(dataTrace.getFileConfig())) {
        String fileConfig = "%"+dataTrace.getFileConfig().trim()+"%";
        dataTrace.setFileConfig(fileConfig);
      }else{
        dataTrace.setFileConfig(null);
      }
    }

    return dataTraceMapper.countFilesByObj(dataTrace);
  }


}
