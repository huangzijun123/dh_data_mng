/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.service;

import com.cetc32.dh.beans.DataCollected;
import com.cetc32.dh.entity.DataPlp;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @Title: DataPlpService
 * @Description:
 * @author: youqing
 * @version: 1.0
 * @date: 2020/11/21 11:19
 */
public interface DataPlpService {


  public Integer insertCollected(DataCollected data);


  public List<DataCollected> selectPloygon(String status,Date startTime, Date endTime, String polygon);

  /**
   * 统计所有的点线面数据个数
   *
   * @return 返回统计结果
   */
  public Integer countAll();


  /**
   * 查询所有的点线面数据个数
   *
   * @return 返回统计结果
   */
  public List<DataPlp> selectAll();

  /**
   * 插入一个点线面数据
   *
   * @param dataPlp 插入的点线面数据
   * @return 返回是否插入成功
   */
  public Integer insertOne(DataPlp dataPlp);

  /**
   * 根据id更新点线面数据记录
   *
   * @param dataPlp 点线面数据
   * @return 返回是否更新成功
   */
  public Integer updateById(DataPlp dataPlp);

  /**
   * 根据id删除一个点线面数据记录
   *
   * @param id 点线面数据的id
   * @return 返回是否删除成功
   */
  public Integer deleteById(Integer id);

  /**
   * 根据id查询点线面数据记录
   *
   * @param id 点线面数据的id
   * @return 返回查询结果
   */
  public DataPlp queryById(Integer id);

  /**
   * 根据状态和审批用户查询点线面数据
   *
   * @param dataPlp 点线面数据
   * @return 返回查询结果
   */
  public List<DataPlp> selectByStatusAndUser(DataPlp dataPlp);

  /**
   * 统计满足状态和审批用户条件的点线面数据
   *
   * @param dataPlp 点线面数据
   * @return 返回查询结果
   */
  public Integer countByStatusAndUser(DataPlp dataPlp);

  /**
   * 根据输入的条件动态查询点线面数据
   *
   * @param offset 偏移量
   * @param limit  每页显示的条数
   * @return 返回查询结果
   */
  public List<DataPlp> queryFilesByObj(@Param("offset") Integer offset, @Param("limit") Integer limit, DataPlp dataPlp);

  /**
   * 根据输入的条件动态统计查询的点线面数据
   *
   * @param dataPlp 点线面数据
   * @return 返回查询结果
   */
  public Integer countFilesByObj(DataPlp dataPlp);

}
