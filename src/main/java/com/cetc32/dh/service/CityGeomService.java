/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/9/15 下午2:31
 ******************************************************************************/
package com.cetc32.dh.service;

import com.cetc32.dh.common.bean.TreeData;
import com.cetc32.dh.entity.CityGeom;
import com.cetc32.dh.entity.DataFile;
import com.cetc32.dh.entity.DataPlp;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface CityGeomService {

    List<TreeData> selectTreeData(String proCode);
    String selectCityInCodes(  List<String> areaCodes);
    List<String> selectCodesInArea(List<String> cities);
   List<CityGeom> selectCityInPolygon(String polygon);

  public int updateByName(CityGeom cityGeom);

  public String readFileContent(String fileName);

  public CityGeom queryByName(String name);

  public Map<String, String> areaToPolygon(List<String> areaNames);

  public Set<DataFile> menuAndPgonCom(Integer menuId, List<String> polygons);

  public Set<DataPlp> menuAndPgonPlm(List<String> polygons);

  public List<CityGeom> areaFindPolygon(List<String> ares);

  public Boolean judgePolygonContain(String polygon1, String polygon2);

  public Boolean judgePointContain(String point, String polygon);

  public List<String> queryAreaCodeByCity(String areaName);

  public List<String> queryAreaCodeByProvince(String areaName);

  public List<String> queryCityNameByProvince(String areaName);
}
