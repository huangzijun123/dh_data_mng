/*******************************************************************************
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/1/29 下午1:52
 ******************************************************************************/
package com.cetc32.dh.controller.views;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/open/")
public class OpenPageController {
    @GetMapping String api(){
        return "api";
    }
}
