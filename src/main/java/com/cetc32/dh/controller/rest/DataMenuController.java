/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.controller.rest;

import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.dto.DataMenuDTO;
import com.cetc32.dh.entity.DataFile;
import com.cetc32.dh.entity.DataMenu;
import com.cetc32.dh.service.DataMenuService;
import com.cetc32.dh.utils.FileUtil;
import com.cetc32.webutil.common.annotations.LoginSkipped;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.*;

/**
 * 数据管理目录目录、编目操作类
 *
 * @author: xiao
 * @version: 1.0
 * @date: 2020/10/14
 * 备注:无
 */
@Slf4j
@RestController
@RequestMapping("/rest/datamng/menu")
public class DataMenuController {

    @Autowired
    private DataMenuService dataMenuService;


    /**
     * 统计编目节点的个数
     *
     * @return 返回查询个数结果
     * 备注:无
     */
    @PostMapping(value = "/count")
    public ResponseResult count() {
        Integer count = dataMenuService.count();
        return ResponseResult.success(count);
    }

    /**
     * 插入编目节点
     *
     * @param dataMenu
     * @return 返回插入节点
     * 备注:无
     */
    //@PostMapping(value = "/insert")
    public ResponseResult insert(DataMenu dataMenu) {
//        DataMenu dataMenu=new DataMenu(2L,"test7",2L,"test7","test7");
        Integer count = dataMenuService.insertDataMenu(dataMenu);
        return ResponseResult.success(count);
    }


    /**
     * 根据编目id删除目录节点(单个)
     *
     * @param id
     * @return 返回删除结果
     * 备注:无
     */
    //@PostMapping(value = "/deleteOne")
    public ResponseResult delete(Long id) {
        Integer count = dataMenuService.deleteById(id);
        return ResponseResult.success(count);
    }

    /**
     * 查询父节点为pid的所有编目节点
     *
     * @param pid
     * @return 返回查询结果
     * 备注:无
     */
    //@PostMapping(value = "/queryByPid")
    public ResponseResult queryByPid(Long pid) {
        DataMenu dataMenu = dataMenuService.queryByPid(pid);
        return ResponseResult.success(dataMenu);
    }


    /**
     * 查询目录树节点中以id为父节点的所有编目节点
     *
     * @param id
     * @return 返回查询结果
     * 备注:无
     */
    //@PostMapping(value = "/queryMenusByPIdSatisfyId")
    public ResponseResult queryMenusByPIdSatisfyId(Long id) {
        List<DataMenu> dataMenuList = dataMenuService.queryByPIdSatisfyId(id);
        return ResponseResult.success(dataMenuList);
    }

    /**
     * 添加一个编目节点
     *
     * @param dataMenu
     * @return 返回添加结果
     * 备注:无
     */
    @ApiOperation(value = "新增编目")
    @PostMapping(value = "/addOneMenu")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pid", value = "父编目号", paramType = "body", dataType = "Integer", required = true),
            @ApiImplicitParam(name = "menuName", value = "编目名称", paramType = "body", dataType = "String", required = true),
            @ApiImplicitParam(name = "htmlUrl", value = "前端网页Url", paramType = "body", dataType = "String", required = true),
            @ApiImplicitParam(name = "icon", value = "编目图表", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "discription", value = "编目描述", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "body", dataType = "Integer"),
    })
    public ResponseResult addOneMenu(@RequestBody DataMenu dataMenu) {
//        dataMenu = new DataMenu(null, "test7", null, "test15", "test15");
        HashMap<String, Object> hashMap = new HashMap();
        if (dataMenu.getPid() == null) {
            dataMenu.setPid(0L);
        }
        if (dataMenu.getPid() == 0) {
            dataMenu.setAddkids(true);
        } else {
            dataMenu.setAddkids(false);
        }
        String path = dataMenuService.queryById(dataMenu.getPid()).getUrl();
        String url = path + File.separator + dataMenu.getMenuName();
        dataMenu.setUrl(url);
        UUID uuid = UUID.randomUUID();
        dataMenu.setKey(uuid.toString());
        dataMenu.setDisabled(false);
        List<DataMenu> dataMenuList = dataMenuService.queryByPIdSatisfyId(dataMenu.getPid());
        for (int i = 0; i < dataMenuList.size(); i++) {
            if (dataMenu.getMenuName().equals(dataMenuList.get(i).getMenuName())) {
                return ResponseResult.error("已经存在添加同根同名目录，不能添加！");
            }
        }
        String message = dataMenuService.addDataMenu(dataMenu);
        if (message.contains("成功")) {
            hashMap.put("key", uuid);
            Long id = (dataMenuService.queryByKey(uuid.toString())).getId();
            hashMap.put("id", id);
            String file_path = dataMenuService.queryById(id).getUrl();
            FileUtil.makeDir(file_path);
            return ResponseResult.success(hashMap);
        }
        return ResponseResult.error("添加失败！");
    }

    /**
     * 根据编目节点id，更新编目节点，只更新编目的描述信息
     *
     * @param menuId 编目节点id
     * @return 返回更新结果
     * 备注:无
     */
    @PostMapping(value = "/updateMenu")
    public ResponseResult updateDataMenu(@RequestBody DataMenu dataMenu) {
//      DataMenu dataMenu = dataMenuService.queryById(menuId);
//      dataMenu.setDiscription(discription);
      System.out.println(dataMenu.getDiscription());
      if(dataMenu!=null&&dataMenu.getId()!=null){
        if(dataMenuService.updatebyId(dataMenu)>0){
          return ResponseResult.success(200,"更新成功","编目"+dataMenuService.queryById(dataMenu.getId()).getMenuName()+"更新成功！");
        }else{
          return ResponseResult.error("更新失败！");
        }
      }else{
        return ResponseResult.error("未找到对应的编目，请输入正确的参数！");
      }
    }


    /**
     * 根据目录节点id，删除目录树(包括子树)
     *
     * @param map
     * @return 返回删除结果
     * 备注:无
     */
    @ApiOperation(value = "删除目录树")
    @PostMapping(value = "/deleteMenuTree")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keys", value = "删除的编目keys", dataType = "List", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "Integer", defaultValue = ""),
    })
    public ResponseResult deleteDataMenuTree(@RequestBody Map<String, Object> map) {
        List<String> keys = new ArrayList<>();
        keys = (List<String>) map.get("keys");
        String messsage = "";
        int result = 0;
        for (int i = 0; i < keys.size(); i++) {
            if (dataMenuService.queryByKey(keys.get(i)) == null) {
                result++;
                continue;
            }
            Long id = (dataMenuService.queryByKey(keys.get(i))).getId();
            Integer childNum = dataMenuService.countMenuChild(id);
            String dirPath = dataMenuService.queryById(id).getUrl();
            String name = dataMenuService.queryById(id).getMenuName();
            Integer count = dataMenuService.deleteMenuTree(id);
            if (count == childNum + 1) {
                FileUtil.deleteDir(dirPath);
                result++;
                messsage = messsage + "成功删除目录" + name + "及其子目录\n";
            } else {
                messsage = messsage + "删除目录" + name + "及其子目录失败\n";
            }
        }
        if (result == keys.size()) {
            messsage = messsage + "成功删除所选目录及其子目录！";
            return ResponseResult.success(messsage);
        } else {
            messsage = messsage + "删除目录失败！";
            return ResponseResult.error(messsage);
        }


    }

    /**
     * 根据编目key查找id
     *
     * @param map
     * @return 返回查找结果
     * 备注:无
     */
    @ApiOperation(value = "编目key到id转换")
    @PostMapping(value = "/keytoid")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "删除的编目key", dataType = "List", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "Integer", defaultValue = ""),
    })
    public ResponseResult keytoid(@RequestBody Map<String, Object> map) {
        String key = (String) map.get("key");
        if (dataMenuService.queryByKey(key) != null) {
            Long id = dataMenuService.queryByKey(key).getId();
            return ResponseResult.success(id);
        }
        return ResponseResult.error("请输入正确的key！");
    }

    /**
     * 根据目录节点id展示目录结构
     *
     * @param map
     * @return 返回展示结果
     * 备注:无
     */
    @LoginSkipped
    @ApiOperation(value = "显示目录树")
    @PostMapping(value = "/showMenuTree")
    public ResponseResult showMenuTree(@RequestBody Map<String, Object> map) {
        Long id;
        if (map.get("id") == null) {
            id = 0L;
        } else {
            id = ((Integer) map.get("id")).longValue();
        }
        List<DataMenuDTO> result = new ArrayList<DataMenuDTO>();
        DataMenuDTO dataMenuDTO = dataMenuService.getMenuTree(id);

        result = dataMenuDTO.getChildren();

        Collections.sort(result, new Comparator<DataMenuDTO>() {
            public int compare(DataMenuDTO o1, DataMenuDTO o2) {
                return Integer.parseInt(o1.getValue()) - Integer.parseInt(o2.getValue());
            }
        });

        return ResponseResult.success(dataMenuDTO.getChildren());
    }

  /**
   * 显示单个目录信息
   *
   * @param menuId
   * @return 返回展示结果
   * 备注:无
   */
  @ApiOperation(value = "显示单个目录信息")
  @GetMapping(value = "/showMenu")
  public ResponseResult showMenu(Long menuId) {
    Map<String,Object> map = new HashMap<>();
    DataMenu dataMenu = dataMenuService.queryById(menuId);
    if(dataMenu!=null){
      map.put("目录名称",dataMenu.getMenuName());
      map.put("目录描述信息",dataMenu.getDiscription());
      return ResponseResult.success(map);
    }else {
      return ResponseResult.error("未找到对应的目录，请传入正确的参数！");
    }

  }

    /**
     * (目录名称)编目名称和编目id
     *
     * @return 返回编目名称和编目id
     */
    public ResponseResult menuIdName() {
        List<DataMenu> dataMenuList = dataMenuService.selectAll();
        List<Map> hashMapList = new ArrayList<>();
        for (int i = 0; i < dataMenuList.size(); i++) {
            Map<String, String> hashmap = new HashMap<>();
            hashmap.put("value", dataMenuList.get(i).getId().toString());
            hashmap.put("label", dataMenuList.get(i).getMenuName());
            hashMapList.add(hashmap);
        }
        return ResponseResult.success(hashMapList);
    }


}


