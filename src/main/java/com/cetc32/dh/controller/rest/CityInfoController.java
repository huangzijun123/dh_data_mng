package com.cetc32.dh.controller.rest;

import com.cetc32.dh.common.response.PageDataResult;
import com.cetc32.dh.common.response.ResponseData;
import com.cetc32.dh.entity.CityInfo;
import com.cetc32.dh.service.CityInfoService;
import com.cetc32.webutil.common.annotations.LoginSkipped;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/city/")
@LoginSkipped
public class CityInfoController {
    @Autowired
    CityInfoService cityInfoService;

    @RequestMapping("/query")
    public PageDataResult queryCityInfos(@Nullable @RequestBody CityInfo cityInfo, @Nullable Integer page, @Nullable Integer results){
        if(cityInfo==null){
            cityInfo =new CityInfo();
        }
        if(page==null ){
            page = cityInfo.getPage();
        }
        if(results==null ){
            results = cityInfo.getResults();
        }
        if( page <=0){
            page =1;
        }
        if( results <=0){
            results =15;
        }
        cityInfo.setPage(page);
        cityInfo.setResults(results);
        int totals =cityInfoService.countCityInfoByParam(cityInfo);
        return PageDataResult.success(totals,
                cityInfoService.selectCityInfoByParam(cityInfo),cityInfo.getOffset());
        //return ResponseData.success();
    }


    /*******
     * 新增和更新为同一个接口
     * *****/
    @PostMapping("/info")
    public ResponseData insertOrUpdateCityInfos(@RequestBody CityInfo cityInfo){
        if(null == cityInfo)
            return ResponseData.error("输入参数为空");
        if(StringUtils.isBlank(cityInfo.getCity())){
            return ResponseData.error("城市名称不能为空");
        }
        if(StringUtils.isBlank(cityInfo.getCityCode())){
            return ResponseData.error("城市编码不能为空");
        }
        if(StringUtils.isBlank(cityInfo.getParentId())){
            return ResponseData.error("城市父亲ID不能为空");
        }
        if(StringUtils.isBlank(cityInfo.getPolygonText())){
            return ResponseData.error("城市区域点集不能为空");
        }
        if(StringUtils.isBlank(cityInfo.getMergerName())){
            return ResponseData.error("城市全称不能为空");
        }
        try{
            if(0<cityInfoService.insertUpdateCityInfo(cityInfo))
                return ResponseData.success("操作成功");
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseData.error("操作失败");
    }

    @PostMapping("/remove")
    public ResponseData removeCityInfos(@RequestBody CityInfo cityInfo){
        if(null ==cityInfo)
            return ResponseData.error("无效的参数");
        String cityCode = cityInfo.getCityCode();
        if(StringUtils.isBlank(cityCode)){
            return ResponseData.error("无效的城市编码");
        }
        try{
            cityInfoService.deleteCityInfo(cityCode);
            return ResponseData.success("操作成功");
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseData.error("操作失败");
    }
}
