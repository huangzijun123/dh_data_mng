/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/9/15 下午3:40
 ******************************************************************************/
package com.cetc32.dh.controller.rest;

import com.cetc32.dh.common.response.ResponseData;
import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.entity.CityGeom;
import com.cetc32.dh.entity.DataMenu;
import com.cetc32.dh.service.CityGeomService;
import com.cetc32.dh.service.DataAreaService;
import com.cetc32.dh.service.DataMenuService;
import com.cetc32.webutil.common.annotations.LoginSkipped;
import io.swagger.annotations.ApiOperation;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController

@RequestMapping("/rest/city/")
@LoginSkipped
public class CityGeomController {
    @Autowired
    CityGeomService cityGeomService;
    @Autowired
    DataAreaService dataAreaService;
    @Autowired
    DataMenuService dataMenuService;


    @LoginSkipped
    @GetMapping("/tree")
    public ResponseData tree(){
        return ResponseData.success(cityGeomService.selectTreeData(null));
    }


  /**
   * 判断某个点是否在一个面中
   * @return 返回判断结果
   * 备注:无
   */
  @ApiOperation(value = "判断点point是否在多边形polygon中")
  @PostMapping("/pointpolygon")
  public ResponseResult pointContain(@RequestBody Map<String, Object> map) {
    String point = (String) map.get("point");
    String polygon = (String) map.get("polygon");
//        String polygon ="POLYGON((1 1,1 6,6 6,6 1,1 1))";
//        String point ="POINT(2 2)";
    //经测试边界线上的点不包含在面中
    Boolean judge = cityGeomService.judgePointContain(point, polygon);
    if (judge == true) {
      return ResponseResult.success("判断结果为：点point包含面polygon");
    } else {
      return ResponseResult.success("判断结果为：点point不包含面polygon");
    }

  }

  /**
   * 判断多边形polygon2是否在多边形polygon1中
   * @return 返回判断结果
   * 备注:无
   */
  @ApiOperation(value = "判断多边形polygon2是否在多边形polygon1中")
  @PostMapping("/jugpolygon")
  public ResponseResult polygonContain(@RequestBody Map<String, Object> map) {
    String area_big = (String) map.get("areaBig");
    String area_small = (String) map.get("areaSmall");
    String polygon1 = "POLYGON((1 1,1 6,6 6,6 1,1 1))";
    String polygon2 = "POLYGON((2 2,2 5,5 5,5 2,2 2))";
    //经测验多边形包含其本身
    Boolean judge = cityGeomService.judgePolygonContain(area_big, area_small);
    if (judge == true) {
      return ResponseResult.success("判断结果为：面polygon1包含面polygon2");
    } else {
      return ResponseResult.success("判断结果为：面polygon1不包含面polygon2");
    }
  }


  /**
   * @Deprecated
   * city名称转换
   *  如返回："中国,陕西省,西安市;"
   */
  @Deprecated
  @PostMapping("/nameChange")
  public ResponseResult nameChange(@RequestBody Map<String, Object> map) {
    String areaNames = (String) map.get("areaNames");
    String[] names = areaNames.split(";");
    String name="";
    String newName="";
    StringBuffer result = new StringBuffer();
    CityGeom cityGeom;
    for (int i = 0; i < names.length; i++) {
      name = names[i];
      newName = name;
      cityGeom = cityGeomService.queryByName(name);
      while (cityGeom != null) {
        newName = cityGeom.getProvince() + "," + newName;
        name = cityGeom.getProvince();
        cityGeom = cityGeomService.queryByName(name);
      }
      result.append(newName);
      result.append(";");
    }
    return ResponseResult.success(result);
  }

  /**
   * @Deprecated
   * 返回指定区域的文件数量(以目录编号和目录结果map返回)
   *  返回数据结果
   *      "obj": [
   *         {
   *             "count": "1",
   *             "menuid": "13"
   *         },
   *         {
   *             "count": "8",
   *             "menuid": "147"
   *         }
   */
  @GetMapping("/countRegionFileNum")
  public ResponseResult countRegionFileNum(String areaName,String flag) {
    System.out.println(flag);
    List<String> areaCodes = new ArrayList<>();
    List<String> MidNames = new ArrayList<>();
    List<String> tmpCodes =new ArrayList<>();
    DataMenu dataMenu = null;
    List<HashMap<Integer, Integer>> result =new ArrayList<>();
    if(flag.equals("市")||flag.equals("省")) {
      areaCodes = cityGeomService.queryAreaCodeByCity(areaName);
      areaCodes.addAll(cityGeomService.queryAreaCodeByProvince(areaName));
    }
    if(flag.equals("国")){
      MidNames = cityGeomService.queryCityNameByProvince(areaName);
      for (String midName:MidNames) {
        tmpCodes =cityGeomService.queryAreaCodeByProvince(midName);
        tmpCodes.addAll(cityGeomService.queryAreaCodeByCity(midName));
        if(tmpCodes!=null&&tmpCodes.size()!=0){
          areaCodes.addAll(tmpCodes);
        }
      }
    }
    if(areaCodes.size()==0){
      return  ResponseResult.error("未找到相关区域信息和文件信息，请传入正确参数！");
    }else{
      result=dataAreaService.countByAreaCodes(areaCodes);
      for (Map map:result) {
        dataMenu = dataMenuService.queryById((long)map.get("menuid"));
        map.put("menuname",dataMenu.getMenuName());
      }
      return ResponseResult.success(result);
    }

  }

}
