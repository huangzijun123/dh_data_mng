package com.cetc32.dh.controller.rest;

import com.alibaba.fastjson.JSONObject;
import com.cetc32.dh.common.response.PageDataResult;
import com.cetc32.dh.common.response.ResponseData;
import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.common.utils.DateUtils;
import com.cetc32.dh.dto.DemandDTO;
import com.cetc32.dh.dto.DemandSubmitDTO;
import com.cetc32.dh.entity.DemandSubmit;
import com.cetc32.dh.entity.vDemand;
import com.cetc32.dh.service.DemandSubmitService;
import com.cetc32.dh.utils.FileUtil;
import com.cetc32.webutil.common.util.SecurityUserUtil;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 需求管理类
 * @author: hubin
 * @version: 1.0
 * @date: 2020/11/30
 * 备注:无
 */
@RestController
@RequestMapping("/demandsubmit")
public class DemanSubmitController {
    @Autowired
    DemandSubmitService demandSubmitService;

    @Value("${myPath}")
    String myPath;

    /**
     *展示所有需求
     * @return PageDateResult
     */
    @ApiOperation(value = "展示所有需求")
    @ApiImplicitParams({
            @ApiImplicitParam(name="classify", value="需求分类",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="searchname",value="需求名称",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="people",value="上报人",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name = "starttime", value = "开始时间", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name ="endtime",value = "结束时间", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name="page",value = "页码",dataType = "Integer",defaultValue = "0"),
            @ApiImplicitParam(name="result",value = "每页数据条数",dataType = "Integer",defaultValue = "10"),
            @ApiImplicitParam(name="status",value = "状态",dataType = "String",defaultValue = ""),
    })
    @PostMapping("/options")
    public PageDataResult demandsubmitAll(@RequestBody vDemand vdemand, @RequestParam(defaultValue = "0",required = false) Integer page, @RequestParam(defaultValue = "10",required = false) Integer results){
       if(StringUtils.isBlank(vdemand.classify)&&StringUtils.isBlank(vdemand.name)&&StringUtils.isBlank(vdemand.people)
               &&StringUtils.isBlank(vdemand.starttime)&&StringUtils.isBlank(vdemand.endtime)&&StringUtils.isBlank(vdemand.status)
            && StringUtils.isBlank(vdemand.getTd_end() )&& StringUtils.isBlank(vdemand.getTd_start())){

           List<DemandSubmit> demandlist = new ArrayList<>();
           Long count = 0L;
           demandlist = demandSubmitService.queryFilesByObj(vdemand);
           count = demandSubmitService.countFilesByObj(vdemand);
           return new PageDataResult(count,demandlist,vdemand.getOffset());
       }else if(StringUtils.isNotBlank(vdemand.name)&&(vdemand.name.contains("%"))){
           List<DemandSubmit> demandlist = new ArrayList<>();
           return new PageDataResult(0,demandlist,0);
       }
       else{
           String frontfirst = vdemand.getStarttime();
           String endTime = vdemand.getEndtime();
           Date firstdate =null,secondDate = null;
           DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
           DateFormat yMdHmsm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
           if(StringUtils.isNotBlank(vdemand.getName()))
                vdemand.setName("%"+vdemand.getName()+"%");
           if(vdemand.getStarttime()!=null&&vdemand.getEndtime()!=null){
               try{
                   firstdate = format1.parse(frontfirst);
                   secondDate = format1.parse(endTime);
                   vdemand.setSqlstarttime(firstdate);
                   vdemand.setSqlendtime(secondDate);
               }catch (ParseException e) {
                   e.printStackTrace();
               }
           }
           if(StringUtils.isNotBlank(vdemand.getTd_end())){
               try {
                   Date d = format1.parse(vdemand.getTd_end());
                   vdemand.setTd_end( yMdHmsm.format(DateUtils.ceilDay(d)));
               } catch (ParseException e) {
                   e.printStackTrace();
               }
           }
           if(StringUtils.isNotBlank(vdemand.getTd_start())){
               try {
                   Date d = format1.parse(vdemand.getTd_start());
                   vdemand.setTd_start( yMdHmsm.format(DateUtils.floorDay(d)));
               } catch (ParseException e) {
                   e.printStackTrace();
               }
           }
           List<DemandSubmit> demandlist = new ArrayList<>();
           Long count = 0L;
           /*if((vdemand.getName()!=null&&vdemand.getName().contains("%"))||(vdemand.getPeople()!=null&&vdemand.getPeople().contains("%"))){
            return new PageDataResult(count,demandlist,vdemand.getOffset());
           }*/
           demandlist = demandSubmitService.queryFilesByObj(vdemand);
           count = demandSubmitService.countFilesByObj(vdemand);
           return new PageDataResult(count,demandlist,vdemand.getOffset());
       }
    }

    /**
     * 新增需求
     * @param demandDTO
     * @return ResponseResult
     */
    @ApiOperation(value="新增需求")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value = "记录id号(不传参)", paramType = "body", dataType = "Integer", required = false),
            @ApiImplicitParam(name="projectName",value = "项目名称",paramType = "body",dataType = "String" ,defaultValue = ""),
            @ApiImplicitParam(name="demandName",value = "需求名称",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="demandDes",value = "需求描述",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="status",value = "状态",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="reporter",value = "报告人",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="departmentid",value = "部门索引",paramType = "body",dataType = "Integer",defaultValue = ""),
            @ApiImplicitParam(name="approver",value="人",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="getApproveFront",value = "前端传来的时间",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="getEndtimeFront",value = "前端传来的结束时间",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="demandClassify",value = "需求分类",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="demandAttachment",value = "附件",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="areachoice",value = "范围选择",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="userid",value = "当前操作的用户id",paramType = "body",dataType = "Integer",defaultValue = ""),
    })
    @PostMapping("/insertselect")
    public ResponseResult insertselect(@RequestBody DemandDTO demandDTO) {
        DemandSubmit demandSubmit = new DemandSubmit();
        BeanUtils.copyProperties(demandDTO,demandSubmit);
        Integer userid =  SecurityUserUtil.getUser().getId();
        if(demandSubmit.getDemandName()==null||demandSubmit.getDemandClassify()==null
                ||demandSubmit.getGetEndtimeFront()==null){
            return ResponseResult.error("传入空值！");
        }
        String entime = demandSubmit.getGetEndtimeFront();
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
       Date date = new Date();
        try{
            date = format1.parse(entime);
        }catch (ParseException e) {
            e.printStackTrace();
        }
        demandSubmit.setEndtime(date);
        String upCachePath=myPath+"_user"+userid;
        //BaseAdminUser adminUser=getCurrentUserId();
        String userName =SecurityUserUtil.getUser().getUsername();
        demandSubmit.setReporter(userName);
        String status="未审批";
        Date d= new Date();
        String timestamp = String.valueOf(d.getTime());
        String path = myPath+"admin"+timestamp;
        demandSubmit.setStatus(status);
        Date create = new Date();
        demandSubmit.setCreattime(create);
        if(FileUtil.copyFolder(upCachePath, path)){
            FileUtil.deleteDir(upCachePath);
        }else {
            return ResponseResult.error("upload fail");
        }
        demandSubmit.setDemandAttachment(path);
       if(demandSubmitService.insertSelective(demandSubmit)>0){
           return new ResponseResult("创建成功！");
       };
        return ResponseResult.error("wrong");
    }

    /**
     *选择指定偏移量的需求
     * @param pagesize
     * @param pagenum
     * @return PageDataResult
     */
    @ApiOperation(value = "选择指定偏移量的需求")
    @PostMapping("/selectbylimit")
    public PageDataResult selectbylimit(@ApiParam(value = "页码") Integer pagesize, @ApiParam(value = "每页数据条数") Integer pagenum){
        PageDataResult pdr = new PageDataResult();
        if(pagenum == null || pagenum == 0){
            pagenum =1;
        }
        if (pagesize == null || pagesize == 0)
            pagesize = 10;
        pdr.setList(demandSubmitService.selectByLimit((pagenum - 1) * pagesize, pagesize));
        pdr.setTotals(demandSubmitService.countDemand());
        return pdr;
    }

    /**
     * 选择指定索引的需求
     * @param id
     * @return ResponseResult
     */
    @ApiOperation(value = "选择指定索引的需求")
    @ApiImplicitParam(name="id",value = "索引",dataType = "Integer",defaultValue = "")
    @PostMapping("/selectbykey")
    public ResponseResult selsectbykey(Integer id){
        if(id == null){
            return ResponseResult.error("id为空！");
        }
        ResponseResult pdr = new ResponseResult();
        pdr.setObj(this.demandSubmitService.selectByPrimaryKey(id));
        return pdr;
    }

    /**
     * 删除指定的需求
     * @param id
     * @return ResponseResult
     */
    @ApiOperation(value = "删除指定索引的需求")
    @ApiImplicitParam(name="id",value = "索引",dataType = "Integer",defaultValue = "")
    @PostMapping("/deletebykey")
    public ResponseResult deletebyprimarykey(Integer id){
        if(id == null)
            return ResponseResult.error("传入值为空！");
        demandSubmitService.deleteByPrimaryKey(id);
        return new ResponseResult("Success");
    }

    /**
     * 更新指定的需求
     * @param demandSubmit
     * @return ResponseResult
     */
    @ApiOperation(value="更新需求")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value = "记录id号", paramType = "body", dataType = "Integer", required = false),
            @ApiImplicitParam(name="projectName",value = "项目名称",paramType = "body",dataType = "String" ,defaultValue = ""),
            @ApiImplicitParam(name="demandName",value = "需求名称",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="demandDes",value = "需求描述",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="status",value = "状态",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="reporter",value = "报告人",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="departmentid",value = "部门索引",paramType = "body",dataType = "Integer",defaultValue = ""),
            @ApiImplicitParam(name="approver",value="人",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="getApproveFront",value = "前端传来的时间",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="getEndtimeFront",value = "前端传来的结束时间",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="demandClassify",value = "需求分类",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="demandAttachment",value = "附件",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="areachoice",value = "范围选择",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="creattime",value = "创建时间",paramType = "body",dataType = "Timestamp",defaultValue = ""),
            @ApiImplicitParam(name="useid",value = "对应的用户id",paramType = "body",dataType = "Integer",defaultValue = ""),
    })
    @PostMapping("/updateselect")
    public ResponseResult updateselect(@RequestBody DemandSubmit demandSubmit) {
        if(demandSubmit == null){
            return ResponseResult.error("传入值为空！");
        }
        String entime = demandSubmit.getGetEndtimeFront();
        if(StringUtils.isNotBlank(entime)){
            DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            try{
                date = format1.parse(entime);
            }catch (ParseException e) {
                e.printStackTrace();
            }
            demandSubmit.setEndtime(date);
        }
        demandSubmitService.updateByPrimaryKeySelective(demandSubmit);
        return new ResponseResult("Success");
    }

    /**
     * 模糊查询
     * @param pageNum
     * @param pageSize
     * @param keyword
     * @return PageDataResult
     */
    @ApiOperation(value="查询接口")
    @PostMapping("/search")
    @ApiImplicitParam(name="keyword",value = "关键词",dataType = "String",defaultValue = "")
    public PageDataResult demandSubmitSearch(@ApiParam(value = "页码") Integer pageNum, @ApiParam(value = "每页数据条数") Integer pageSize, String keyword) {

        PageDataResult pdr = new PageDataResult();
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0)
            pageSize = 10;

        if(keyword.equals("all")){
            pdr.setTotals(demandSubmitService.countDemand());
            PageHelper.startPage(pageNum, pageSize);
            pdr.setList(demandSubmitService.findAll());
        }else {
            pdr.setTotals(demandSubmitService.findByKeyWord(keyword).size());
            PageHelper.startPage(pageNum, pageSize);
            pdr.setList(demandSubmitService.findByKeyWord(keyword));
        }

        return pdr;
    }

    /**
     * 目录/文件上传
     * @param file 目录，文件
     * @return 返回提交结果
     * */
    @ApiOperation(value = "文件上传")
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "文件夹下的文件list", paramType = "body", dataType = "List", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "用户id", paramType = "body", dataType = "Integer"),
    })
    public ResponseResult uploadFile(@ApiParam(value = "二进制文件流")  MultipartFile[] file) {
//        String filepath = "/root/upLoad";
        String upCachePath=myPath+"_user"+ SecurityUserUtil.getUser().getId();
        String filepath = upCachePath;
        String result = FileUtil.uploadFile(file, filepath);
        if (!result.contains("上传失败"))
            return ResponseResult.success(result);
        return ResponseResult.error(result);

    }


    /**
     * 文件上传缓冲区清空
     * @return 返回文件清空结果
     * */
    @ApiOperation(value = "清空历史上传")
    @RequestMapping(value = "/clearUploadFile", method = RequestMethod.POST)
    @ApiImplicitParam(name = "userId", value = "当前用户id")
    public ResponseResult clearUploadFile() {
        String upCachePath=myPath+"_user"+ SecurityUserUtil.getUser().getId();
        FileUtil.deleteDir(upCachePath);
        File file = new File(upCachePath);
        if (!file.exists()) {
            return ResponseResult.success("历史上传清空成功");
        }
        return ResponseResult.error("历史上传清空失败");
    }

    /**
     * 获取当前用户下的所有需求
     * @return PageDataResult
     */
    @PostMapping("/getdemand")
    public PageDataResult getUserAll(){
        //BaseAdminUser adminUser = adminUserService.queryById(SecurityUserUtil.getUser().getId());
        String name = SecurityUserUtil.getUser().getUsername();
        PageDataResult pdr = new PageDataResult();
        pdr.setTotals(demandSubmitService.countMineSubmit(name));
        pdr.setList(demandSubmitService.selectMySubmit(name));
        return pdr;
    }

    /**
     * 单个需求通过
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/acceptdemand")
    public ResponseResult acceptDemand(Integer id,String status){
        DemandSubmit demandSubmit = demandSubmitService.selectByPrimaryKey(id);
        demandSubmit.setStatus(status);
        demandSubmitService.updateByPrimaryKeySelective(demandSubmit);
        return new ResponseResult("done");
    }


    /**
     * 单个需求拒绝
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/rejectdemand")
    public ResponseResult rejectDemand(Integer id,String status){
        DemandSubmit demandSubmit = demandSubmitService.selectByPrimaryKey(id);
        demandSubmit.setStatus(status);
        demandSubmitService.updateByPrimaryKeySelective(demandSubmit);
        return new ResponseResult("done");
    }


    /**
     * 多个需求通过
     * @return 返回执行结果
     * **/
    @ApiOperation(value = "批量审批通过")
    @PostMapping("/accept")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "申请的记录id", dataType = "Map", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "当前操作的用户id", dataType = "Map", defaultValue = "")
    })
    public ResponseResult acceptMany(@RequestBody Map<String, Object> map) {
        List<Integer> ids = new ArrayList<>();
        ids = (List<Integer>) map.get("ids");
        DemandSubmit demandSubmit = new DemandSubmit();
        String reason=(String) map.get("reason");
        String staus = "审批通过";
        String userName =  SecurityUserUtil.getUser().getUsername();
        int sum = 0;
        for (int i = 0; i < ids.size(); i++) {
            Integer id = ids.get(i);
            demandSubmit = demandSubmitService.selectByPrimaryKey(id);
            demandSubmit.setApprover(userName);
            demandSubmit.setReason(reason);
            Date appt = new Date();
            demandSubmit.setApproceTime(appt);
            demandSubmit.setStatus(staus);
            if (demandSubmitService.updateByPrimaryKeySelective(demandSubmit) > 0) {
                sum = sum + 1;
            }
        }
        if (sum == ids.size())
            return ResponseResult.success("已通过");
        return ResponseResult.error("失败");
    }

    /**
     * 多个需求通过拒绝
     * @return 返回执行结果
     * **/
    @ApiOperation(value = "批量审批拒绝")
    @PostMapping("/refuse")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "申请的记录id", dataType = "Map", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "当前操作的用户id", dataType = "Map", defaultValue = "")
    })
    public ResponseResult refuseMany(@RequestBody Map<String, Object> map) {
        List<Integer> ids = new ArrayList<>();
        ids = (List<Integer>) map.get("ids");
        DemandSubmit demandSubmit = new DemandSubmit();
        String reason=(String) map.get("reason");
        String userName = SecurityUserUtil.getUser().getUsername();
        String staus = "审批拒绝";
        int sum = 0;
        for (int i = 0; i < ids.size(); i++) {
            Integer id = ids.get(i);
            demandSubmit = demandSubmitService.selectByPrimaryKey(id);
            demandSubmit.setApprover(userName);
            demandSubmit.setReason(reason);
            Date appt = new Date();
            demandSubmit.setApproceTime(appt);
            demandSubmit.setStatus(staus);
            if (demandSubmitService.updateByPrimaryKeySelective(demandSubmit) > 0) {
                sum = sum + 1;
            }
        }
        if (sum == ids.size())
            return ResponseResult.success("已拒绝！");
        return ResponseResult.error("失败");
    }




    /**
     * 下载功能
     * @return  下载后的文件名
     * **/
    @ApiOperation(value = "下载")
    @GetMapping("/download")
    @ApiImplicitParam(name = "id", value = "需求的记录id", dataType = "Integer", defaultValue = "")
    public String download(Integer id, HttpServletRequest request, HttpServletResponse response) throws Exception{
        DemandSubmit demandSubmit= demandSubmitService.selectByPrimaryKey(id);
        String path = demandSubmit.getDemandAttachment();
        // path是指欲下载的文件的路径。

//        // 取得文件名。
//        String filename = file.getName();
//        // 取得文件的后缀名。
//        String ext = filename.substring(filename.lastIndexOf(".") + 1).toUpperCase();
        try{
            String zipPath =  FileUtil.downloadAllAttachment(path,request,response);
            String filename = zipPath.substring(zipPath.lastIndexOf(File.separator)+1,zipPath.length());
            FileUtil.downloadFile(response,filename,zipPath);
            FileUtil.deleteDir(zipPath);
            return filename;
        }catch (Exception e){
            e.printStackTrace();
            return JSONObject.toJSONString(ResponseData.error("下载文件异常，文件可能已被删除！"));
        }


    }

    /**
     * 更新指定的需求
     * @param demandSubmitDTO
     * @return ResponseResult
     */
    @ApiOperation(value="根据状态获取需求id和name")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value = "记录id号", paramType = "body", dataType = "Integer", required = false),
            @ApiImplicitParam(name="demandName",value = "需求名称",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="demandStatus",value = "状态",paramType = "body",dataType = "String",defaultValue = ""),
    })
    @PostMapping("/searchbystatus")
    public PageDataResult searchStatus(@RequestBody DemandSubmitDTO demandSubmitDTO, @RequestParam(defaultValue = "0",required = false) Integer page, @RequestParam(defaultValue = "10",required = false) Integer results){
        if(demandSubmitDTO.getId()==null&&StringUtils.isBlank(demandSubmitDTO.getDeamndName())
                &&StringUtils.isBlank(demandSubmitDTO.getDemandStatus())){
            demandSubmitDTO.setDemandStatus("1");
            List<DemandSubmitDTO> demandSubmitDTOS = demandSubmitService.searchbystatus(demandSubmitDTO);
            Integer count = demandSubmitService.searchbystatus(demandSubmitDTO).size();
            return new PageDataResult(count,demandSubmitDTOS,page * results);
        }else {
            List<DemandSubmitDTO> demandSubmitDTOS = demandSubmitService.searchbystatus(demandSubmitDTO);
            Integer count = demandSubmitService.searchbystatus(demandSubmitDTO).size();
            return new PageDataResult(count,demandSubmitDTOS,page * results);
        }
    }

    @ApiOperation(value = "获取所有的分类")
    @PostMapping("/getclassify")
    public List<String> getAllclassify(){
        List<String> results = new ArrayList<>();
        results.add("全部分类");
        List<String> getclassify = demandSubmitService.alldemandclassify();
        for (int i = 0;i<getclassify.size();i++){
            results.add(getclassify.get(i));
        }
        return results;
    }
}
