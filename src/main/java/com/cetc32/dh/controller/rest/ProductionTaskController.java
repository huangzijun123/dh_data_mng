package com.cetc32.dh.controller.rest;

import com.cetc32.dh.beans.PackageTaskParams;
import com.cetc32.dh.common.response.ResponseData;
import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.entity.CityGeom;
import com.cetc32.dh.entity.DataFile;
import com.cetc32.dh.entity.DataMenu;
import com.cetc32.dh.entity.DataPlp;
import com.cetc32.dh.service.CityGeomService;
import com.cetc32.dh.service.DataFileService;
import com.cetc32.dh.service.DataMenuService;
import com.cetc32.dh.service.DataPlpService;
import com.cetc32.webutil.common.annotations.LoginSkipped;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/rest/prod/task/")
public class ProductionTaskController {

    CityGeomService cityGeomService;
    DataFileService dataFileService;
    DataPlpService dataPlpService;
    DataMenuService dataMenuService;
    @Value("${plpUrl}")
    String plpUrl;

    @Autowired
    public void setCityGeomService(CityGeomService cityGeomService) {
        this.cityGeomService = cityGeomService;
    }

    @Autowired
    public void setDataFileService(DataFileService dataFileService) {
        this.dataFileService = dataFileService;
    }

    @Autowired
    public void setDataPlpService(DataPlpService dataPlpService) {
        this.dataPlpService = dataPlpService;
    }

    @Autowired
    public void setDataMenuService(DataMenuService dataMenuService) {
        this.dataMenuService = dataMenuService;
    }
    /**
     * 根据封装parmas检索提取对应区域数据，menuId为多个存在两种情况，
     * 一种是完整的区域名称的方式回去对应信息，可能多个areaNames流程为；
     *      1、先根据areaNames获取对应的完整的国省市三级名称
     *      2、根据areaNames 获取对应Polygon信息
     *      3、获取对应MenuId下包含的当前区域的所有数据路径
     * 一种是基于params中的area信息检索对应的区域信息，可能多个Polygon，
     *      1、基于polygon获取对应国家省市三级名称
     *      2、获取包含该Polygon的对应MenuId下的数据
     * **/
    @PostMapping("/package")
    @LoginSkipped
    public ResponseData packageTaskParams(@RequestBody PackageTaskParams params){

        if(null == params)
            return ResponseData.error("无效的参数！");
        if(StringUtils.isBlank(params.getMenuId()))
            return ResponseData.error("缺少必要的数据分类");
        if(StringUtils.isBlank(params.getAreaNames())
                && StringUtils.isBlank(params.getArea()))
            return ResponseData.error("区域名称和区域点集至少有一个不能为空");
        Map<String,Object> results= new HashMap<>();
        if(StringUtils.isNotBlank(params.getAreaNames())
                && (StringUtils.isBlank(params.getArea())
                    || !params.getArea().trim().toUpperCase().startsWith("POLYGON"))){
            ResponseData msg =filterTaskInfoByNames(params, results);
            if(null != msg)
                return msg;
        }else if(params.getArea().trim().toUpperCase().startsWith("POLYGON")){
            String[] Polygons= params.getArea().split(";");
            String newName= "";
            for(String polygon:Polygons){
                List<CityGeom>cgs = cityGeomService.selectCityInPolygon(polygon);
                if(cgs.size()>0){
                    CityGeom cityGeom=null;
                    String name=cgs.get(0).getCity();
                    int count =0;
                    /**拼接国省市信息***/
                    while( (cityGeom=cityGeomService.queryByName(name)) != null){
                        count++;
                        if(StringUtils.isNotBlank(cityGeom.getCity())) {
                            newName  = cityGeom.getCity()  + ',' +newName;
                        }else{
                            break;
                        }
                        if(StringUtils.isNotBlank(cityGeom.getProvince())){
                            name =cityGeom.getProvince();
                        }else {
                            break;
                        }
                        if(count>10)/**防止部分数据异常陷入死循环*/
                            break;
                    }
                    newName = shortPackNames(newName);
                    if(null !=newName){
                        results.put("city",newName+";");
                    }else{
                        return ResponseData.error(polygon + "无法找到Polygon对应国家省市信息");
                    }
                }
            }
            if(StringUtils.isBlank(newName))
                return ResponseData.error(params.getArea()+"未检索到区域层级名称！");
            results.put("city",newName+";");
            Set<String> filePaths = new HashSet<>();
            String[] menuIds = params.getMenuId().split(",");
            String id = getPlpMenuId();
            for(String menuId:menuIds){;
                if(menuId.equals(id)){//dataPlpService
                    try{
                        packagePlpFileByPolygons(filePaths, Polygons);
                    }catch (Exception e){
                        return ResponseData.error(e.getMessage());
                    }
                }else{
                    DataFile dataFile =new DataFile();
                    dataFile.setMenuId(Integer.valueOf(menuId));
                    for(int i=0 ;i<Polygons.length;i++){
                        dataFile.setPoints(Polygons[i]);
                        try {
                            List<DataFile> lists = dataFileService.queryFilesByObj(0, Integer.MAX_VALUE, dataFile);
                            for(DataFile df :lists){
                                filePaths.add(df.getFilePath());
                            }
                        }catch (Exception e){
                            return ResponseData.error(e.getMessage());
                        }

                    }
                }
            }
            results.put("area",params.getArea());

            if(filePaths==null || filePaths.size() ==0)
                return ResponseData.error("未检索到指定区域可用数据！");
            results.put("path",filePaths);
        }else{
            return ResponseData.error("提供区域名称或者区域点集合无法");
        }
        return ResponseData.success(results);
    }

    private String getPlpMenuId() {
        List<DataMenu> menus = dataMenuService.selectAll();

        Optional<DataMenu> menu = menus.stream().filter(f->plpUrl.equals(f.getHtmlUrl())).findFirst();
        assert(menu.isPresent());
        String id = menu.get().getId()+"";
        return id;
    }

    /********************************************************
     *      1、先根据areaNames获取对应的完整的国省市三级名称
     *      2、根据areaNames 获取对应Polygon信息
     *      3、获取对应MenuId下包含的当前区域的所有数据路径
     * @param params  过滤数据需要的参数包括menuId，areaNames，areaq区域点集合
     * @param results 需要把结果封装成的数据供给后续使用
     * @return 正常情况下返回值为空，数据异常时候回返回ResponseData
     ************************************************************/
    private ResponseData filterTaskInfoByNames(PackageTaskParams params, Map<String, Object> results) {
        String[] names = params.getAreaNames().split(",");
        String name="",newName="",polygons="",errors="";
        StringBuffer cityies = new StringBuffer();
        CityGeom cityGeom;int count=0;
        for (int i = 0; i < names.length; i++) {
            name = names[i];
            newName = "";
            count=0;
            /**拼接国省市信息***/
            while( (cityGeom=cityGeomService.queryByName(name)) != null){
                count++;
                if(StringUtils.isNotBlank(cityGeom.getCity())) {
                    newName =  cityGeom.getCity()+","+newName;
                    if(names[i].trim().equals(cityGeom.getCity().trim())){/** 及时提取区域信息 */
                        polygons +=  cityGeom.getPolygonText().trim()+";";
                        polygons.replace(";;",";");
                    }
                }else{
                    break;
                }
                if(StringUtils.isNotBlank(cityGeom.getProvince())){
                    name =cityGeom.getProvince();
                }else {
                    break;
                }
                if(count>10)/**防止部分数据异常陷入死循环*/
                    break;
            }
            newName = shortPackNames(newName);
            if(null !=newName){
                cityies.append(newName);
                cityies.append(";");
            }else{
                errors += name +" ";
            }
        }
        Set<String> filePaths = new HashSet<>();
        String[] menuIds = params.getMenuId().split(",");
        String id =getPlpMenuId();
        for(String menuId:menuIds){;
            if(menuId.equals(id)){//dataPlpService
                packagePlpFileByPolygons(filePaths, polygons.split(";"));
                continue;
            }else{
                DataFile dataFile =new DataFile();
                dataFile.setMenuId(Integer.valueOf(menuId));
                for(int i=0 ;i<names.length;i++){
                    dataFile.setRegion(names[i]);
                    List<DataFile> lists = dataFileService.queryFilesByObj(0,Integer.MAX_VALUE,dataFile);
                    for(DataFile df :lists){
                        filePaths.add(df.getFilePath());
                    }
                }
            }
        }
        if(StringUtils.isNotBlank(errors)){
            return ResponseData.error("["+errors+"]未匹配到对应城市信息");
        }
        if(StringUtils.isBlank(cityies))
            return ResponseData.error(params.getAreaNames()+"未检索到区域层级名称！");
        results.put("city",cityies.toString());
        if(StringUtils.isBlank(polygons))
            return ResponseData.error(params.getAreaNames()+"未检索到区域点集合！");
        results.put("area",polygons);
        if(filePaths==null || filePaths.size() ==0)
            return ResponseData.error("未检索到指定区域可用数据！");
        results.put("path",filePaths);
        return null;
    }

    private String shortPackNames(String newName) {
        while (null != newName && newName.trim().endsWith(",")) {
            try {
                newName = newName.trim().substring(0,newName.trim().length()-1);
            } catch (StringIndexOutOfBoundsException e) {
                newName = null;
            }
        }
        return newName;
    }

    private void packagePlpFileByPolygons(Set<String> filePaths, String[] split) {
        Set<DataPlp> plps = cityGeomService.menuAndPgonPlm(Arrays.asList(split));
        if (null == plps || plps.size() == 0)
            return;
        Iterator<DataPlp> it = plps.iterator();
        while (it.hasNext()) {
            filePaths.add(it.next().getFileName());
        }
    }
}
