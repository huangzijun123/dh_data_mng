/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.controller.rest;

import com.cetc32.dh.common.response.PageDataResult;
import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.entity.DataPlp;
import com.cetc32.dh.entity.DataTrace;
import com.cetc32.dh.service.DataTraceService;
import com.cetc32.dh.utils.FileUtil;
import com.cetc32.webutil.common.util.SecurityUserUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 数据管理目录轨迹数据操作类
 *
 * @author: xiao
 * @version: 1.0
 * @date: 2020/10/14
 * 备注:无
 */
@RestController
@RequestMapping("/rest/trace/")
public class DataTraceController {

    @Autowired
    DataTraceService dataTraceService;

    /**
     * 提交轨迹数据
     *
     * @param dataTrace 轨迹数据实体类
     * @return 返回提交结果
     */
    @ApiOperation(value = "提交轨迹数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "security", value = "数据安全等级", paramType = "body", dataType = "Integer", defaultValue = ""),
            @ApiImplicitParam(name = "deviceid", value = "设备ID", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "fileTime", value = "文件年份", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "description", value = "数据描述", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "approver", value = "审批人", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "points", value = "点线面点集", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "flag", value = "标志位", paramType = "body", dataType = "Boolean", defaultValue = ""),
            @ApiImplicitParam(name = "fileConfig", value = "文件标识别，数据标识", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "body", dataType = "Integer"),
            @ApiImplicitParam(name = "encrylongitude", value = "偏移经度", paramType = "body", dataType = "Integer", defaultValue = ""),
            @ApiImplicitParam(name = "enccrylatitude", value = "偏移纬度", paramType = "body", dataType = "Integer", defaultValue = ""),
            @ApiImplicitParam(name = "linkid", value = "linkid", paramType = "body", dataType = "String"),
            @ApiImplicitParam(name = "coordinateerror", value = "精度", paramType = "body", dataType = "Integer", defaultValue = ""),
            @ApiImplicitParam(name = "locsource", value = "数据源", paramType = "body", dataType = "Integer", defaultValue = ""),
            @ApiImplicitParam(name = "direction", value = "方向", paramType = "body", dataType = "Integer"),
            @ApiImplicitParam(name = "speed", value = "速度", paramType = "body", dataType = "Integer", defaultValue = ""),
            @ApiImplicitParam(name = "lon", value = "经度", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "lat", value = "纬度", paramType = "body", dataType = "String"),
    })
    @PostMapping("/subtrace")
    public ResponseResult DataSub(@RequestBody DataTrace dataTrace) {
        dataTrace.setStatus("未审批");
        dataTrace.setSubmitor(SecurityUserUtil.getUser().getUsername());
        dataTrace.setCreateTime(new Date());
        if (dataTraceService.insertOne(dataTrace) > 0) {
            return ResponseResult.success("表单信息提交成功！");
        }
        return ResponseResult.error("表单信息提交失败！");
    }

    /**
     * 查看满足输入条件的轨迹数据
     *
     * @param dataTrace 轨迹数据实体
     * @return 返回查询结果
     */
    @ApiOperation(value = "满足输入条件的轨迹数据")
    @PostMapping("/approvestrace")
    public PageDataResult myAprroves(@RequestBody DataTrace dataTrace) {
        Integer page = dataTrace.getPage();
        Integer results = dataTrace.getResults();
        if (dataTrace.getTimeRange() != null && dataTrace.getTimeRange().length == 2) {
            dataTrace.setStartTimeCompare(dataTrace.getTimeRange()[0]);
            dataTrace.setEndTimeCompare(dataTrace.getTimeRange()[1]);
            System.out.println(dataTrace.getStartTimeCompare());
            System.out.println(dataTrace.getEndTimeCompare());
        }

        if (null == page || page <= 0)
            page = 1;
        if (null == results || results <= 0)
            results = 10;

        int offset = (page - 1) * results;

      List<DataTrace> dataTraceList = new ArrayList<DataTrace>();
      dataTraceList = dataTraceService.queryFilesByObj(offset, results, dataTrace);
      return new PageDataResult(dataTraceService.countFilesByObj(dataTrace), dataTraceList, offset);

//        return new PageDataResult(dataTraceService.queryFilesByObj(offset, results, dataTrace).size(),
//                dataTraceService.queryFilesByObj(offset, results, dataTrace), offset);
    }

    /**
     * 根据编目节点id查询已通过的数据
     *
     * @param map
     * @return 返回查询结果
     * 备注:无
     */
    @ApiOperation(value = "查询已通过的数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "menuId", value = "编目节点", paramType = "body", dataType = "Integer", required = true, defaultValue = ""),
    })
    @PostMapping("/traceQueryAccepted")
    public PageDataResult traceQueryAccepted(@RequestBody Map<String, Object> map) {
        DataTrace dataTrace = new DataTrace();
        Integer menuId = (Integer) map.get("menuId");
        dataTrace.setMenuId(menuId);
        dataTrace.setStatus("审批通过");
        Integer page = dataTrace.getPage();
        Integer results = dataTrace.getResults();

        if (null == page || page <= 0)
            page = 1;
        if (null == results || results <= 0)
            results = 10;

        int offset = (page - 1) * results;

        return new PageDataResult(dataTraceService.countFilesByObj(dataTrace),
                dataTraceService.queryFilesByObj(offset, results, dataTrace),
                offset);
    }


  /**
   * 记录数据详情展示
   *
   * @param id   文件id
   * @return 返回记录数据详情结果
   */
  @ApiOperation(value = "查看记录详情")
  @RequestMapping(value = "/tracedetail", method = RequestMethod.GET)
  public ResponseResult traceDetail(Integer id) {
    DataTrace dataTrace = dataTraceService.queryById(id);
    if(dataTrace!=null){
      return ResponseResult.success(200,"详情",dataTrace);
    }else{
      return  ResponseResult.error("未查找到相关信息，请输入正确的参数！");
    }
  }

    /**
     * 审批通过已导入的轨迹数据
     *
     * @param map
     * @return 返回执行结果
     **/
    @ApiOperation(value = "审批通过导入的轨迹数据")
    @PostMapping("/accepttrace")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "申请审批的记录id", dataType = "List", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "Integer", defaultValue = ""),
    })
    public ResponseResult acceptSubmit(@RequestBody Map<String, Object> map) {
        System.out.println(map);
        List<Integer> ids = new ArrayList<>();
        DataTrace dataTrace = new DataTrace();
        int sum = 0;
        ids = (List<Integer>) map.get("ids");
        String userName = SecurityUserUtil.getUser().getUsername();
        for (int i = 0; i < ids.size(); i++) {
            Integer id = ids.get(i);
            dataTrace = dataTraceService.queryById(id);
            dataTrace.setStatus("审批通过");
            dataTrace.setApproveTime(new Date());
            dataTrace.setApprover(userName);
            if (dataTraceService.updateById(dataTrace) > 0) {
                sum++;
            }
        }
        if (sum == ids.size())
            return ResponseResult.success("已审批通过");
        return ResponseResult.error("审批失败");

    }


    /**
     * 审批拒绝导入的轨迹数据
     *
     * @param map
     * @return 返回执行结果
     **/
    @ApiOperation(value = "审批拒绝导入的轨迹数据")
    @PostMapping("/rejecttrace")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "申请时审批的记录id", dataType = "List", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "Integer", defaultValue = ""),
    })
    public ResponseResult rejectSubmit(@RequestBody Map<String, Object> map) {
        System.out.println(map);
        List<Integer> ids = new ArrayList<>();
        DataTrace dataTrace = new DataTrace();
        int sum = 0;
        ids = (List<Integer>) map.get("ids");
        String userName = SecurityUserUtil.getUser().getUsername();
        for (int i = 0; i < ids.size(); i++) {
            Integer id = ids.get(i);
            dataTrace = dataTraceService.queryById(id);
            dataTrace.setStatus("审批拒绝");
            dataTrace.setApprover(userName);
            dataTrace.setApproveTime(new Date());
            if (dataTraceService.updateById(dataTrace) > 0) {
                sum++;
            }
        }
        if (sum == ids.size())
            return ResponseResult.success("已审批拒绝");
        return ResponseResult.error("审批失败");
    }


    /**
     * 删除轨迹数据记录
     *
     * @param map
     * @return 返回执行结果
     **/
    @ApiOperation(value = "删除轨迹数据")
    @PostMapping("/deltrace")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "申请删除的文件id", dataType = "List", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "Integer", defaultValue = ""),
    })
    public ResponseResult delTraceData(@RequestBody Map<String, Object> map) {
        System.out.println(map);
        List<Integer> ids = new ArrayList<>();
        int sum = 0;
        ids = (List<Integer>) map.get("ids");
        Integer userId =  SecurityUserUtil.getUser().getId();
//        String userName = null;
//        if ((adminUserService.getUserById(userId)) != null) {
//            userName = adminUserService.getUserById(userId).getSysUserName();
//        }
        for (int i = 0; i < ids.size(); i++) {
            Integer id = ids.get(i);
            if (dataTraceService.deleteById(id) > 0) {
                sum++;
            }
        }
        if (sum == ids.size()) {
            return ResponseResult.success("删除成功");
        }
        return ResponseResult.error("删除失败");
    }


    /**
     * 下载轨迹数据文件
     *
     * @param fileIds 待下载文件id
     * @return 返回下载结果
     */
    @ApiOperation(value = "下载轨迹数据文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileIds", value = "文件id", paramType = "body", dataType = "List"),
    })
    @GetMapping("/downloadMulFile")
    public String downloadMulFile(String[] fileIds, HttpServletResponse response) {
//        System.out.println(fileIds.length+"xxxxxxxxx");

        if (fileIds.length == 1) {
            Integer fileId = Integer.parseInt(fileIds[0]);
            System.out.println("文件号！！！！" + fileId);
            String fileName = dataTraceService.queryById(fileId).getFileName();
//            String filePath = dataFileService.queryById(fileId).getFilePath() + File.separator + dataFileService.queryById(fileId).getFileName();
            String filePath = dataTraceService.queryById(fileId).getFilePath();
            if (!FileUtil.isDirectory(filePath)) {
                FileUtil.downloadFile(response, fileName, filePath);
            }
        }

        String message = null;
        String directory = "/root/load";
        File directoryFile = new File(directory);
        if (!directoryFile.isDirectory() && !directoryFile.exists()) {
            directoryFile.mkdirs();
        }
        //设置最终输出zip文件的目录+文件名
        String zipFileName = "已下载文件" + ".zip";
        String strZipPath = directory + "/" + zipFileName;
        File zipFile = new File(strZipPath);
        //读取需要压缩的文件
        Integer fileId = null;
        String fileName = null;

        List<String> fileNames = new ArrayList<>();
        List<String> filePaths = new ArrayList<>();
        for (int i = 0; i < fileIds.length; i++) {
            fileId = Integer.parseInt(fileIds[i]);
            if (dataTraceService.queryById(fileId) != null) {
                fileName = dataTraceService.queryById(fileId).getFilePath();
//                fileName = dataFileService.queryById(fileId).getFilePath() + File.separator + dataFileService.queryById(fileId).getFileName();
                if (FileUtil.isDirectory(fileName)) {
                    List<String> stringList = FileUtil.getAllFile(fileName);
                    for (int j = 0; j < stringList.size(); j++) {
                        fileNames.add(stringList.get(j));
                        filePaths.add(fileName.substring(0, fileName.lastIndexOf("/")));
//                        filePaths.add(dataFileService.queryById(fileId).getFilePath());
                    }
                } else {
                    fileNames.add(fileName);
                    filePaths.add(fileName.substring(0, fileName.lastIndexOf("/")));
//                    filePaths.add(dataFileService.queryById(fileId).getFilePath());
                }
            }
        }

        ZipOutputStream zipStream = null;
        FileInputStream zipSource = null;
        BufferedInputStream bufferStream = null;
        try {
            //构造最终压缩包的输出流
            zipStream = new ZipOutputStream(new FileOutputStream(zipFile));
            for (int i = 0; i < fileNames.size(); i++) {
                //解码获取真实路径与文件名
//                String realFilePath = java.net.URLDecoder.decode(fileNames.get(i), "UTF-8");
                String realFilePath = fileNames.get(i);
                System.out.println(realFilePath);
                File file = new File(realFilePath);
                //TODO:未对文件不存在时进行操作，后期优化。
                if (file.exists()) {
                    zipSource = new FileInputStream(file);//将需要压缩的文件格式化为输入流
                    /**
                     * 压缩条目不是具体独立的文件，而是压缩包文件列表中的列表项，称为条目，就像索引一样这里的name就是文件名,
                     * 文件名和之前的重复就会导致文件被覆盖
                     */
//                    ZipEntry zipEntry = new ZipEntry("("+i+")"+fileNames.get(i).split("/")[fileNames.get(i).split("/").length-1]);//在压缩目录中文件的名字
                    ZipEntry zipEntry = new ZipEntry(fileNames.get(i).replace(filePaths.get(i), ""));//在压缩目录中文件的名字
                    zipStream.putNextEntry(zipEntry);//定位该压缩条目位置，开始写入文件到压缩包中
                    bufferStream = new BufferedInputStream(zipSource, 1024 * 10);
                    int read = 0;
                    byte[] buf = new byte[1024 * 10];
                    while ((read = bufferStream.read(buf, 0, 1024 * 10)) != -1) {
                        zipStream.write(buf, 0, read);
                    }
                } else {
                    message = message + file.getName() + "不存在！" + "\n";
                    System.out.println(file.getName() + "不存在！");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //关闭流
            try {
                if (null != bufferStream) bufferStream.close();
                if (null != zipStream) {
                    zipStream.flush();
                    zipStream.close();
                }
                if (null != zipSource) zipSource.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //判断系统压缩文件是否存在：true-把该压缩文件通过流输出给客户端后删除该压缩文件  false-未处理
        if (zipFile.exists()) {
            System.out.println(zipFileName);
            System.out.println(strZipPath);
            FileUtil.downloadFile(response, zipFileName, strZipPath);
            FileUtil.deleteDir(directory);
        }
        if (message == null) {
            message = "全部文件下载成功！";
        }
        return message;

    }


}
