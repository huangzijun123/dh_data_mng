/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.controller.rest;

import com.cetc32.dh.common.response.PageDataResult;
import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.entity.DataFile;
import com.cetc32.dh.entity.DataMenu;
import com.cetc32.dh.entity.DataPlp;
import com.cetc32.dh.entity.DataTrace;
import com.cetc32.dh.service.DataFileService;
import com.cetc32.dh.service.DataMenuService;
import com.cetc32.dh.service.DataPlpService;
import com.cetc32.dh.service.DataTraceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Stream;

/**
 * 数据管理公共接口
 * @author: xiao
 * @version: 1.0
 * @date: 2020/10/14
 * 备注:无
 */
@Slf4j
@RestController
@RequestMapping("/rest/datamanage/common")
public class DataCommonController {
    @Value("${plpUrl}")
    private String plpUrl;
    @Value("${traceUrl}")
    private String traceUrl;
    @Autowired
    private DataFileService dataFileService;
    @Autowired
    private DataPlpService dataPlpService;
    @Autowired
    private DataTraceService dataTraceService;
    @Autowired
    private DataMenuService dataMenuService;


    /**
     * 统计今日提交或审批数管个数
     *
     * @param map status审核
     * @return 返回查询个数结果
     * 备注:无
     */
    @PostMapping(value = "/todaydataCount")
    public ResponseResult countTodayData(@RequestBody Map<String, Object> map) {
        DataFile dataFile = new DataFile();
        DataTrace dataTrace = new DataTrace();
        DataPlp dataPlp = new DataPlp();
        int count_all;
        int count;
        String status = (String) map.get("status");
        LocalDateTime today_start = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);//当天零点
        String td_start = today_start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime today_end = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);//当天最晚点
        String td_end = today_end.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        dataFile.setTd_start(td_start);
        dataFile.setTd_end(td_end);
        dataPlp.setTd_start(td_start);
        dataPlp.setTd_end(td_end);
        dataTrace.setTd_start(td_start);
        dataTrace.setTd_end(td_end);
        dataFile.setStatus(null);
        dataPlp.setStatus(null);
        dataTrace.setStatus(null);
        count_all = dataFileService.countFilesByObj(dataFile);
        count_all += dataPlpService.countFilesByObj(dataPlp);
        count_all += dataTraceService.countFilesByObj(dataTrace);
        if (status == null || status.contains("全部审批") || StringUtils.isBlank(status)) {
            return ResponseResult.success(count_all);
        } else {
            status = "未审批";
            dataFile.setStatus(status);
            dataPlp.setStatus(status);
            dataTrace.setStatus(status);
            count = dataFileService.countFilesByObj(dataFile);
            count += dataPlpService.countFilesByObj(dataPlp);
            count += dataTraceService.countFilesByObj(dataTrace);
            count_all = count_all - count;
            return ResponseResult.success(count_all);
        }
    }

    /**
     * 统计今日提交或审批数管个数
     *
     * @param map status审核
     * @return 返回查询个数结果
     * 备注:无
     */
    @PostMapping(value = "/todaydataShow")
    public PageDataResult showTodayData(@RequestBody Map<String, Object> map) {
        DataFile dataFile = new DataFile();
        DataTrace dataTrace = new DataTrace();
        DataPlp dataPlp = new DataPlp();
        int total;
        List<Object> objectList = new ArrayList<Object>();
        String status;
        try {
            status = (String) map.get("status");
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        LocalDateTime today_start = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);//当天零点
        String td_start = today_start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime today_end = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);//当天最晚点
        String td_end = today_end.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        dataFile.setTd_start(td_start);
        dataFile.setTd_end(td_end);
        dataPlp.setTd_start(td_start);
        dataPlp.setTd_end(td_end);
        dataTrace.setTd_start(td_start);
        dataTrace.setTd_end(td_end);
        if (status == null || status.contains("全部审批") || StringUtils.isBlank(status)) {
            dataFile.setStatus(null);
            dataPlp.setStatus(null);
            dataTrace.setStatus(null);
        } else {
            dataFile.setStatus(status);
            dataPlp.setStatus(status);
            dataTrace.setStatus(status);
        }
        List<DataFile> dataFileList = new ArrayList<>();
        List<DataPlp> dataPlpList = new ArrayList<>();
        List<DataTrace> dataTraceList = new ArrayList<>();
        dataFileList = dataFileService.queryFilesByObj(null, null, dataFile);
        dataPlpList = dataPlpService.queryFilesByObj(null, null, dataPlp);
        dataTraceList = dataTraceService.queryFilesByObj(null, null, dataTrace);
        for (DataFile dataFileOne : dataFileList) {
            objectList.add(dataFileOne);
        }
        for (DataPlp dataPlpOne : dataPlpList) {
            objectList.add(dataPlpOne);
        }
        for (DataTrace dataTraceOne : dataTraceList) {
            objectList.add(dataTraceOne);
        }
        System.out.println(objectList.size());
        total = objectList.size();
        return new PageDataResult(objectList, total, 200);
    }

    /**
     * 统计今日提交或审批数管个数
     *
     * @param map status审核
     * @return 返回查询个数结果
     * 备注:无
     */
    @PostMapping(value = "/approveRate")
    public ResponseResult approveRate(@RequestBody Map<String, Object> map) {
        List<Object> list = new ArrayList<Object>();
        List<Integer> menuIds = (List<Integer>) map.get("menuIds");
        String status0 = (String) map.get("status");
        final String status;
        if (status0 == null || status0.contains("全部审批") || StringUtils.isBlank(status0)) {
            status = null;
        }else{
            status = status0;
        }

        List<DataMenu> allMenus =dataMenuService.selectAll();
        //final Optional<DataMenu> plpMenu =null ,traceMenu =null;
        Stream<DataMenu> statisMenus=null;
        final Optional<DataMenu> plpMenu = allMenus.stream().filter(f-> plpUrl.equals(f.getHtmlUrl())).findFirst();
        assert(plpMenu.isPresent());
        final Optional<DataMenu> traceMenu = allMenus.stream().filter(f-> traceUrl.equals(f.getHtmlUrl())).findFirst();
        assert(traceMenu.isPresent());
        allMenus.stream().filter(f-> "statistic".equals(f.getTags())).limit(allMenus.size()).forEach(f-> {
            Integer menuId= f.getId().intValue();


            Integer num;
        /*for (Integer menuId : menuIds) {*/
            if ( menuId == (traceMenu.get().getId().intValue())) {
                DataTrace dataTrace = new DataTrace();
                dataTrace.setStatus(status);
                dataTrace.setMenuId(menuId);
                num = dataTraceService.countFilesByObj(dataTrace);
                Map<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put("name", traceMenu.get().getMenuName());
                hashMap.put("value", num);
                list.add(hashMap);
//                hashMap.put(dataMenuService.queryById(menuId.longValue()).getMenuName()+":未审批百分比", (float) un_approve / total);
//                hashMap.put(dataMenuService.queryById(menuId.longValue()).getMenuName()+":已审批百分比", 1-(float) un_approve / total);
            } else if (menuId == plpMenu.get().getId().intValue()) {
                DataPlp dataPlp = new DataPlp();
                dataPlp.setStatus(status);
                dataPlp.setMenuId(menuId);
                num = dataPlpService.countFilesByObj(dataPlp);
                Map<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put("name", plpMenu.get().getMenuName());
                hashMap.put("value", num);
                list.add(hashMap);
            } else {
                DataFile dataFile = new DataFile();
                dataFile.setStatus(status);
                dataFile.setMenuId(menuId);
                num = dataFileService.countFilesByObj(dataFile);
                Map<String, Object> hashMap = new HashMap<String, Object>();
                Optional<DataMenu> optDataMenu= allMenus.stream().filter(o-> menuId.intValue()== o.getId().intValue()).findFirst();
                assert(optDataMenu.isPresent());
                hashMap.put("name", optDataMenu.get().getMenuName());
                hashMap.put("value", num);
                list.add(hashMap);
            }

        //}

        });
        return ResponseResult.success(list);
    }

    /**
     * 统计今日提交或审批数管个数
     *
     * @param map status审核
     * @return 返回查询个数结果
     * 备注:无
     */
    @PostMapping(value = "/sevenDayAdd")
    public ResponseResult sevenDayAdd(@RequestBody Map<String, Object> map) {
        List<Object> list = new ArrayList<Object>();
        //List<Integer> menuIds = (List<Integer>) map.get("menuIds");

        LocalDateTime today_start = LocalDateTime.of(LocalDate.now().minusDays(7), LocalTime.now());//当天零点
        String td_start = today_start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime today_end = LocalDateTime.of(LocalDate.now(), LocalTime.now());//当天最晚点
        String td_end = today_end.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        List<DataMenu> allMenus =dataMenuService.selectAll();
        //final Optional<DataMenu> plpMenu =null ,traceMenu =null;
        Stream<DataMenu> statisMenus=null;
        final Optional<DataMenu> plpMenu = allMenus.stream().filter(f-> plpUrl.equals(f.getHtmlUrl())).findFirst();
        assert(plpMenu.isPresent());
        final Optional<DataMenu> traceMenu = allMenus.stream().filter(f-> traceUrl.equals(f.getHtmlUrl())).findFirst();
        assert(traceMenu.isPresent());
        allMenus.stream().filter(f-> "statistic".equals(f.getTags())).limit(allMenus.size()).forEach(f-> {
            Integer menuId = f.getId().intValue();
            Integer add_num;
            if (menuId == (traceMenu.get().getId().intValue())) {
                DataTrace dataTrace = new DataTrace();
                dataTrace.setTd_start(td_start);
                dataTrace.setTd_end(td_end);
                add_num = dataTraceService.countFilesByObj(dataTrace);
                Map<String, Object> hashmap = new HashMap<String, Object>();
                hashmap.put("name", traceMenu.get().getMenuName());
                hashmap.put("value", add_num);
                list.add(hashmap);
            } else if (menuId == plpMenu.get().getId().intValue()) {
                DataPlp dataPlp = new DataPlp();
                dataPlp.setTd_start(td_start);
                dataPlp.setTd_end(td_end);
                add_num = dataPlpService.countFilesByObj(dataPlp);
                Map<String, Object> hashmap = new HashMap<String, Object>();
                hashmap.put("name",  plpMenu.get().getMenuName());
                hashmap.put("value", add_num);
                list.add(hashmap);
            } else {
                DataFile dataFile = new DataFile();
                dataFile.setTd_start(td_start);
                dataFile.setTd_end(td_end);
                dataFile.setMenuId(menuId);
                add_num = dataFileService.countFilesByObj(dataFile);
                Map<String, Object> hashmap = new HashMap<String, Object>();
                hashmap.put("name", dataMenuService.queryById(menuId.longValue()).getMenuName());
                hashmap.put("value", add_num);
                list.add(hashmap);
            }

        });
        return ResponseResult.success(list);
    }




}
