package com.cetc32.dh.controller.rest;

import com.alibaba.fastjson.JSONObject;
import com.cetc32.dh.beans.DhOflineData;
import com.cetc32.dh.beans.ReqSubmit;
import com.cetc32.dh.beans.TraceUpload;
import com.cetc32.dh.common.response.ResponseData;
import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.entity.*;
import com.cetc32.dh.service.*;

import com.cetc32.dh.utils.FileUtil;
import com.cetc32.webutil.common.annotations.LoginSkipped;
import com.cetc32.webutil.common.util.SecurityUserUtil;
/*import com.mysql.cj.util.Base64Decoder;*/
import io.swagger.annotations.ApiOperation;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/rest")
public class OtherInterfaceController {
    @Value("${offlineDataMenuId}")
    Integer offLineDataMenuId;
    @Value("${gainMenuId}")
    Integer gainMenuId;
    @Autowired
    DataFileService dataFileService;

    @Autowired
    DemandSubmitService demandSubmitService;

    @Autowired
    ProductdemandService productdemandService;

    @Autowired
    EstimateTaskService estimateTaskService;

    @Autowired
    DataTraceService dataTraceService;

    @Autowired
    DataPlpService dataPlpService;

    @Value("${upLoadPath}")
    String upLoadPath;

    @ApiOperation(value = "生产业务需求上报")
    @PostMapping("/req/submit")
    public ResponseData reqSubmit(ReqSubmit req, MultipartFile files)
    {
        DemandSubmit submit=new DemandSubmit();
        //submit.setProjectName(req.getProject());
        submit.setArea(req.getArea());
        submit.setDepartmentid(req.getDepartment());
        submit.setDemandDes(req.getDescription());
        submit.setEndtime(req.getDuedate());
        submit.setDemandName(req.getName());
        submit.setReporter(SecurityUserUtil.getUser().getUsername());
        submit.setDemandAttachment(null);
        submit.setStatus("未审批");
        submit.setDemandClassify(req.getDemandClassify());
        Date create = new Date();
        submit.setCreattime(create);
        if(null != files){
            String upCachePath = upLoadPath  +  "_"+SecurityUserUtil.getUser().getId()+"_"+System.currentTimeMillis();
            String filepath = upCachePath;
            String result = FileUtil.uploadFile(new MultipartFile[]{files}, filepath);
            if (result.contains("上传失败"))
                return ResponseData.error(result);
            submit.setDemandAttachment(filepath);
        }
        if(demandSubmitService.insertSelective(submit)>0)
        {
            return ResponseData.success("上报成功");
        }
        return ResponseData.error("上报失败");
    }

    @ApiOperation(value = "生产业务需求状态反馈")
    @PostMapping("/req/status")
    public ResponseData requestStatus(@RequestBody  Map<String,Object> map)
    {
        Integer reqId = (Integer) map.get("reqId");
        String status = (String)map.get("status");
        if(null == reqId && null == status)
            return ResponseData.error("无效的需求ID和状态");
        switch(status){
            case "审批通过": case "审批拒绝":;break;
            default:return ResponseData.error("无效的状态信息");
        }
        DemandSubmit  result = new DemandSubmit();
        result.setId(reqId);
        result.setStatus(status);
        demandSubmitService.updateByPrimaryKeySelective(result);
        return ResponseData.success();

    }

    @LoginSkipped
    @ApiOperation(value = "生产任务状态反馈")
    @PostMapping("/callback/product")
    public ResponseData callbackProduct(@RequestBody Map<String,String> req)
    {
        try {

            Integer taskid=req.get("taskid")==null?null:Integer.parseInt(req.get("taskid")) ;
            String status=req.get("status");
            if(taskid!=null && status!=null)
            {
                Productdemand submit=productdemandService.selectByPrimaryKey(taskid);
                if(submit!=null)
                {
                    submit.setStatus(status);
                    if(productdemandService.updateByPrimaryKeySelective(submit)>0)
                    {
                        return ResponseData.success("状态反馈成功");
                    }
                    else
                    {
                        return ResponseData.error("插入失败！");
                    }
                }else
                {
                    return ResponseData.error("taskid不存在！");
                }
            }
            else
            {
                return ResponseData.error("taskid 和 status不能为空！");
            }
        }
        catch (NumberFormatException ex) {
            ex.printStackTrace();
            return ResponseData.error("状态反馈失败！");
        }
    }
    @ApiOperation(value = "评估任务状态反馈")
    @PostMapping("/callback/evaluation")
    public ResponseData callbackEvaluation(@RequestBody JSONObject req)
    {
        try {

            Integer taskid=req.getInteger("taskid") ;
            String status=req.getString("status");
            JSONObject report =req.getJSONObject("report");
            if(taskid!=null && status!=null)
            {
                EstimateTask submit=estimateTaskService.selectByPrimaryKey(taskid);
                if(submit!=null)
                {
                    /*if(null !=report){
                        if(StringUtils.isBlank(submit.getTaskPath())){
                            submit.setTaskPath(upLoadPath  +  "_"+SecurityUserUtil.getUser().getId()+"_"+System.currentTimeMillis());
                        }
                        saveBase64File(submit.getTaskPath(),0,0,report.split("base64,"),"EstimateReports",null,null);*//*

                        submit.setTaskPath(filepath);
                    }*/

                    submit.setStatus(status);
                    if(null != report)
                        submit.setReport(report.toJSONString());
                    if(estimateTaskService.updateByPrimaryKeySelective(submit)>0)
                    {
                        return ResponseData.success("状态反馈成功");
                    }
                    else
                    {
                        return ResponseData.error("插入失败！");
                    }
                }else
                {
                    return ResponseData.error("taskid不存在！");
                }
            }
            else
            {
                return ResponseData.error("taskid 和 status不能为空！");
            }
        }
        catch (NumberFormatException ex) {
            ex.printStackTrace();
            return ResponseData.error("状态反馈失败！");
        }

    }

    @ApiOperation(value = "数据获取",notes = "数据获取获取指定时间空间数据")
    @GetMapping(value = "/export/ploygon")
    public ResponseData DataPloygon(@RequestBody Map<String, Object> map) {
        String status = "审批通过";
        SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd" );
        Date startTime;
        Date endTime;
        try {
            startTime=map.get("starttime")==null?null:sdf.parse((String)map.get("starttime"));
            endTime=map.get("endtime")==null?null:sdf.parse((String)map.get("endtime"));
        } catch (ParseException e) {

            return ResponseData.error("日期格式有误（允许格式为：yyyy-MM-dd）");
        }
        String polygon=map.get("polygon")==null?null:(String) map.get("polygon");
        if(polygon==null)
        {
            return ResponseData.error("缺少参数 polygon ");
        }
        return ResponseData.success(dataPlpService.selectPloygon(status,startTime,endTime,polygon));
    }




    @PostMapping("/import/trajectory")
    public ResponseResult DataTrajectory(@RequestBody TraceUpload trace) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        DataTrace dt =new DataTrace();
        dt.setFileSize(trace.getSize());
        try {
            dt.setEndTime(format.parse(trace.getEndtime()));
            dt.setStartTime(format.parse(trace.getStarttime()));
        } catch (ParseException e) {
            return ResponseResult.error(" starttime 或 endtime有误！");
        }

        dt.setFilePath(trace.getPath());
        dt.setFileConfig(trace.getTitle());
        if(dataTraceService.insertOne(dt)>0)
        {
            return ResponseResult.success("上报成功！");
        }
        return ResponseResult.error("上报失败！");

    }

    @LoginSkipped
    @ApiOperation(value = "成果数据上报")
    @PostMapping("/import/gain")
    public ResponseData DataGain(@RequestBody DataFile dataFile) {
        dataFile.setMenuId(gainMenuId);
        dataFile.setStatus("未审批");
        dataFile.setCreateTime(new Date());
        if(dataFileService.insertGain(dataFile)>0)
        {
            return ResponseData.success();
        }
        return ResponseData.error("上报失败！");

    }


    @ApiOperation(value = "采集数据上报")
    @PostMapping("/import/collection")
    //@LoginSkipped
    public ResponseData DataCollection(@RequestBody DataPlpCollect data) {
        if(data.getEventType()==null
                && verifyEnventType(data)) {
                return ResponseData.error("无法识别的时间类型");
        }
        if(data.getType() ==null || !verifyDataType(data)){
            return ResponseData.error("数据类型不在已知范围内");
        }
        data.setSubmitor(SecurityUserUtil.getUser().getUsername());
        String msg="操作成功!";
        String filePath = upLoadPath + File.separator+"collect"+File.separator+SecurityUserUtil.getUser().getId()
                +File.separator+System.currentTimeMillis() ;
        if(null != data.getPhoto()){
            String[] base64Data = data.getPhoto();

            int errFiles=0;
            for(int i =0;i<base64Data.length;i++){
                String [] d = base64Data[i].split("base64,");
                try {
                    errFiles = saveBase64File(filePath, errFiles, i, d,"photo",DataPlp.class.getDeclaredMethod("setPhotoByte" ,byte[].class),data);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
            if(errFiles == base64Data.length)
                return ResponseData.error("文件数据有误");
            else if(errFiles > 0)
                msg += errFiles +" 个图片文件出错！";
            data.setFileName(filePath);
        }
        if(null != data.getScreenshot()){
            String[] base64Data = data.getScreenshot();
            int errFiles=0;
            for(int i =0;i<base64Data.length;i++){
                String [] d = base64Data[i].split("base64,");
                try {
                    errFiles = saveBase64File(filePath, errFiles, i, d,"screenshot",DataPlp.class.getDeclaredMethod("setShot", byte[].class),data);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
            if(errFiles == base64Data.length)
                return ResponseData.error("文件数据有误");
            else if(errFiles > 0)
                msg += errFiles +" 个截图文件出错！";
            data.setFileName(filePath);
        }
        data.setStatus("未审批");
        if(dataPlpService.insertOne(data)>0)
        {
            return ResponseData.success(msg);
        }
        return ResponseData.error("上报失败！");

    }

    @Nullable
    private boolean verifyDataType(@RequestBody DataPlpCollect data) {
        switch ( data.getType()){
            case "120400":case "120500":case "120600":case "120700":
            case "130200":case "140200":case "260100":case "270100":
            case "140502":case "270201":case "140508":case "140510":
            case "140513":  /// poionts
            case "120800":case "140100":case "140300":case "140516":
            case "140523":case "160200":// lines
            case "160400":case "161103":case "220100":case "220200"://ployons
                return true;
            default:return false;
        }
    }

    private int saveBase64File(String filePath, int errFiles, int i, String[] d, String inffix, Method method,DataPlpCollect obj) {
        String dataPrix;
        String datab;
        if(d != null && d.length == 2){
            dataPrix = d[0];
            datab = d[1];
        }else{
            errFiles++;
            return errFiles;
           // return ResponseData.error("数据不合法");
        }

        String suffix = "";
        if("data:image/jpeg;".equalsIgnoreCase(dataPrix)){//data:image/jpeg;base64,base64编码的jpeg图片数据
            suffix = ".jpg";
        } else if("data:image/x-icon;".equalsIgnoreCase(dataPrix)){//data:image/x-icon;base64,base64编码的icon图片数据
            suffix = ".ico";
        } else if("data:image/gif;".equalsIgnoreCase(dataPrix)){//data:image/gif;base64,base64编码的gif图片数据
            suffix = ".gif";
        } else if("data:image/png;".equalsIgnoreCase(dataPrix)){//data:image/png;base64,base64编码的png图片数据
            suffix = ".png";
        } else if("data:file/zip;".equalsIgnoreCase(dataPrix)){//data:image/png;base64,base64编码的png图片数据
            suffix = ".zip";
        }else{
            errFiles++;
            return errFiles;
            //return ResponseData.error("上传图片格式不合法");
        }
        //获取解码器
        Base64.Decoder decoder =  Base64.getDecoder();
        byte[] bytes =  decoder.decode(datab.getBytes());  //将base64转换成字节流
        try{
            //使用apache提供的工具类操作流
            if(method !=null && obj !=null)
                method.invoke(obj,bytes);
            if(StringUtils.isNotBlank(filePath)){
                String tempFileName = filePath + File.separator+inffix+"_" +i+ suffix;
                FileUtils.writeByteArrayToFile(new File(tempFileName), bytes);
            }
        }catch(Exception ee){
            errFiles++;
            //return ResponseData.error("上传失败，写入文件失败，"+ee.getMessage());
        }
        return errFiles;
    }

    private boolean verifyEnventType( DataPlpCollect data) {
        String p=data.getPoints();
        if(null == p || StringUtils.isBlank(p)){
            return true;
        }
        p= p.toUpperCase().trim();
        if(p.startsWith("POINT") || p.equals("8"))
        {
            data.setEventType("8");
        }
        if(p.startsWith("LINESTRING")|| p.equals("64"))
        {
            data.setEventType("64");;
        }
        if(p.startsWith("POLYGON")|| p.equals("128"))
        {
            data.setEventType("128");
        }
        return false;
    }

    @ApiOperation(value = "离线数据上报")
    @PostMapping("/import/offlinedata")
    public ResponseData offlineData(@RequestBody DataFile data) {
        if(null != data){
            if(null == data.getDirectoryName()
                    || null == data.getName()
                    || null == data.getVersionDescription()){
                return ResponseData.error("文件路径、城市名、版本描述信息为必填项！");
            }
            DataFile df =new DataFile();
            df.setStatus("未审批");
            df.setMenuId(offLineDataMenuId);
            if(null != SecurityUserUtil.getUser())
                df.setSubmitor(SecurityUserUtil.getUser().getUsername());
            df.setFilePath(data.getDirectoryName());
            df.setFileDiscription(data.getVersionDescription());
            df.setRegion(data.getName());
            dataFileService.insertDataFile(df);
            return ResponseData.success("离线数据上报完成");
        }
        return ResponseData.error("无效的请求数据");

    }

}
