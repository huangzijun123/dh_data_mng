/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.controller.rest;

import com.cetc32.dh.common.response.PageDataResult;
import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.entity.DataFile;
import com.cetc32.dh.entity.DataPlp;
import com.cetc32.dh.service.DataPlpService;
import com.cetc32.webutil.common.util.SecurityUserUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 数据管理目录点线面数据操作类
 *
 * @author: xiao
 * @version: 1.0
 * @date: 2020/10/14
 * 备注:无
 */
@RestController
@RequestMapping("/rest/plp/")
public class DataPlpController {

    @Autowired
    DataPlpService dataPlpService;

    /**
     * 提交点线面数据
     *
     * @param dataPlp 点线面数据实体类
     * @return 返回提交结果
     */
    @ApiOperation(value = "提交点线面数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileType", value = "数据类型", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "security", value = "数据安全等级", paramType = "body", dataType = "Integer", defaultValue = ""),
            @ApiImplicitParam(name = "region", value = "区域", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "fileTime", value = "文件年份", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "description", value = "数据描述", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "approver", value = "审批人", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "points", value = "点线面点集", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "photoByte", value = "照片流", paramType = "body", dataType = "byte", defaultValue = ""),
            @ApiImplicitParam(name = "fileConfig", value = "文件标识别，数据标识", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "用户id", paramType = "body", dataType = "Integer"),
    })
    @PostMapping("/subplp")
    public ResponseResult DataSub(@RequestBody DataPlp dataPlp) {
        dataPlp.setSubmitor(SecurityUserUtil.getUser().getUsername());
        dataPlp.setStatus("未审批");
        dataPlp.setCreateTime(new Date());
        if (dataPlpService.insertOne(dataPlp) > 0) {
            return ResponseResult.success("表单信息提交成功！");
        }
        return ResponseResult.error("表单信息提交失败！");
    }


  /**
   * 记录数据详情展示
   *
   * @param id   文件id
   * @return 返回记录数据详情结果
   */
  @ApiOperation(value = "查看记录详情")
  @RequestMapping(value = "/plpdetail", method = RequestMethod.GET)
  public ResponseResult plpDetail(Integer id) {
    DataPlp dataPlp = dataPlpService.queryById(id);
    if(dataPlp!=null){
      return ResponseResult.success(200,"详情",dataPlp);
    }else{
      return  ResponseResult.error("未查找到相关信息，请输入正确的参数！");
    }
  }


    /**
     * 根据编目节点id查询通过的点线面数据
     *
     * @param map
     * @return 返回查询结果
     * 备注:无
     */
    @ApiOperation(value = "查询已通过的点线面数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "menuId", value = "编目节点", paramType = "body", dataType = "Integer", required = true, defaultValue = ""),
    })
    @PostMapping("/plpqueryAccepted")
    public PageDataResult plpQueryAccepted(@RequestBody Map<String, Object> map) {
        DataPlp dataPlp = new DataPlp();
        Integer menuId = (Integer) map.get("menuId");
        dataPlp.setMenuId(menuId);
        dataPlp.setStatus("审批通过");
        Integer page = dataPlp.getPage();
        Integer results = dataPlp.getResults();
        if (null == page || page <= 0)
            page = 1;
        if (null == results || results <= 0)
            results = 10;

        int offset = (page - 1) * results;

        return new PageDataResult(dataPlpService.countFilesByObj(dataPlp),
                dataPlpService.queryFilesByObj(offset, results, dataPlp),
                offset);
    }

    /**
     * 根据输入条件查询点线面数据
     *
     * @param dataPlp
     * @return 返回查询结果
     * 备注:无
     */
    @ApiOperation(value = "显示满足输入条件的点线面数据", notes = "至少传入page，和current两个参数以及menuId编目号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "body", dataType = "Integer", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "results", value = "每页数据条数", paramType = "body", dataType = "Integer", required = true, defaultValue = "10"),
            @ApiImplicitParam(name = "fileName", value = "文件名称", paramType = "body", defaultValue = ""),
            @ApiImplicitParam(name = "fileYear", value = "文件年份", paramType = "body", dataType = "Integer", defaultValue = ""),
            @ApiImplicitParam(name = "fileType", value = "文件类型", paramType = "body", defaultValue = ""),
            @ApiImplicitParam(name = "fileSecurity", value = "文件安全等级", paramType = "body", dataType = "Integer", defaultValue = ""),
            @ApiImplicitParam(name = "region", value = "文件区域", paramType = "body", defaultValue = ""),
            @ApiImplicitParam(name = "menuId", value = "目录节点", paramType = "body", dataType = "Integer", required = true, defaultValue = ""),
            @ApiImplicitParam(name = "status", value = "审批状态", paramType = "body", dataType = "String", defaultValue = "")
    })

    @PostMapping("/approvesplp")
    public PageDataResult myAprroves(@RequestBody DataPlp dataPlp) {
        Integer page = dataPlp.getPage();
        Integer results = dataPlp.getResults();
        if (dataPlp.getTimeRange() != null && dataPlp.getTimeRange().length == 2) {
            dataPlp.setStartTime(dataPlp.getTimeRange()[0]);
            dataPlp.setEndTime(dataPlp.getTimeRange()[1]);
        }
        if (null == page || page <= 0)
            page = 1;
        if (null == results || results <= 0)
            results = 10;

        int offset = (page - 1) * results;

      List<DataPlp> dataPlpsList = new ArrayList<DataPlp>();
      dataPlpsList = dataPlpService.queryFilesByObj(offset, results, dataPlp);
      return new PageDataResult(dataPlpService.countFilesByObj(dataPlp), dataPlpsList, offset);

//        return new PageDataResult(dataPlpService.countFilesByObj(dataPlp),
//                dataPlpService.queryFilesByObj(offset, results, dataPlp),
//                offset);
    }

    /**
     * 根据编目节点id查询审批通过的点线面数据
     *
     * @param map
     * @return 返回查询结果
     **/
    @ApiOperation(value = "审批通过的点线面数据")
    @PostMapping("/acceptplp")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "申请审批的记录id", dataType = "List", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "Integer", defaultValue = ""),
    })
    public ResponseResult acceptSubmit(@RequestBody Map<String, Object> map) {
        System.out.println(map);
        List<Integer> ids = new ArrayList<>();
        DataPlp dataPlp = new DataPlp();
        int sum = 0;
        ids = (List<Integer>) map.get("ids");
        String userName =  SecurityUserUtil.getUser().getUsername();
        for (int i = 0; i < ids.size(); i++) {
            Integer id = ids.get(i);
            dataPlp = dataPlpService.queryById(id);
            dataPlp.setStatus("审批通过");
            dataPlp.setApprover(userName);
            dataPlp.setApproveTime(new Date());
            if (dataPlpService.updateById(dataPlp) > 0) {
                sum++;
            }
        }
        if (sum == ids.size())
            return ResponseResult.success("已审批通过");
        return ResponseResult.error("审批失败");

    }


    /**
     * 审批拒绝导入的点线面数据
     *
     * @param map
     * @return 返回执行结果
     **/
    @ApiOperation(value = "审批拒绝点线面数据")
    @PostMapping("/rejectplp")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "文件夹下的文件list", paramType = "body", dataType = "List", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "body", dataType = "Integer"),
    })
    public ResponseResult rejectSubmit(@RequestBody Map<String, Object> map) {
        System.out.println(map);
        List<Integer> ids = new ArrayList<>();
        DataPlp dataPlp = new DataPlp();
        int sum = 0;
        ids = (List<Integer>) map.get("ids");
        String userName =  SecurityUserUtil.getUser().getUsername();
        for (int i = 0; i < ids.size(); i++) {
            Integer id = ids.get(i);
            dataPlp = dataPlpService.queryById(id);
            dataPlp.setStatus("审批拒绝");
            dataPlp.setApprover(userName);
            dataPlp.setApproveTime(new Date());
            if (dataPlpService.updateById(dataPlp) > 0) {
                sum++;
            }
        }
        if (sum == ids.size())
            return ResponseResult.success("已审批拒绝");
        return ResponseResult.error("审批失败");
    }


    /**
     * 删除的点线面数据
     *
     * @param map
     * @return 返回执行结果
     **/
    @ApiOperation(value = "删除点线面数据")
    @PostMapping("/delplp")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "申请删除的文件id", dataType = "List", defaultValue = ""),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "Integer", defaultValue = ""),
    })
    public ResponseResult delPlpData(@RequestBody Map<String, Object> map) {
        System.out.println(map);
        List<Integer> ids = new ArrayList<>();
        int sum = 0;
        ids = (List<Integer>) map.get("ids");
        Integer userId =  SecurityUserUtil.getUser().getId();
        String userName = SecurityUserUtil.getUser().getUsername();
        for (int i = 0; i < ids.size(); i++) {
            Integer id = ids.get(i);
            if (dataPlpService.deleteById(id) > 0) {
                sum++;
            }
        }
        if (sum == ids.size()) {
            return ResponseResult.success("删除成功");
        }
        return ResponseResult.error("删除失败");
    }


}
