package com.cetc32.dh.controller.rest;

import com.alibaba.fastjson.JSONObject;
import com.cetc32.dh.common.response.PageDataResult;
import com.cetc32.dh.common.response.ResponseData;
import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.dto.EstimateDTO;
import com.cetc32.dh.entity.EstimateTask;
import com.cetc32.dh.entity.vEstimate;
import com.cetc32.dh.service.EstimateTaskService;
import com.cetc32.dh.utils.FileUtil;
import com.cetc32.webutil.common.util.SecurityUserUtil;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.sf.json.JSONArray;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/estimatetask/")
public class EstimateTaskController {
    @Autowired
    EstimateTaskService estimateTaskService;

    @Value("${myPath}")
    String myPath;


    /**
     *展示所有评估任务
     * @return PageDateResult
     */
    @ApiOperation(value = "展示所有评估任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name="classify", value="评估任务分类",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="name",value="评估任务名称",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="astatus",value="审批状态",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="page",value = "页码",dataType = "Integer",defaultValue = "0"),
            @ApiImplicitParam(name="result",value = "每页数据条数",dataType = "Integer",defaultValue = "10"),
    })
    @PostMapping("/options")
    public PageDataResult organizationAll(@RequestBody vEstimate vEstimate,@RequestParam(defaultValue = "0",required = false) Integer page, @RequestParam(defaultValue = "10",required = false) Integer results){
      if(vEstimate.getName()!=null&&vEstimate.getName().contains("%")){
        return new PageDataResult(0,new ArrayList<>(),page * results);
      }
        if((StringUtils.isBlank(vEstimate.classify)||vEstimate.classify.equals("全部分类"))&&StringUtils.isBlank(vEstimate.name)&&StringUtils.isBlank(vEstimate.astatus)&&StringUtils.isBlank(vEstimate.creator)){
            List<EstimateTask> estimateTasks = estimateTaskService.findAll();
            Integer count = estimateTaskService.countEstimate();
            return new PageDataResult(count.intValue(),estimateTasks,page * results);
        }else if(StringUtils.isNotBlank(vEstimate.name)&&(vEstimate.name.contains("%"))){
            List<EstimateTask> demandlist = new ArrayList<>();
            return new PageDataResult(0,demandlist,0);
        }
        else {
            if(StringUtils.isNotBlank(vEstimate.getName()))
                vEstimate.setName("%"+vEstimate.getName()+"%");
            if(vEstimate.classify.equals("全部分类")){
                vEstimate.classify=null;
            }
            List<EstimateTask> estimateTasks = estimateTaskService.queryFilesByObj(vEstimate);
            Integer count = estimateTaskService.queryFilesByObj(vEstimate).size();
            return new PageDataResult(count.intValue(),estimateTasks,page * results);
        }
    }

    /**
     * 新增评估任务
     * @param estimateDTO
     * @return ResponseResult
     */
    @ApiOperation(value="新增评估任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value = "记录id号(不传参)", paramType = "body", dataType = "Integer", required = false),
            @ApiImplicitParam(name="name",value = "任务名称",paramType = "body",dataType = "String" ,defaultValue = ""),
            @ApiImplicitParam(name="taskClassify",value = "评估任务类型",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="taskType",value = "评估任务服务选择类型",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="taskPath",value = "评估任务附件路径",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="starttimefront",value = "前端传过来的任务开始时间",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="endtimefront",value = "前端传过来的任务结束时间",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="creator",value="任务创建人",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="creattimefront",value = "前端传来的创建时间",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="status",value = "状态",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="approver",value = "审批人",paramType = "body",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="demandId",value = "对应的需求id",paramType = "body",dataType = "Integer",defaultValue = ""),
            @ApiImplicitParam(name = "frontyear",value = "前端传来的时间数组",paramType = "body" ,dataType = "List",defaultValue = ""),
            @ApiImplicitParam(name = "approvtime",value = "时间",paramType = "body" ,dataType = "Timestamp",defaultValue = ""),
            @ApiImplicitParam(name="useid",value = "对应的用户id",paramType = "body",dataType = "Integer",defaultValue = ""),
    })
    @PostMapping("/insertselect")
    public ResponseResult insertselect(@RequestBody EstimateDTO estimateDTO){
        EstimateTask estimateTask = new EstimateTask();
        BeanUtils.copyProperties(estimateDTO,estimateTask);
        Integer userid=  SecurityUserUtil.getUser().getId();
        if(StringUtils.isBlank(estimateTask.getName())||StringUtils.isBlank(estimateTask.getTaskClassify())){
            return ResponseResult.error("传入空值！");
        }
        List<String> handletime = estimateTask.getFrontyear();
        String starttime1 = handletime.get(0);
        String endtime1 = handletime.get(1);
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        Date startdate = new Date();
        Date enddate = new Date();
        Date createtime=null;
        try{
            startdate = format1.parse(starttime1);
            enddate = format1.parse(endtime1);
        }catch (ParseException e) {
            e.printStackTrace();
        }

        estimateTask.setStarttime(startdate);
        estimateTask.setEndtime(enddate);
        String upCachePath=myPath+"_user"+userid;
        //BaseAdminUser adminUser=getCurrentUserId();
        String userName =SecurityUserUtil.getUser().getUsername();
        estimateTask.setCreator(userName);
        String status="未审批";
        Date d= new Date();
        String timestamp = String.valueOf(d.getTime());
        String path = myPath+"admin"+timestamp;
        estimateTask.setStatus(status);
        Date creat = new Date();
        estimateTask.setCreattime(creat);
        if(!estimateTask.getTaskClassify().equals("服务")){
            if(FileUtil.copyFolder(upCachePath, path)){
                FileUtil.deleteDir(upCachePath);
            }else {
                return ResponseResult.error("upload fail");
            }
            estimateTask.setTaskPath(path);
        }
        if(estimateTaskService.insertSelective(estimateTask)>0){
            return new ResponseResult("创建成功！");
        }
        return ResponseResult.error("wrong");
    }

    @PostMapping("/selectbylimit")
    public PageDataResult selectbylimit(@RequestParam("pageSize") Integer pagesize, @RequestParam("pageNum") Integer pagenum){
        PageDataResult pdr = new PageDataResult();
        if(pagenum == null || pagenum == 0){
            pagenum =1;
        }
        if (pagesize == null || pagesize == 0)
            pagesize = 10;
        pdr.setList(estimateTaskService.selectByLimit((pagenum - 1) * pagesize, pagesize));
        pdr.setTotals(estimateTaskService.countEstimate());
        return pdr;
    }

    @PostMapping("/selectbykey")
    public ResponseResult selsectbykey(Integer id){
        if(id == null){
            return ResponseResult.error("id为空！");
        }
        return ResponseResult.success(this.estimateTaskService.selectByPrimaryKey(id));
    }

    /***
     * @Author xuwenyuan
     * @Date 2021.9.7 added
     * @Description 获取当前评估任务的反馈报告
     * @param id  评估任务ID
     * @return ResponseData结构的对应信息
     * **/
    @GetMapping("/report")
    public ResponseData checkCurrentReprot(Integer id){
        if(null == id || id<=0)
            return ResponseData.error("无效的任务ID");
        try{
            return ResponseData.success(JSONObject.parseObject(estimateTaskService.selectByPrimaryKey(id).getReport()));
        }catch ( NullPointerException e){
            return ResponseData.error("指定ID： " +id +" 不存在对应评估任务");
        }catch ( Exception e){
            e.printStackTrace();
        }
        return ResponseData.error();
    }

    @PostMapping("/deletebykey")
    public ResponseResult deletebyprimarykey(Integer id){
        if(id == null)
            return ResponseResult.error("传入值为空！");
        estimateTaskService.deleteByPrimaryKey(id);
        return new ResponseResult("Success");
    }

    @PostMapping("/updateselect")
    public ResponseResult updateselect(EstimateTask record){
        if(record == null){
            return ResponseResult.error("传入值为空！");
        }
        estimateTaskService.updateByPrimaryKeySelective(record);
        return new ResponseResult("Success");
    }

    @PostMapping("/search")
    public PageDataResult CircuitSearch(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize, String keyword) {

        PageDataResult pdr = new PageDataResult();
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0)
            pageSize = 10;

        pdr.setTotals(estimateTaskService.findByKeyWord(keyword).size());
        PageHelper.startPage(pageNum, pageSize);
        pdr.setList(estimateTaskService.findByKeyWord(keyword));
        return pdr;
    }

    @PostMapping("/gettask")
    public PageDataResult getUserTask(){
        //BaseAdminUser adminUser = adminUserService.queryById(SecurityUserUtil.getUser().getId());
        String name = SecurityUserUtil.getUser().getUsername();
        PageDataResult pdr = new PageDataResult();
        pdr.setTotals(estimateTaskService.countMineSubmit(name));
        pdr.setList(estimateTaskService.selectMySubmit(name));
        return pdr;
    }

    @PostMapping("/accepttask")
    public ResponseResult acceptDemand(Integer id,String status){
        EstimateTask estimateTask = estimateTaskService.selectByPrimaryKey(id);
        estimateTask.setStatus(status);
        estimateTaskService.updateByPrimaryKeySelective(estimateTask);
        return new ResponseResult("done");
    }


    @PostMapping("/rejectproduct")
    public ResponseResult rejectDemand(Integer id,String status){
        EstimateTask estimateTask = estimateTaskService.selectByPrimaryKey(id);
        estimateTask.setStatus(status);
        estimateTaskService.updateByPrimaryKeySelective(estimateTask);
        return new ResponseResult("done");
    }

    /**
     * 目录/文件上传
     * @param file 目录，文件
     * @return 返回提交结果
     * */
    @ApiOperation(value = "文件上传")
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "文件夹下的文件list", paramType = "body", dataType = "List", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "用户id", paramType = "body", dataType = "Integer"),
    })
    public ResponseResult uploadFile(@ApiParam(value = "二进制文件流")  MultipartFile[] file) {
//        String filepath = "/root/upLoad";
        String upCachePath=myPath+"_user"+ SecurityUserUtil.getUser().getId();
        String filepath = upCachePath;
        String result = FileUtil.uploadFile(file, filepath);
        if (!result.contains("上传失败"))
            return ResponseResult.success(result);
        return ResponseResult.error(result);

    }


    /**
     * 文件上传缓冲区清空
     * @return 返回文件清空结果
     * */
    @ApiOperation(value = "清空历史上传")
    @RequestMapping(value = "/clearUploadFile", method = RequestMethod.POST)
    @ApiImplicitParam(name = "userId", value = "当前用户id")
    public ResponseResult clearUploadFile(String userId) {
        String upCachePath=myPath+"_user"+userId;
        FileUtil.deleteDir(upCachePath);
        File file = new File(upCachePath);
        if (!file.exists()) {
            return ResponseResult.success("历史上传清空成功");
        }
        return ResponseResult.error("历史上传清空失败");
    }

    /**
     * 多个任务通过
     * @return 返回执行结果
     * **/
    @ApiOperation(value = "批量审批通过")
    @PostMapping("/accept")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "申请的记录id", dataType = "Map", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "当前操作的用户id", dataType = "Map", defaultValue = "")
    })
    public ResponseResult acceptMany(@RequestBody Map<String, Object> map){
        List<Integer> ids = new ArrayList<>();
        ids = (List<Integer>) map.get("ids");
        EstimateTask estimateTask = new EstimateTask();
        String staus = "审批通过";
        String userName = SecurityUserUtil.getUser().getUsername();
        int sum = 0;
        for (int i = 0; i < ids.size(); i++){
            Integer id = ids.get(i);
            estimateTask = estimateTaskService.selectByPrimaryKey(id);
            estimateTask.setStatus(staus);
            Date apptime = new Date();
            estimateTask.setApprover(userName);
            estimateTask.setApprovtime(apptime);
            if(estimateTaskService.updateByPrimaryKeySelective(estimateTask)>0){
                sum = sum + 1;
            }
        }
        if (sum == ids.size())
            return ResponseResult.success("已通过");
        return ResponseResult.error("失败");
    }

    /**
     * 多个任务拒绝
     * @return 返回执行结果
     * **/
    @ApiOperation(value = "批量审批拒绝")
    @PostMapping("/refuse")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "申请的记录id", dataType = "Map", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "当前操作的用户id", dataType = "Map", defaultValue = "")
    })
    public ResponseResult rejectMany(@RequestBody Map<String, Object> map){
        List<Integer> ids = new ArrayList<>();
        ids = (List<Integer>) map.get("ids");
        EstimateTask estimateTask = new EstimateTask();
        String staus = "审批拒绝";
        String userName = SecurityUserUtil.getUser().getUsername();
        int sum = 0;
        for (int i = 0; i < ids.size(); i++){
            Integer id = ids.get(i);
            estimateTask = estimateTaskService.selectByPrimaryKey(id);
            estimateTask.setStatus(staus);
            Date apptime = new Date();
            estimateTask.setApprover(userName);
            estimateTask.setApprovtime(apptime);
            if(estimateTaskService.updateByPrimaryKeySelective(estimateTask)>0){
                sum = sum + 1;
            }
        }
        if (sum == ids.size())
            return ResponseResult.success("已拒绝！");
        return ResponseResult.error("失败");
    }

    /**
     * 下载功能
     * @return  下载后的文件名
     * **/
    @ApiOperation(value = "下载")
    @GetMapping("/download")
    @ApiImplicitParam(name = "id", value = "需求的记录id", dataType = "Integer", defaultValue = "")
    public String download(Integer id, HttpServletRequest request, HttpServletResponse response) throws Exception{
        EstimateTask estimateTask = estimateTaskService.selectByPrimaryKey(id);
        String path = estimateTask.getTaskPath();
        String zipPath =  FileUtil.downloadAllAttachment(path,request,response);
        String filename = zipPath.substring(zipPath.lastIndexOf(File.separator)+1,zipPath.length());
        FileUtil.downloadFile(response,filename,zipPath);
        FileUtil.deleteDir(zipPath);
        return filename;
    }

    /*
     *获取所有的服务名
     * @return List<String>
     */
    @ApiOperation(value="获取所有的服务名")
    @PostMapping("/getservice")
    public List<String> getAllService(){
        String classify = "服务";
        List<String> getnames = estimateTaskService.allservice(classify);
        return getnames;
    }

    @ApiOperation(value = "获取所有的分类")
    @PostMapping("/getclassify")
    public List<String> getAllclassify(){
        List<String> results = new ArrayList<>();
        results.add("全部分类");
        List<String> getclassify = estimateTaskService.alltaskclassify();
        for (int i = 0;i<getclassify.size();i++){
            results.add(getclassify.get(i));
        }
        return results;
    }


    @ApiOperation(value = "获取服务的url")
    @PostMapping("/geturl")
    public ResponseResult getJsonUrl(){
//        String fileName = "src/main/resources/config/ServiceAll.json";
//        String jsonStr = "";
//        try {
//            File jsonFile = new File(fileName);
//            if(jsonFile==null){
//                return ResponseResult.error("文件为空！");
//            }
//            FileReader fileReader = new FileReader(jsonFile);
//
//            Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
//            int ch = 0;
//            StringBuffer sb = new StringBuffer();
//            while ((ch = reader.read()) != -1) {
//                sb.append((char) ch);
//            }
//            fileReader.close();
//            reader.close();
//            jsonStr = sb.toString();
//            List<HashMap> listMap = JSONObject.parseArray(jsonStr, HashMap.class);
//
//            return ResponseResult.success(listMap);
//        } catch (IOException e) {
//            e.printStackTrace();
//            return ResponseResult.error("获取失败");
//        }
        String dir ="src/main/resources/config/ServiceAll.json" ;

        try {
            File file = new File(dir);
            if (!file.exists()) {
                file.createNewFile();
            }
            String str= FileUtils.readFileToString(file, "UTF-8");
            List<Map> maps= (List) JSONArray.fromObject(str);
            return ResponseResult.success(maps);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseResult.error("解析失败！");
        }
    }
}
