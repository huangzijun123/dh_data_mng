/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.controller.rest;

import com.cetc32.dh.common.response.PageDataResult;
import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.entity.*;
import com.cetc32.dh.service.DemandSubmitService;
import com.cetc32.dh.service.EstimateTaskService;
import com.cetc32.dh.service.ProductdemandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 任务管理接口
 *
 * @author: xiao
 * @version: 1.0
 * @date: 2020/10/14
 * 备注:无
 */
@Slf4j
@RestController
@RequestMapping("/rest/datamanage/demandtask")
public class DemandTaskCommonController {
    @Autowired
    DemandSubmitService demandSubmitService;
    @Autowired
    EstimateTaskService estimateTaskService;
    @Autowired
    ProductdemandService productdemandService;

    /**
     * 统计今日提交的生产需求个数
     *
     * @return 返回查询个数结果
     * 备注:无
     */
    @PostMapping(value = "/todaydemandsub")
    public ResponseResult countTodayDemand() {
        vDemand vdemand = new vDemand();
        int count;
        LocalDateTime today_start = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);//当天零点
        String td_start = today_start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime today_end = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);//当天最晚点
        String td_end = today_end.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        vdemand.setTd_start(td_start);
        vdemand.setTd_end(td_end);
        count = demandSubmitService.queryFilesByObj(vdemand).size();
        return ResponseResult.success(count);
    }

    /**
     * 统计今日下发/审批/截止任务个数
     * @param map message 区分的任务类型的信息
     * @return 返回查询个数结果
     * 备注:无
     */
    @PostMapping(value = "/todaytask")
    public ResponseResult countTodayTask(@RequestBody Map<String, Object> map) {
        String message = (String) map.get("message");
        if (message == null || !(message.equals("下发任务") || message.equals("审批任务") || message.equals("截止任务"))) {
            return ResponseResult.error("请输入正确的message!");
        }
        vProduct vproduct = new vProduct();
        vEstimate vestimate = new vEstimate();
        int count;
        LocalDateTime today_start = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);//当天零点
        String td_start = today_start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime today_end = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);//当天最晚点
        String td_end = today_end.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        vproduct.setTd_start(td_start);
        vproduct.setTd_end(td_end);
        vestimate.setTd_start(td_start);
        vestimate.setTd_end(td_end);
        if (message != null) {
            vproduct.setMessage(message);
            vestimate.setMessage(message);
        }
        count = productdemandService.queryFilesByObj(vproduct).size();
        count += estimateTaskService.queryFilesByObj(vestimate).size();
        return ResponseResult.success(count);
    }

    /**
     * 评估任务数和生产任务数之比
     *
     * @return 返回结果
     * 备注:无
     */
    @PostMapping(value = "/demandpercent")
    public ResponseResult demandPercent() {
        ArrayList<Object> list = new ArrayList<Object>();
        vDemand vdemand_product = new vDemand();
        vDemand vdemand_estimate = new vDemand();
        int vProduct_num;
        int vEstimate_num;
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        HashMap<String, Object> hashMap1 = new HashMap<String, Object>();
        vdemand_product.setClassify("生产任务");
        vdemand_estimate.setClassify("评估任务");
        vProduct_num = demandSubmitService.queryFilesByObj(vdemand_product).size();
        vEstimate_num = demandSubmitService.queryFilesByObj(vdemand_estimate).size();
        hashMap.put("name", "生产需求");
        hashMap.put("value", vProduct_num);
        list.add(hashMap);
        hashMap1.put("name", "评估需求");
        hashMap1.put("value", vEstimate_num);
        list.add(hashMap1);
        return ResponseResult.success(list);
    }

    /**
     * 评估需求数和生产需求数之比
     *
     * @return 返回结果
     * 备注:无
     */
    @PostMapping(value = "/taskpercent")
    public ResponseResult taskPercent() {
        ArrayList<Object> list = new ArrayList<Object>();
        vProduct vproduct = new vProduct();
        vEstimate vestimate = new vEstimate();
        int vProduct_num;
        int vEstimate_num;
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        HashMap<String, Object> hashMap1 = new HashMap<String, Object>();
        vProduct_num = productdemandService.queryFilesByObj(vproduct).size();
        vEstimate_num = estimateTaskService.queryFilesByObj(vestimate).size();
        hashMap.put("name", "生产任务");
        hashMap.put("value", vProduct_num);
        list.add(hashMap);
        hashMap1.put("name", "评估任务");
        hashMap1.put("value", vEstimate_num);
        list.add(hashMap1);
        return ResponseResult.success(list);
    }

    /**
     * 近七日每日任务统计数
     *
     * @return 返回结果
     * 备注:无
     */
    @PostMapping(value = "/sevenDayTotal")
    public ResponseResult sevenDayTotal() {
        ArrayList<Object> list_day = new ArrayList<Object>();
        ArrayList<Object> list_count = new ArrayList<Object>();
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        String message = "下发任务";
        vProduct vproduct = new vProduct();
        vEstimate vestimate = new vEstimate();
        vproduct.setMessage(message);
        vestimate.setMessage(message);
        for (int i = 6; i >= 0; i--) {
            int count = 0;
            LocalDateTime today_start = LocalDateTime.of(LocalDate.now().minusDays(i), LocalTime.MIN);//当天零点
            String td_start = today_start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            LocalDateTime today_end = LocalDateTime.of(LocalDate.now().minusDays(i), LocalTime.MAX);//当天最晚点
            String td_end = today_end.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            String td = today_start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            vproduct.setTd_start(td_start);
            vproduct.setTd_end(td_end);
            vestimate.setTd_start(td_start);
            vestimate.setTd_end(td_end);
            count = productdemandService.queryFilesByObj(vproduct).size();
            count += estimateTaskService.queryFilesByObj(vestimate).size();
            list_day.add(td);
            list_count.add(count);
        }
        hashMap.put("day", list_day);
        hashMap.put("data", list_count);
        return ResponseResult.success(hashMap);
    }


    /**
     * 未审批任务、需求获取接口
     * @param map message 区分不同需求是任务接口的信息
     * @return 返回结果
     * 备注:无
     */
    @PostMapping(value = "/unaprrove")
    public PageDataResult unaprrove(@RequestBody Map<String, Object> map) {
        String message = (String) map.get("message");
        ArrayList<Object> list = new ArrayList<Object>();
        vDemand vdemand = new vDemand();
        vProduct vproduct = new vProduct();
        vEstimate vestimate = new vEstimate();
        vdemand.setStatus("未审批");
        vproduct.setStatus("未审批");
        vestimate.setStatus("未审批");
        if (message != null && message.equals("未审批需求")) {
            List<DemandSubmit> vDemandList = demandSubmitService.queryFilesByObj(vdemand);
            return new PageDataResult(vDemandList, vDemandList.size(), 200);
        } else if (message != null && message.equals("未审批任务")) {
            List<Productdemand> vProductsList = productdemandService.queryFilesByObj(vproduct);
            for (Productdemand productdemand : vProductsList) {
                list.add(productdemand);
            }
            List<EstimateTask> vEstimateList = estimateTaskService.queryFilesByObj(vestimate);
            for (EstimateTask estimateTask : vEstimateList) {
                list.add(estimateTask);
            }
            return new PageDataResult(list, list.size(), 200);
        } else {
            return new PageDataResult(null, 0, -1);
        }
    }


}
