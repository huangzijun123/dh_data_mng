package com.cetc32.dh.controller.rest;

import com.cetc32.dh.common.response.ResponseData;
import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.entity.*;
import com.cetc32.dh.mybatis.ProvinceMapper;
import com.cetc32.dh.service.CityGeomService;
import com.cetc32.dh.service.DataFileService;
import com.cetc32.dh.service.ProvinceGeomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/rest/progeom/")
public class ProvinceGeomController {
  @Autowired
  ProvinceMapper provinceMapper;
  @Autowired
  ProvinceGeomService provinceGeomService;
  @Autowired
  CityGeomService cityGeomService;
  @Autowired
  DataFileService dataFileService;

  @PostMapping("/init")
  public ResponseData init() {
    List<Province> provinceList = provinceMapper.selectByDistinct();
    ProvinceGeom provinceGeom = new ProvinceGeom();
    int data = 0;
    for (Province province : provinceList) {
      provinceGeom.setProvince(province.getProvince());
      provinceGeom.setProvinceCode(province.getProvinceCode());
      provinceGeomService.insertOne(provinceGeom);
      data++;
    }
    return ResponseData.success(200, "success", data);
  }

  /*将点连接成面*/
  @PostMapping("/geom")
  public ResponseResult geom() {
    int count = 0;
    List<ProvinceGeom> provinceGeomList = provinceGeomService.selectAll();
    for (ProvinceGeom provinceGeom : provinceGeomList) {
      String result = "";
      String geom = "";
      List<String> stringList = new ArrayList<>();
      if (provinceMapper.selectByCode(provinceGeom.getProvinceCode()).size() > 0) {
        //搜集所有该城市数据
        List<Province> provinceList = provinceMapper.selectByCode(provinceGeom.getProvinceCode());
        Collections.sort(provinceList, new Comparator<Province>() {
          public int compare(Province p1, Province p2) {
            return Integer.parseInt(p1.getFid()) - Integer.parseInt(p2.getFid());
          }
        });
        String start_end = null;
        for (int i = 0; i < provinceList.size(); i++) {
          String s = provinceList.get(i).getPointX();
          s = s + " ";
          s = s + provinceList.get(i).getPointY();
          if (i == 0) {
            start_end = s;
          }
          stringList.add(s);
        }
        stringList.add(start_end);
        result = stringList.toString();
        //result ="(117.293890872000006 30.427812941999999, 117.293890872000006 30.427812941999999, 117.293890872000006 30.427812941999999, 117.293890872000006 30.427812941999999,118.293890872000006 33.427812941999999,110.293890872000006 36.427812941999999,117.293890872000006 30.427812941999999)";
        result = result.replace("[", "(");
        result = result.replace("]", ")");
        System.out.println(result);
        result = "POLYGON(" + result + ")";
        System.out.println(result);
//                geom = cityService.judge(result);
      }
      if (result != "") {
        provinceGeom.setGeom(result);
      }

      if (provinceGeomService.updateByName(provinceGeom) > 0) {
        count++;
      }
    }
    if (count > 0) {
      return ResponseResult.success("更新成功");
    } else {
      return ResponseResult.error("更新失败");
    }
  }


  /*
  判断输入的点或者面在哪些区域中包含，返回区域的名称
   */
  @GetMapping("/pointandpolygon")
  public ResponseResult pointAndPolygon(String shape) {
    Boolean jdg_point = false;
    Boolean jdg_polygon = false;
    shape = "POLYGON((98.5 46.5,98.7 46.5,98.7 46.7,98.5 46.7,98.5 46.5))";
    shape = "POINT(2 2)";
    List<String> nameList = new ArrayList<>();
    List<ProvinceGeom> provinceGeomList = provinceGeomService.selectAll();
    for (ProvinceGeom provinceGeom : provinceGeomList) {
      if (provinceGeom.getPolygonText() != null && shape.contains("POINT")) {
        jdg_point = cityGeomService.judgePointContain(shape, provinceGeom.getPolygonText());//不包含边界上的点
      }
      if (provinceGeom.getPolygonText() != null && shape.contains("POLYGON")) {
        jdg_polygon = cityGeomService.judgePolygonContain(provinceGeom.getPolygonText(), shape);
      }
      if (jdg_point || jdg_polygon) {
        nameList.add(provinceGeom.getProvince());
      }
    }
    return ResponseResult.success(nameList);
  }





  /**
   * 根据地区名称areaName查询polygon
   */
  @PostMapping(value = "/areaTopolygon" , produces = "application/json;charset=UTF-8")
  public ResponseResult areaToPolygon(@RequestBody Map<String, Object> map) {
    String area = (String) map.get("areaNames");
    List<String> areaNames =Arrays.asList(area.split(";"));
//    List<String> areaNames = new ArrayList<>();
//    areaNames = (List<String>) map.get("areaNames");
    List<CityGeom> data =cityGeomService.areaFindPolygon(areaNames);
    List<String> errors =new ArrayList<>();
    for(String c:areaNames){
      boolean contains =false;
      for(CityGeom s: data){
        if(s.getCity().contains(c)){
          contains=true;
          break;
        }
      }
      if(!contains){
        errors.add(c);
      }
    }
    if(0 != errors.size()){
      return ResponseResult.error(errors.toString() +" 对应数据信息找不到！");
    }
    return ResponseResult.success(data);
  }


  /**
   * 根据polygons查路径
   */
  @PostMapping("/menuAndPgon")
  public ResponseResult menuAndPgon(@RequestBody Map<String, Object> map) {
    Integer menuId = (Integer) map.get("menuId");
    List<String> polygons = new ArrayList<>();
    polygons = (List<String>) map.get("polygons");
    Set<DataFile> resultFileList = null;
    if (menuId == 34) {
      return ResponseResult.success(cityGeomService.menuAndPgonPlm(polygons));
    }
    resultFileList = cityGeomService.menuAndPgonCom(menuId,polygons);
    return ResponseResult.success(resultFileList);
  }

  /**
   * 根据areaNames查路径
   */
  @PostMapping("/menuAndArea")
  public ResponseResult menuAndArea(@RequestBody Map<String, Object> map) {
    Integer menuId = (Integer) map.get("menuId");
    Set<DataFile> resultFileList = null;
    String area = (String) map.get("areaNames");
    List<String> areaNames =Arrays.asList(area.split(";"));
    Map<String, String> areaPlg = cityGeomService.areaToPolygon(areaNames);
    if(areaPlg.containsKey(null))
    {
      return ResponseResult.error(areaPlg.get(null)+"对应数据信息找不到！");
    }
    Collection<String> valueCollection = areaPlg.values();
    List<String> polygons = new ArrayList<String>(valueCollection);

    if(menuId==34){
      return ResponseResult.success(cityGeomService.menuAndPgonPlm(polygons));
    }
    resultFileList = cityGeomService.menuAndPgonCom(menuId,polygons);
    return ResponseResult.success(resultFileList);
  }

}










