package com.cetc32.dh.controller.rest;

import com.cetc32.dh.common.response.PageDataResult;
import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.dto.EditJsonDTO;
import com.cetc32.dh.dto.ProductDTO;
import com.cetc32.dh.entity.*;
import com.cetc32.dh.service.ProductdemandService;
import com.cetc32.dh.utils.FileUtil;
import com.cetc32.webutil.common.util.SecurityUserUtil;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 生产任务管理类
 * @author: hubin
 * @version: 1.0
 * @date: 2020/11/30
 * 备注:无
 */
@RestController
@RequestMapping("/productdemand")
public class ProductDemandController {
    @Autowired
    ProductdemandService productdemandService;

    @Value("${myPath}")
    String myPath;

    @Value("${flowPath}")
    String flowPath;

    /**
     *展示所有评估任务
     * @return PageDateResult
     */
    @ApiOperation(value = "展示所有生产任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name="classify", value="生产任务分类",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="name",value="生产任务名称",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="astatus",value="审批状态",dataType = "String",defaultValue = ""),
            @ApiImplicitParam(name="page",value = "页码",dataType = "Integer",defaultValue = "0"),
            @ApiImplicitParam(name="result",value = "每页数据条数",dataType = "Integer",defaultValue = "10"),
    })
    @PostMapping("/options")
    public PageDataResult productsubmitAll(@RequestBody vProduct vProduct,@RequestParam(defaultValue = "0",required = false) Integer page, @RequestParam(defaultValue = "10",required = false) Integer results) {
      if(vProduct.getName()!=null&&vProduct.getName().contains("%")){
        return new PageDataResult(0,new ArrayList<>(),page * results);
      }
        if(StringUtils.isBlank(vProduct.classify)&&StringUtils.isBlank(vProduct.name)&&StringUtils.isBlank(vProduct.astatus)&&StringUtils.isBlank(vProduct.creator)){
            List<Productdemand> productdemands = productdemandService.findAll();
            Integer count = productdemandService.countProduct();
            return new PageDataResult(count,productdemands,page * results);
        }else if(StringUtils.isNotBlank(vProduct.name)&&(vProduct.name.contains("%"))){
            List<Productdemand> demandlist = new ArrayList<>();
            return new PageDataResult(0,demandlist,0);
        }
        else {
            if(StringUtils.isNotBlank(vProduct.getName()))
                vProduct.setName("%"+vProduct.getName()+"%");
            List<Productdemand> productdemands = productdemandService.queryFilesByObj(vProduct);
            Integer count = productdemandService.queryFilesByObj(vProduct).size();
            return new PageDataResult(count,productdemands,page * results);
        }
    }

    /**
     * 新增生产任务
     *
     * @param productDTO
     * @return ResponseResult
     */
    @ApiOperation(value = "新增生产任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "记录id号(不传参)", paramType = "body", dataType = "Integer", required = false),
            @ApiImplicitParam(name = "name", value = "任务名称", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "demandClassify", value = "生产任务类型", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "demandyear", value = "生产数据年份", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "taskdocument", value = "生产任务附件路径", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "starttimefront", value = "前端传过来的任务开始时间", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "endtimefront", value = "前端传过来的任务结束时间", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "status", value = "状态", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "approver", value = "审批人", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "demandid", value = "对应的需求id", paramType = "body", dataType = "Integer", defaultValue = ""),
            @ApiImplicitParam(name = "creator", value = "任务创建人", paramType = "body", dataType = "String", defaultValue = ""),
            @ApiImplicitParam(name = "fronttime",value = "前端传来的时间数组",paramType = "body" ,dataType = "List",defaultValue = ""),
            @ApiImplicitParam(name="creattime",value = "创建时间",paramType = "body",dataType = "Timestamp",defaultValue = ""),
            @ApiImplicitParam(name = "approvtime",value = "时间",paramType = "body" ,dataType = "Timestamp",defaultValue = ""),
            @ApiImplicitParam(name="useid",value = "对应的用户id",paramType = "body",dataType = "Integer",defaultValue = ""),
            @ApiImplicitParam(name="flow",value = "流程编排对应的json",paramType = "body",dataType = "JsonObject",defaultValue = ""),
    })
    @PostMapping("/insertselect")
    public ResponseResult insertselect(@RequestBody ProductDTO productDTO) throws IOException{
        Productdemand productdemand = new Productdemand();
        BeanUtils.copyProperties(productDTO,productdemand);
        Integer userid = SecurityUserUtil.getUser().getId();
        List<String> regions = productDTO.getTaskdocuments();
        if(regions!=null){
            String region = String.join(",",regions);
            productdemand.setTaskdocument(region);
        }
        JSONObject json = productDTO.getFlow();
        String compaiRegion = json.toJSONString();
        if (StringUtils.isBlank(productdemand.getName())) {
            return ResponseResult.error("传入值为空！");
        }
        List<String> handletime = productdemand.getFronttime();
        String starttime1 = handletime.get(0);
        String endtime1 = handletime.get(1);
        String demandfront = productDTO.getDemanddata();
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        Date startdate = new Date();
        Date enddate = new Date();
//        Date demandy = new Date();
        try {
            startdate = format1.parse(starttime1);
            enddate = format1.parse(endtime1);
//            demandy = format1.parse(demandfront);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        String filename=uuid+".json";
        try {
            FileUtil.wirteText(json.toJSONString(),flowPath+ filename);
        }
        catch (RuntimeException ex)
        {
            ex.printStackTrace();
            return ResponseResult.error("File Write Error");
        }
        productdemand.setFlowresult(flowPath+filename);
        productdemand.setStarttime(startdate);
        productdemand.setEndtime(enddate);
//        productdemand.setDemandyear(demandy);
        String upCachePath = myPath + "_user" + userid;
       // BaseAdminUser adminUser =adminUserService.queryById(SecurityUserUtil.getUser().getId());
        String userName =SecurityUserUtil.getUser().getUsername();
        productdemand.setCreator(userName);
        String status = "未审批";
        Date d = new Date();
        String timestamp = String.valueOf(d.getTime());
        String path = myPath + "admin" + timestamp;
        productdemand.setStatus(status);
        Date creat= new Date();
        productdemand.setCreattime(creat);
        String areaname=productdemand.getTaskdocument().replaceAll(",",";");
        productdemand.setTaskdocument(areaname);
//        if (FileUtil.copyFolder(upCachePath, path)) {
//            FileUtil.deleteDir(upCachePath);
//        } else {
//            return ResponseResult.error("upload fail");
//        }
//        productdemand.setTaskdocument(path);
        if (productdemandService.insertSelective(productdemand) > 0) {
            return new ResponseResult("创建成功！");
        }
        ;
        return ResponseResult.error("wrong");
    }

    /**
     * 选择指定偏移量的生产任务
     *
     * @param pagesize
     * @param pagenum
     * @return PageDataResult
     */
    @PostMapping("/selectbylimit")
    public PageDataResult selectbylimit(@RequestParam("pageSize") Integer pagesize, @RequestParam("pageNum") Integer pagenum) {
        PageDataResult pdr = new PageDataResult();
        if (pagenum == null || pagenum == 0) {
            pagenum = 1;
        }
        if (pagesize == null || pagesize == 0)
            pagesize = 10;
        pdr.setList(productdemandService.selectByLimit((pagenum - 1) * pagesize, pagesize));
        pdr.setTotals(productdemandService.countProduct());
        return pdr;
    }

    /**
     * 选择指定索引的生产任务
     *
     * @param id
     * @return ResponseResult
     */
    @GetMapping("/selectbykey")
    public ResponseResult selsectbykey(Integer id) {
        if (id == null) {
            return ResponseResult.error("id为空！");
        }
        ResponseResult pdr = new ResponseResult();
        pdr.setObj(this.productdemandService.selectByPrimaryKey(id));
        return pdr;
    }

    /**
     * 删除指定的生产任务
     *
     * @param id
     * @return ResponseResult
     */
    @PostMapping("/deletebykey")
    public ResponseResult deletebyprimarykey(Integer id) {
        if (id == null)
            return ResponseResult.error("传入值为空！");
        productdemandService.deleteByPrimaryKey(id);
        return new ResponseResult("Success");
    }

    /**
     * 更新指定的生产任务
     *
     * @param productdemand
     * @return ResponseResult
     */
    @PostMapping("/updateselect")
    public ResponseResult updateselect(Productdemand productdemand) {
        if (productdemand == null) {
            return ResponseResult.error("传入值为空！");
        }
        productdemandService.updateByPrimaryKeySelective(productdemand);
        return new ResponseResult("Success");
    }

    /**
     * 模糊查询
     *
     *
     * @param pageNum
     * @param pageSize
     * @param keyword
     * @return PageDataResult
     */
    @PostMapping("/search")
    public PageDataResult CircuitSearch(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize, String keyword) {

        PageDataResult pdr = new PageDataResult();
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0)
            pageSize = 10;

        pdr.setTotals(productdemandService.findByKeyWord(keyword).size());
        PageHelper.startPage(pageNum, pageSize);
        pdr.setList(productdemandService.findByKeyWord(keyword));
        return pdr;
    }

    /**
     * 获取当前用户下的所有生产任务
     *
     * @return PageDataResult
     */
    @PostMapping("/getproduct")
    public PageDataResult getUserproduct() {
        //BaseAdminUser adminUser = adminUserService.queryById(SecurityUserUtil.getUser().getId());
        String name = SecurityUserUtil.getUser().getUsername();
        PageDataResult pdr = new PageDataResult();
        pdr.setTotals(productdemandService.countMineSubmit(name));
        pdr.setList(productdemandService.selectMySubmit(name));
        return pdr;
    }

    /**
     * 单个生产任务通过
     *
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/acceptproduct")
    public ResponseResult acceptDemand(Integer id, String status) {
        Productdemand productdemand = productdemandService.selectByPrimaryKey(id);
        productdemand.setStatus(status);
        productdemandService.updateByPrimaryKeySelective(productdemand);
        return new ResponseResult("done");
    }


    /**
     * 单个生产任务拒绝
     *
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/rejectproduct")
    public ResponseResult rejectDemand(Integer id, String status) {
        Productdemand productdemand = productdemandService.selectByPrimaryKey(id);
        productdemand.setStatus(status);
        productdemandService.updateByPrimaryKeySelective(productdemand);
        return new ResponseResult("done");
    }


    /**
     * 目录/文件上传
     *
     * @param file 目录，文件
     * @return 返回提交结果
     */
    @ApiOperation(value = "文件上传")
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "文件夹下的文件list", paramType = "body", dataType = "List", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "用户id", paramType = "body", dataType = "Integer"),
    })
    public ResponseResult uploadFile(@ApiParam(value = "二进制文件流") MultipartFile[] file) {
//        String filepath = "/root/upLoad";
        String upCachePath = myPath + "_user" + SecurityUserUtil.getUser().getId();
        String filepath = upCachePath;
        String result = FileUtil.uploadFile(file, filepath);
        if (!result.contains("上传失败"))
            return ResponseResult.success(result);
        return ResponseResult.error(result);

    }


    /**
     * 文件上传缓冲区清空
     *
     * @return 返回文件清空结果
     */
    @ApiOperation(value = "清空历史上传")
    @RequestMapping(value = "/clearUploadFile", method = RequestMethod.POST)
    @ApiImplicitParam(name = "userId", value = "当前用户id")
    public ResponseResult clearUploadFile() {
        String upCachePath = myPath + "_user" + SecurityUserUtil.getUser().getId();
        FileUtil.deleteDir(upCachePath);
        File file = new File(upCachePath);
        if (!file.exists()) {
            return ResponseResult.success("历史上传清空成功");
        }
        return ResponseResult.error("历史上传清空失败");
    }


    /**
     * 多个任务通过
     *
     * @return 返回执行结果
     **/
    @ApiOperation(value = "批量审批通过")
    @PostMapping("/accept")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "申请的记录id", dataType = "Map", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "当前操作的用户id", dataType = "Map", defaultValue = "")
    })
    public ResponseResult acceptMany(@RequestBody Map<String, Object> map) {
        List<Integer> ids = new ArrayList<>();
        ids = (List<Integer>) map.get("ids");
        Productdemand productdemand = new Productdemand();
        String staus = "审批通过";
        String userName = SecurityUserUtil.getUser().getUsername();
        int sum = 0;
        for (int i = 0; i < ids.size(); i++) {
            Integer id = ids.get(i);
            productdemand = productdemandService.selectByPrimaryKey(id);
            Date apptime = new Date();
            productdemand.setApprover(userName);
            productdemand.setApprovtime(apptime);
            productdemand.setStatus(staus);
            if (productdemandService.updateByPrimaryKeySelective(productdemand) > 0) {
                sum = sum + 1;
            }
        }
        if (sum == ids.size())
            return ResponseResult.success("已通过");
        return ResponseResult.error("失败");
    }


    /**
     * 多个任务拒绝
     *
     * @return 返回执行结果
     **/
    @ApiOperation(value = "批量审批拒绝")
    @PostMapping("/refuse")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "申请的记录id", dataType = "Map", defaultValue = ""),
            @ApiImplicitParam(name = "userid", value = "当前操作的用户id", dataType = "Map", defaultValue = "")
    })
    public ResponseResult rejectMany(@RequestBody Map<String, Object> map) {
        List<Integer> ids = new ArrayList<>();
        ids = (List<Integer>) map.get("ids");
        Productdemand productdemand = new Productdemand();
        String staus = "审批拒绝";
        String userName = SecurityUserUtil.getUser().getUsername();
        int sum = 0;
        for (int i = 0; i < ids.size(); i++) {
            Integer id = ids.get(i);
            productdemand = productdemandService.selectByPrimaryKey(id);
            Date apptime = new Date();
            productdemand.setApprover(userName);
            productdemand.setApprovtime(apptime);
            productdemand.setStatus(staus);
            if (productdemandService.updateByPrimaryKeySelective(productdemand) > 0) {
                sum = sum + 1;
            }
        }
        if (sum == ids.size())
            return ResponseResult.success("已拒绝！");
        return ResponseResult.error("失败");
    }

    /**
     * 下载功能
     * @return  下载后的文件名
     * **/
    @ApiOperation(value = "下载")
    @GetMapping("/download")
    @ApiImplicitParam(name = "id", value = "需求的记录id", dataType = "Integer", defaultValue = "")
    public String download(Integer id, HttpServletRequest request, HttpServletResponse response) throws Exception{
        Productdemand productdemand = productdemandService.selectByPrimaryKey(id);
        String path = productdemand.getTaskdocument();
        String zipPath =  FileUtil.downloadAllAttachment(path,request,response);
        String filename = zipPath.substring(zipPath.lastIndexOf(File.separator)+1,zipPath.length());
        FileUtil.downloadFile(response,filename,zipPath);
        FileUtil.deleteDir(zipPath);
        return filename;
    }

    /**
     *新建接口
     * @return PageDateResult
     */
    @ApiOperation(value = "封装数据")
    @PostMapping("/packagedata")
    @ApiImplicitParam(name = "data" , value = "传递的数据" ,dataType = "String" ,defaultValue = "")
    public ResponseResult packageData(String data){
        Map<String,Object> map = new HashMap<>();
        return  ResponseResult.success(map);
    }

    /**
     *新建接口
     * @return PageDateResult
     */
    @ApiOperation(value = "获取数据")
    @PostMapping("/getData")
    @ApiImplicitParam(name = "data" , value = "传递的数据" ,dataType = "String" ,defaultValue = "")
    public ResponseResult getData(@RequestBody List<String> data){
        Map<String,Object> map = new HashMap<>();
        map = productdemandService.PackageData(data);
        return  ResponseResult.success(map);
    }

    /**
     * 获取编排的Json
     * @return ResponseResult
     */
    @ApiOperation(value = "获取Json" )
    @PostMapping("/getJson")
    @ApiImplicitParam(name = "id" ,value = "传递的id" ,dataType = "Integer" ,defaultValue = "")
    public ResponseResult getJson(@RequestBody Map<String,Object> did) throws IOException{
        Integer id=(Integer)did.get("id");
        Productdemand pro = productdemandService.selectByPrimaryKey(id);
        String dir =pro.getFlowresult() ;
        String fileContent = FileUtil.readText(dir).replaceAll("=",":");
        JSONObject json = JSONObject.parseObject(fileContent);
        ResponseResult pdr = new ResponseResult();
        pdr.setObj(json);
        return pdr;
    }

    /**
     * 更新编排的Json
     * @return ResponseResult
     */
    @ApiOperation(value = "编辑Json" )
    @PostMapping("/editJson")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id" ,value = "传递的id" ,dataType = "Integer" ,defaultValue = ""),
            @ApiImplicitParam(name = "editOne" ,value = "传递的新Json" ,dataType = "JsonObjecr" ,defaultValue = "")
    })
    public ResponseResult editJson(@RequestBody EditJsonDTO editJsonDTO) throws IOException{
        Integer id = editJsonDTO.getId();
        Productdemand pro = productdemandService.selectByPrimaryKey(id);
        String dir =pro.getFlowresult() ;
        FileUtil.deleteDir(dir);
        JSONObject json =editJsonDTO.getEditOne() ;

        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        String filename=uuid+".json";
        try {
            FileUtil.wirteText(json.toJSONString(),flowPath+ filename);
        }
        catch (RuntimeException ex)
        {
            ex.printStackTrace();
            return ResponseResult.error("File Write Error");
        }
        pro.setFlowresult(flowPath+filename);
        if(productdemandService.updateByPrimaryKeySelective(pro)>0){
            return new ResponseResult("编辑更新成功！");
        }
        return ResponseResult.error("wrong");
    }
}
