/*******************************************************************************
 * @Description：
 * @Author ：肖小霞
 * @version：1.0
 * @date ： 2021/1/21 下午4:45
 ******************************************************************************/
package com.cetc32.dh.controller.rest;

import com.cetc32.dh.common.response.PageDataResult;
import com.cetc32.dh.common.response.ResponseResult;
import com.cetc32.dh.entity.*;
import com.cetc32.dh.service.*;
import com.cetc32.dh.utils.FileUtil;
import com.cetc32.webutil.common.annotations.LoginSkipped;
import com.cetc32.webutil.common.util.SecurityUserUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 数管文件、数据接口操作类
 *
 * @author: xiao
 * @version: 1.0
 * @date: 2020/10/14
 * 备注:无
 */

@RestController
@RequestMapping("/rest/import/")
public class DataFileController {

  @Autowired
  DataFileService dataFileService;
  @Autowired
  DataMenuService dataMenuService;
  @Autowired
  ZipFileService zipFileService;
  @Autowired
  DataAreaService dataAreaService;
  @Autowired
  CityGeomService cityGeomService;
  @Value("${upLoadPath}")
  String upLoadPath;
  @Value("${upDatePath}")
  String upDatePath;
  @Value("${downPath}")
  String downPath;
  @Value("${storePath}")
  String storePath;
  @Value("${server.port}")
  private String port;



  /**
   * 对已提交的文件和表单数据审批通过
   *
   * @param map
   * @return 返回执行结果
   **/
  @ApiOperation(value = "审批通过")
  @PostMapping("/accept")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "ids", value = "申请审批的记录id", dataType = "List", defaultValue = ""),
    @ApiImplicitParam(name = "userId", value = "用户id", dataType = "Integer", defaultValue = ""),
  })
  public ResponseResult gaacceptSubmit(@RequestBody Map<String, Object> map) {
    System.out.println(map);
    List<String> ids = new ArrayList<>();
    DataFile dataFile = new DataFile();
    int sum = 0;
    ids = (List<String>) map.get("ids");
    String userName = SecurityUserUtil.getUser().getUsername();
    for (int i = 0; i < ids.size(); i++) {
      Integer id = Integer.parseInt(ids.get(i));
      dataFile = dataFileService.queryById(id.longValue());
      dataFile.setStatus("审批通过");
      dataFile.setApprover(userName);
      dataFile.setApproveTime(new Date());
      if (dataFileService.updatebyId(dataFile) > 0) {
        sum++;
      }
    }
    if (sum == ids.size())
      return ResponseResult.success("已审批通过");
    return ResponseResult.error("审批失败");

  }

  /**
   * 对已提交的文件和表单数据审批拒绝
   *
   * @param map
   * @return 返回执行结果
   **/
  @ApiOperation(value = "拒绝请求")
  @PostMapping("/reject")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "ids", value = "申请审批的记录id", dataType = "List", defaultValue = ""),
    @ApiImplicitParam(name = "userId", value = "用户id", dataType = "Integer", defaultValue = ""),
  })
  public ResponseResult rejectSubmit(@RequestBody Map<String, Object> map) {
    System.out.println(map);
    List<String> ids = new ArrayList<>();
    DataFile dataFile = new DataFile();
    int sum = 0;
    ids = (List<String>) map.get("ids");
    String userName = SecurityUserUtil.getUser().getUsername();
    for (int i = 0; i < ids.size(); i++) {
      Integer id = Integer.parseInt(ids.get(i));
      dataFile = dataFileService.queryById(id.longValue());
      dataFile.setStatus("审批拒绝");
      dataFile.setApprover(userName);
      dataFile.setApproveTime(new Date());
      if (dataFileService.updatebyId(dataFile) > 0) {
        String delFile = dataFile.getFilePath();
        File file = new File(delFile);
        if (file.exists()) {
          FileUtil.deleteDir(delFile);
        }
        sum++;
      }
    }
    if (sum == ids.size())
      return ResponseResult.success("已审批拒绝");
    return ResponseResult.error("审批失败");
  }


  /**
   * 删除已提交的数据记录
   *
   * @param map
   * @return 返回执行结果
   **/
  @ApiOperation(value = "删除数据")
  @PostMapping("/delcommon")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "ids", value = "申请删除的文件id", dataType = "List", defaultValue = ""),
    @ApiImplicitParam(name = "userId", value = "用户id", dataType = "Integer", defaultValue = ""),
  })
  public ResponseResult delData(@RequestBody Map<String, Object> map) {
//    System.out.println(map);
    List<String> ids = new ArrayList<>();
    DataFile dataFile = new DataFile();
    int sum = 0;
    ids = (List<String>) map.get("ids");
//    Integer userId =  SecurityUserUtil.getUser().getId();
//    System.out.println(userId);
//        String userName = null;
//        if ((adminUserService.getUserById(userId)) != null) {
//            userName = adminUserService.getUserById(userId).getSysUserName();
//        }

    for (int i = 0; i < ids.size(); i++) {
      System.out.println(ids.get(i));
      Long id = Long.parseLong(ids.get(i));
      dataFile = dataFileService.queryById(id);
      String delFile = dataFile.getFilePath();
      File file = new File(delFile);
      if (dataFileService.deleteById(id) > 0) {
        if (file.exists()) {
          FileUtil.deleteDir(delFile);
        }
        sum++;
      }
    }
    if (sum == ids.size()) {
      return ResponseResult.success("删除成功");
    }
    return ResponseResult.error("删除失败");

  }


//    /**
//     * @param page 页码
//     * @param results 每页显示条数
//     * @return 分页数据
//     * */
//    @ApiOperation(value = "所有待审批的数据")
//    @PostMapping("/approvescommon")
//    public PageDataResult myAprroves(@ApiParam(value = "页码") Integer page, @ApiParam(value = "每页数据条数") Integer results,@ApiParam(value = "编目id") Integer menuid) {
//        if (null == page || page <= 0)
//            page = 1;
//        if (null == results || results <= 0)
//            results = 10;
//        DataFile ds = new DataFile();
//        List<DataFile> list = new ArrayList<>();
//        list = dataFileService.selectByStatusAndUser(ds);
//        return new PageDataResult(dataFileService.countByStatusAndUser(ds),
//                list, (page - 1) * results);
//    }


  /**
   * 根据目录节点id和条件查询数据记录
   *
   * @param dataFile 使用datdFile实体类接收多个参数
   * @return pdr 返回查询结果
   * 备注:无
   */
  @ApiOperation(value = "查询数据", notes = "至少传入page，和current两个参数以及menuId编目号")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "page", value = "页码", paramType = "body", dataType = "Integer", required = true, defaultValue = "1"),
    @ApiImplicitParam(name = "results", value = "每页数据条数", paramType = "body", dataType = "Integer", required = true, defaultValue = "10"),
    @ApiImplicitParam(name = "fileName", value = "文件名称", paramType = "body", defaultValue = ""),
    @ApiImplicitParam(name = "fileTime", value = "文件年份", paramType = "body", dataType = "Integer", defaultValue = ""),
    @ApiImplicitParam(name = "fileType", value = "文件类型", paramType = "body", defaultValue = ""),
    @ApiImplicitParam(name = "fileSecurity", value = "文件安全等级", paramType = "body", dataType = "Integer", defaultValue = ""),
    @ApiImplicitParam(name = "region", value = "文件区域", paramType = "body", defaultValue = ""),
    @ApiImplicitParam(name = "menuId", value = "目录节点", paramType = "body", dataType = "Integer", required = true, defaultValue = ""),
    @ApiImplicitParam(name = "status", value = "审批状态", paramType = "body", dataType = "String", defaultValue = "")
  })
  @PostMapping("/approvecommon")
  public PageDataResult queryFilesByObj(@RequestBody DataFile dataFile) {
    InetAddress address = null;//获取的是本地的IP地址 //PC-20140317PXKX/192.168.0.121
    try {
      address = InetAddress.getLocalHost();
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
    String hostAddress = address.getHostAddress();//192.168.0.121
    Integer page = dataFile.getPage();
    Integer results = dataFile.getResults();
    if (dataFile.getTimeRange() != null && dataFile.getTimeRange().length == 2) {
      dataFile.setStartTime(dataFile.getTimeRange()[0]);
      dataFile.setEndTime(dataFile.getTimeRange()[1]);
    }

    if (page == null || page <= 0) {
      page = 1;
    }
    if (results == null || results <= 0)
      results = 10;

    int offset = (page - 1) * results;

    List<DataFile> dataFileList = new ArrayList<DataFile>();
    dataFileList = dataFileService.queryFilesByObj(offset, results, dataFile);

    for (int i = 0; i < dataFileList.size(); i++) {
      if (dataFileList.get(i).getRegion() != null && !dataFileList.get(i).getRegion().trim().isEmpty()) {
        List<String> regionList = Arrays.asList(dataFileList.get(i).getRegion().split(","));
        dataFileList.get(i).setRegionList(new ArrayList<String>(regionList));
      }
      if(dataFileList.get(i).getImgExample()!=null){
        dataFileList.get(i).setImgExample("http://"+hostAddress+":"+port+"/pic"+dataFileList.get(i).getImgExample());
      }

    }
    return new PageDataResult(dataFileService.countFilesByObj(dataFile), dataFileList, offset);
  }

  /**
   * 根据目录节点id查询已通过的数据
   *
   * @param map
   * @return pdr返回查询结果
   * 备注:无
   */
  @ApiOperation(value = "根据目录节点id查询已通过的数据")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "menuId", value = "目录节点", paramType = "body", dataType = "Integer", required = true, defaultValue = ""),
  })
  @PostMapping("/queryAccepted")
  public PageDataResult queryAccepted(@RequestBody Map<String, Object> map) {
    DataFile dataFile = new DataFile();
    Integer menuId = (Integer) map.get("menuId");
    dataFile.setMenuId(menuId);
    dataFile.setStatus("审批通过");
    Integer page = dataFile.getPage();
    Integer results = dataFile.getResults();

    if (page == null || page <= 0) {
      page = 1;
    }
    if (results == null || results <= 0)
      results = 10;

    int offset = (page - 1) * results;

    List<DataFile> dataFileList = new ArrayList<DataFile>();
    dataFileList = dataFileService.queryFilesByObj(offset, results, dataFile);


    return new PageDataResult(dataFileService.countFilesByObj(dataFile), dataFileList, offset);

  }

  /**
   * 提交文件和表单数据
   *
   * @param dataFile 使用dataFile实体类接收多个参数
   * @return 返回提交结果
   */
  @ApiOperation(value = "提交数据")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "fileType", value = "文件类型", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "fileSecurity", value = "文件等级", paramType = "body", dataType = "Integer", defaultValue = ""),
    @ApiImplicitParam(name = "region", value = "区域", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "fileTime", value = "文件年份", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "fileDiscription", value = "文件描述", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "approver", value = "审批人", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "menuId", value = "编目号", paramType = "body", dataType = "Integer", defaultValue = ""),
    @ApiImplicitParam(name = "menuName", value = "编目名称", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "title", value = "文件标识", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "gcs", value = "图像地理坐标系", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "scanLevel", value = "图像级别（1-20）", paramType = "body", dataType = "Integer", defaultValue = ""),
    @ApiImplicitParam(name = "scale", value = "图像比例尺", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "lan", value = "经度（左上）", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "lon", value = "纬度（左上）", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "fileConfig", value = "文件标识别，数据标识", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "userId", value = "用户id", paramType = "body", dataType = "Integer"),
  })
  @PostMapping("/subcommon")
  public ResponseResult DataSub(@RequestBody DataFile dataFile) throws Exception {
    if(null == dataFile)
      return ResponseResult.error("无效的参数");
    //List<DataArea> dataAreas = new ArrayList<>();

    //packageDataFile(dataFile);

    String upCachePath = upLoadPath + "_user" + SecurityUserUtil.getUser().getId();
    String fileName = null;
    Integer count = 0;
    String path_move = dataMenuService.queryById(dataFile.getMenuId().longValue()).getUrl();
    FileUtil.makeDir(path_move);
    File baseFile = new File(upCachePath);
    if (baseFile == null) {
      return ResponseResult.error("未上传文件");
    }
    File[] files = baseFile.listFiles();
    Integer dataId =-1;
    for (int i = 0; i < files.length; i++) {
      dataFile.setImgExample(null);
      fileName = moveUploadedFiles(dataFile, upCachePath, path_move, files[i]);
      if (fileName == null)
        break;
      dataFile.setFileName(fileName);
      String path = path_move + File.separator + fileName;
      dataFile.setFilePath(path);
      //dataFile_new = dataFile;
      if (dataFileService.insertDataFile(dataFile) > 0) {
        System.out.println("***************************************"+dataFile.getId());

        count++;
        //data_area表建立关系
      }

      //dataFile_new2 = dataFile_new;
      /*dataFile_new2.setId(dataFile_new.getId() + i + 1);
      dataFile_new.setId(dataFile_new2.getId());*/
    }

    if (FileUtil.copyFolder(upCachePath, path_move) && count == files.length) {
      FileUtil.deleteDir(upCachePath);
      return ResponseResult.success("表单信息提交成功！");
    }

    return ResponseResult.error("表单信息提交失败！");
  }

  /************************************************************************
   * 封装前端提交的DataFile实体类型，校验RegionList中区域信息是否是通用的areaCode
   * 如果是这提取其对应CityName ,如果校验RegionList中区域信息为省市，则转化为areaCode
   * @param dataFile  需要封装的datafile数据实体
   * @return 无
   * **********************************************************************/
  /*public  void packageDataFile(DataFile dataFile) {
    List<String> rgList =dataFile.getRegionList();
    if (rgList != null && rgList.size()>0) {
      if(Pattern.matches("^\\d{6}$",  //校验6位数字编码
              rgList.get((new Random().nextInt(rgList.size()))).trim())){ //随即选取一个进行校验
        dataFile.setRegion(cityGeomService.selectCityInCodes(rgList));
      }else if(Pattern.matches("^[\\u4e00-\\u9fa5]{0,20}$",
                      rgList.get((new Random().nextInt(rgList.size()))).trim())){//校验数字编码失败，则表示可能为文字
        dataFile.setRegion(rgList.toString()
                .replace("[", "")
                .replace("]", ""));
        //根据城市名称获取城市areaCode
        dataFile.setRegionList(cityGeomService.selectCodesInArea(rgList));
      }
    }
    dataFile.setStatus("未审批");
    dataFile.setSubmitor(SecurityUserUtil.getUser().getUsername());
  }*/

  private String moveUploadedFiles( DataFile dataFile, String upCachePath, String path_move, File file) {
    String fileName;
    String oldFileName=file.getName();
    if (file.isDirectory()) {
      List<String> allFiles = FileUtil.getAllFile(upCachePath + File.separator + oldFileName);
      dataFile.setFileNumbers(allFiles.size());
      dataFile.setFileSize(FileUtils.sizeOfDirectory(file));
      fileName = FileUtil.nameAddTime(oldFileName);
      for (int j = 0; j <allFiles.size() ; j++) {
        String name = allFiles.get(j);
        if (name.contains("jpeg") || name.contains("jpg") || name.contains("png") || name.contains("gif")) {
          dataFile.setImgExample((path_move+name.replace(upCachePath,"")).replace(oldFileName,fileName));
          break;
        }
      }
      file.renameTo(new File(upCachePath + File.separator + fileName));
    } else {
      fileName = file.getName();
      dataFile.setFileSize(file.length());
      dataFile.setFileNumbers(1);
      if(fileName.contains("jpeg")||fileName.contains("jpg")||fileName.contains("png")||fileName.contains("gif")){
        dataFile.setImgExample((path_move+File.separator+fileName.replace(upCachePath,"")));
        //return null;
      }
      file.renameTo(new File(upCachePath + File.separator + fileName));
    }
    return fileName;
  }

  /**
   * 记录数据详情展示
   *
   * @param fileId   件id
   * @return 返回记录数据详情结果
   */
  @ApiOperation(value = "查看记录详情")
  @RequestMapping(value = "/comdetail", method = RequestMethod.GET)
  public ResponseResult comDetail(Long fileId) {
    DataFile dataFile = dataFileService.queryById(fileId);
    if(dataFile!=null){
      return ResponseResult.success(200,"详情",dataFile);
    }else{
      return  ResponseResult.error("未查找到相关信息，请输入正确的参数！");
    }
  }

  /**
   * 展示记录图片详情
   *
   * @param fileId   件id
   * @return 返回记录数据详情结果
   */
  @ApiOperation(value = "查看记录详情")
  @RequestMapping(value = "/imgdetail", method = RequestMethod.GET)
  public ResponseResult imgDetail(Long fileId) {

    String img_url = null;
    InetAddress address = null;//获取的是本地的IP地址 //PC-20140317PXKX/192.168.0.121
    try {
      address = InetAddress.getLocalHost();
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
    String hostAddress = address.getHostAddress();//192.168.0.121
    DataFile dataFile = dataFileService.queryById(fileId);
    if(dataFile==null){
      return  ResponseResult.error("未查找到相关信息，请输入正确的参数！");
    }else{
      String filePath = dataFile.getFilePath();
      List<String> filePathList = FileUtil.getAllFile(filePath);
      List<String> result_list = new ArrayList<>();
      for (int i = 0; i <filePathList.size() ; i++) {
        String fileName = filePathList.get(i);
        System.out.println(fileName);
        if(fileName.contains("jpg")||fileName.contains("jpeg")||fileName.contains("png")||fileName.contains("gif")){
          img_url="http://"+hostAddress+":"+port+"/pic"+fileName;
          result_list.add(img_url);
        }
      }
      return ResponseResult.success(200,"详情",result_list);

    }

  }





  /**
   * 目录/文件提交上传
   *
   * @param file   目录，文件
   * @return 返回文件提交结果
   */
  @ApiOperation(value = "提交时文件上传")
  @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
  @ApiImplicitParams({
    @ApiImplicitParam(name = "file", value = "文件夹下的文件list", paramType = "body", dataType = "List", defaultValue = ""),
    @ApiImplicitParam(name = "userId", value = "用户id", paramType = "body", dataType = "Integer"),
  })
  public ResponseResult uploadFile(@ApiParam(value = "二进制文件流") MultipartFile[] file) {
//        String filepath = "/root/upLoad";
//        userId="";
    System.out.println(file.length);
    String upCachePath = upLoadPath + "_user" +  SecurityUserUtil.getUser().getId();
    System.out.println(upCachePath);
    String filepath = upCachePath;
    String result = FileUtil.uploadFile(file, filepath);
    if (!result.contains("上传失败"))
      return ResponseResult.success(result);
    return ResponseResult.error(result);

  }

  /**
   * 文件更新上传
   *
   * @param file   目录，文件
   * @param fileId 文件id
   * @return 返回文件更新提交结果
   */
  @ApiOperation(value = "更新时文件上传")
  @RequestMapping(value = "/updateFile", method = RequestMethod.POST)
  @ApiImplicitParams({
    @ApiImplicitParam(name = "file", value = "文件夹下的文件list", paramType = "body", dataType = "List", defaultValue = ""),
    @ApiImplicitParam(name = "fileId", value = "文件id", paramType = "body", dataType = "Integer"),
  })
  public ResponseResult updateFile(@ApiParam(value = "二进制文件流") MultipartFile[] file, String fileId) {
    String upCachePath = upDatePath + "file" + fileId;
    String filepath = upCachePath;
    String result = FileUtil.uploadFile(file, filepath);
    if (!result.contains("上传失败"))
      return ResponseResult.success(result);
    return ResponseResult.error(result);
  }


  /**
   * 下载文件文件，文件夹
   *
   * @param fileIds 选择待下载文件的id
   * @return 下载结果
   */
  @ApiOperation(value = "下载文件")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "fileIds", value = "文件id", paramType = "body", dataType = "List"),
  })
  @LoginSkipped
  @GetMapping("/downloadMulFile")
  public String downloadMulFile(String[] fileIds, HttpServletResponse response) {


    if (fileIds.length == 1) {
      Long fileId = Long.parseLong(fileIds[0]);
      System.out.println("文件号！！！！" + fileId);

      String fileName = dataFileService.queryById(fileId).getFileName();
      fileName = fileName.replace("\\", File.separator);
      fileName = fileName.replace("/", File.separator);
//            String filePath = dataFileService.queryById(fileId).getFilePath() + File.separator + dataFileService.queryById(fileId).getFileName();
      String filePath = dataFileService.queryById(fileId).getFilePath();
      filePath = filePath.replace("\\", File.separator);
      filePath = filePath.replace("/", File.separator);
      //filePath = storePath + File.separator + filePath;
      if (!FileUtil.isDirectory(filePath)) {
        FileUtil.downloadFile(response, fileName, filePath);
      }
    }

    String message = null;
    String directory = downPath;
    File directoryFile = new File(directory);
    if (!directoryFile.isDirectory() && !directoryFile.exists()) {
      directoryFile.mkdirs();
    }
    //设置最终输出zip文件的目录+文件名
    String zipFileName = "已下载文件" + ".zip";
    String strZipPath = directory + File.separator + zipFileName;
    File zipFile = new File(strZipPath);
    //读取需要压缩的文件
    Long fileId = null;
    String fileName = null;

    List<String> fileNames = new ArrayList<>();
    List<String> filePaths = new ArrayList<>();
    for (int i = 0; i < fileIds.length; i++) {
      fileId = Long.parseLong(fileIds[i]);
      if (dataFileService.queryById(fileId) != null) {
        fileName = dataFileService.queryById(fileId).getFilePath();
//                fileName = dataFileService.queryById(fileId).getFilePath() + File.separator + dataFileService.queryById(fileId).getFileName();
        if (FileUtil.isDirectory(fileName)) {
          List<String> stringList = FileUtil.getAllFile(fileName);
          for (int j = 0; j < stringList.size(); j++) {
            fileNames.add(stringList.get(j));
            filePaths.add(fileName.substring(0, fileName.lastIndexOf(File.separator)));
//                        filePaths.add(dataFileService.queryById(fileId).getFilePath());
            System.out.println("path" + fileName.substring(0, fileName.lastIndexOf(File.separator)));
          }
        } else {
          fileNames.add(fileName);
          filePaths.add(fileName.substring(0, fileName.lastIndexOf(File.separator)));
          System.out.println("path" + fileName.substring(0, fileName.lastIndexOf(File.separator)));
//                    filePaths.add(dataFileService.queryById(fileId).getFilePath());
        }
      }
    }

    ZipOutputStream zipStream = null;
    FileInputStream zipSource = null;
    BufferedInputStream bufferStream = null;
    try {
      //构造最终压缩包的输出流
      zipStream = new ZipOutputStream(new FileOutputStream(zipFile));
      for (int i = 0; i < fileNames.size(); i++) {
        //解码获取真实路径与文件名
//                String realFilePath = java.net.URLDecoder.decode(fileNames.get(i), "UTF-8");
        String realFilePath = fileNames.get(i);
        System.out.println(realFilePath);
        File file = new File(realFilePath);
        //TODO:未对文件不存在时进行操作，后期优化。
        if (file.exists()) {
          zipSource = new FileInputStream(file);//将需要压缩的文件格式化为输入流
          /**
           * 压缩条目不是具体独立的文件，而是压缩包文件列表中的列表项，称为条目，就像索引一样这里的name就是文件名,
           * 文件名和之前的重复就会导致文件被覆盖
           */
//                    ZipEntry zipEntry = new ZipEntry("("+i+")"+fileNames.get(i).split("/")[fileNames.get(i).split("/").length-1]);//在压缩目录中文件的名字
          ZipEntry zipEntry = new ZipEntry(fileNames.get(i).replace(filePaths.get(i), ""));//在压缩目录中文件的名字
          zipStream.putNextEntry(zipEntry);//定位该压缩条目位置，开始写入文件到压缩包中
          bufferStream = new BufferedInputStream(zipSource, 1024 * 10);
          int read = 0;
          byte[] buf = new byte[1024 * 10];
          while ((read = bufferStream.read(buf, 0, 1024 * 10)) != -1) {
            zipStream.write(buf, 0, read);
          }
        } else {
          message = message + file.getName() + "不存在！" + "\n";
          System.out.println(file.getName() + "不存在！");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //关闭流
      try {
        if (null != bufferStream) bufferStream.close();
        if (null != zipStream) {
          zipStream.flush();
          zipStream.close();
        }
        if (null != zipSource) zipSource.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    //判断系统压缩文件是否存在：true-把该压缩文件通过流输出给客户端后删除该压缩文件  false-未处理
    if (zipFile.exists()) {
      System.out.println(zipFileName);
      System.out.println(strZipPath);
      FileUtil.downloadFile(response, zipFileName, strZipPath);
//            FileUtil.deleteDir(directory);
    }
    if (message == null) {
      message = "全部文件下载成功！";
    }
    return message;

  }

  /**
   * 断点下载文件
   *
   * @return 下载结果
   */
  @ApiOperation(value = "断点下载文件")
  @GetMapping("/breakdownFile")
  public void breakdownFile(HttpServletRequest request, HttpServletResponse response) {
    Integer zipId = Integer.parseInt(request.getParameter("id"));
    ZipFile zipFile = zipFileService.queryById(zipId);
    String filePath = zipFile.getZipPath();
    String fileName = zipFile.getZipName();
    String downloadFileName = filePath + File.separator + fileName;
    try {
      FileUtil.doBreakDown(downloadFileName, request, response);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  /**
   * 压缩文件、文件加
   *
   * @return 压缩结果
   */
  //@LoginSkipped
  @GetMapping("/zipFiles")
  public ResponseResult zipFiles(String[] fileIds, String zipName) {
    String zip_result = null;
    ZipFile zipFile = new ZipFile();
    Long fileId = null;
    String fileName = null;
    Date date = null;
    //zip文件存放路径
    String directory = downPath + File.separator + "user" +  SecurityUserUtil.getUser().getId();
    List<String> fileNames = new ArrayList<>();
    List<String> filePaths = new ArrayList<>();
    for (int i = 0; i < fileIds.length; i++) {
      fileId = Long.parseLong(fileIds[i]);
      if (dataFileService.queryById(fileId) != null) {
        fileName = dataFileService.queryById(fileId).getFilePath();
        //适应windows，linux系统
        fileName = FileUtil.nameAdapt(fileName);
        if (FileUtil.isDirectory(fileName)) {
          List<String> stringList = FileUtil.getAllFile(fileName);
          for (int j = 0; j < stringList.size(); j++) {
            fileNames.add(stringList.get(j));
            filePaths.add(fileName.substring(0, fileName.lastIndexOf(File.separator)));
          }
        } else {
          fileNames.add(fileName);
          filePaths.add(fileName.substring(0, fileName.lastIndexOf(File.separator)));
        }
      }
    }
    if (fileNames.size() == 0) {
      return ResponseResult.error("压缩前，请选择需要压缩的文件！");
    }
    //提取文件名称和文件路径，压缩文件
    zip_result = FileUtil.fileTozip(fileNames, filePaths, directory, zipName);

    if (!zip_result.contains("失败")) {
      zipFile.setUserId( SecurityUserUtil.getUser().getId());
      zipFile.setZipPath(directory);
      zipFile.setZipName(zip_result);
      date = new Date();
      zipFile.setZipDate(date);
      if (zipFileService.insertOne(zipFile) > 0) {
        return ResponseResult.success("文件压缩成功，压缩文件名称为" + zip_result);
      }
    } else {
      FileUtil.deleteDir(directory + File.separator + zip_result);
      return ResponseResult.error(zip_result);
    }
    return ResponseResult.error(zip_result);

  }


  /**
   * 清空文件提交上传缓冲区
   * @return 返回文件清空结果
   */
  @ApiOperation(value = "清空上传历史上传")
  @RequestMapping(value = "/clearUploadFile", method = RequestMethod.POST)
  @ApiImplicitParam(name = "userId", value = "当前用户id")
  public ResponseResult clearUploadFile() {
    String upCachePath = upLoadPath + "_user" +  SecurityUserUtil.getUser().getId();
    System.out.println(upCachePath + "cccccccccccccc");
    FileUtil.deleteDir(upCachePath);
    File file = new File(upCachePath);
    if (!file.exists()) {
      return ResponseResult.success("历史上传清空成功");
    }
    return ResponseResult.error("历史上传清空失败");
  }


  /**
   * 清空文件更新上传缓冲区
   *
   * @param map
   * @return 返回文件清空结果
   */
  @ApiOperation(value = "清空更新历史上传")
  @RequestMapping(value = "/clearUpdate", method = RequestMethod.POST)
  @ApiImplicitParam(name = "fileId", value = "文件id")
  public ResponseResult clearUpdateile(@RequestBody Map<String, Object> map) {
    Long fileId = Long.parseLong((String) map.get("fileId"));
    String upCachePath = upDatePath + "file" + fileId;
    System.out.println(upCachePath);
    FileUtil.deleteDir(upCachePath);
    File file = new File(upCachePath);
    if (!file.exists()) {
      return ResponseResult.success("历史上传清空成功");
    }
    return ResponseResult.error("历史上传清空失败");
  }

//    /**
//     * 显示文件夹中文件详情
//     * @return 返回文件夹中文件结果
//     * */
//    @ApiOperation(value = "显示文件夹中的文件")
//    @PostMapping("/showFiles")
//    @ApiImplicitParam(name = "fileId", value = "文件id号", paramType = "body", dataType = "String")
//    public ResponseResult showFiles(@RequestBody String fileId) {
//        System.out.println(fileId);
//        if(dataFileService.queryById(Long.parseLong(fileId))!=null){
//            String filePath = dataFileService.queryById(Long.parseLong(fileId)).getFilePath();
//            String fileName = dataFileService.queryById(Long.parseLong(fileId)).getFileName();
//            String whole_dir = filePath+File.separator+fileName;
//            List<String> stringList = FileUtil.getAllFile(whole_dir);
//            List<String> fileNames = new ArrayList<String>();
//            for(int i=0;i<stringList.size();i++){
//                fileNames.add(stringList.get(i).substring(stringList.get(i).indexOf(fileName)));
//            }
//            return ResponseResult.success(fileNames);
//        }
//        return ResponseResult.error("没有该文件夹！");
//    }


  /**
   * 更新数据信息
   *
   * @param map
   * @return 返回提交结果
   **/
  @ApiOperation(value = "更新数据")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "fileId", value = "文件file的id", paramType = "body", dataType = "Integer", required = false),
    @ApiImplicitParam(name = "fileTime", value = "拍摄时间", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "lan", value = "经度", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "lon", value = "纬度", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "gcs", value = "地理坐标系", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "scanLevel", value = "比例尺", paramType = "body", dataType = "Integer", defaultValue = ""),
    @ApiImplicitParam(name = "fileConfig", value = "数据标识", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "region", value = "区域范围", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "area", value = "范围自定义", paramType = "body", dataType = "String", defaultValue = ""),
    @ApiImplicitParam(name = "userId", value = "用户id", paramType = "body", dataType = "Integer", defaultValue = ""),

  })
  @PostMapping("/update")
  public ResponseResult DataUpdate(@RequestBody Map<String, Object> map) {
    Boolean flag = true;
    Long fileId = Long.parseLong((String) map.get("fileId"));
    String fileTime = (String) map.get("fileTime");
    String lan = (String) map.get("lan");
    String lon = (String) map.get("lon");
    String gcs = (String) map.get("gcs");
    String fileConfig = (String) map.get("fileConfig");
    String scale = (String) map.get("scale");
    String region = null;
    String new_path = null;
    if ((List<String>) map.get("regionList") != null) {
      region = ((List<String>) map.get("regionList")).toString();
      region = region.replace("[", "");
      region = region.replace("]", "");
    }
    String area = (String) map.get("area");
    Integer scanLevel = ((Integer) map.get("scanLevel"));
    Integer userId =  SecurityUserUtil.getUser().getId();
    DataFile dataFile = dataFileService.queryById(fileId);
    String old_filePath = dataFile.getFilePath();

    if (fileTime != null && StringUtils.isNotBlank(fileTime)) {
      dataFile.setFileTime(fileTime);
    }
    if (fileConfig != null && fileConfig != dataFile.getFileConfig()) {
      dataFile.setFileConfig(fileConfig);
    }

    if (area != null && area != dataFile.getArea()) {
      dataFile.setArea(area);
    }
    if (region != null && !region.equals(dataFile.getRegion())) {
      dataFile.setRegion(region);
    }

    if (lan != null && lan != dataFile.getLan()) {
      dataFile.setLan(lan);
    }
    if (lon != null && lon != dataFile.getLon()) {
      dataFile.setLon(lon);
    }
    if (gcs != null && gcs != dataFile.getGcs()) {
      dataFile.setGcs(gcs);
    }
    if (scale != null && scale != dataFile.getScale()) {
      dataFile.setScale(scale);
    }
    if (scanLevel != null && scanLevel != dataFile.getScanLevel()) {
      dataFile.setScanLevel(scanLevel);
    }
    String updateCachePath = upDatePath + "file" + fileId;
    File updateCache = new File(updateCachePath);
    if (updateCache.exists()) {
      File[] files = updateCache.listFiles();
      if (files.length > 1) {
        return ResponseResult.error("上传文件或文件夹只允许一个,上传个数超出范围，请点击清空更新缓存后再上传！");
      }
    }
    if (updateCache.exists() && flag == true) {
      if (updateCache.listFiles().length == 1) {
        FileUtil.deleteDir(dataFile.getFilePath());
        File[] files = updateCache.listFiles();
        String path_whole = dataFile.getFilePath();
        String path = path_whole.substring(0, path_whole.lastIndexOf("/"));
        if (files[0].getName().contains(".")) {
          new_path = path + File.separator + files[0].getName();
          flag = files[0].renameTo(new File(path + File.separator + files[0].getName()));
        } else {
          //文件加上时间戳
          String fileName = FileUtil.nameAddTime(files[0].getName());
          flag = files[0].renameTo(new File(updateCachePath + File.separator + fileName));
          if (flag) {
            flag = FileUtil.copyFolder(updateCachePath, old_filePath.substring(0, old_filePath.lastIndexOf("/")));
          }
        }
        if (flag) {
          if (!files[0].getName().contains(".")) {
            dataFile.setFileName(FileUtil.nameAddTime(files[0].getName()));
          } else {
            dataFile.setFileName(files[0].getName());
          }
          dataFile.setFilePath(path + File.separator + dataFile.getFileName());
          if (files[0].getName().contains(".")) {
            File f = new File(path + File.separator + files[0].getName());
            dataFile.setFileSize(f.length());
            dataFile.setFileNumbers(1);
          } else {
            dataFile.setFileSize(FileUtils.sizeOfDirectory(new File(dataFile.getFilePath())));
            dataFile.setFileNumbers(FileUtil.getAllFile(dataFile.getFilePath()).size());
          }

        }
        if(!old_filePath.equals(new_path)){
          FileUtil.deleteDir(old_filePath);
        }
        FileUtil.deleteDir(updateCachePath);
      }
    }
    if (dataFileService.updatebyId(dataFile) > 0) {
      return ResponseResult.success("信息修改成功");
    }
    return ResponseResult.error("信息修改失败");
  }


  /**
   * 统计编目menuId下的所有文件数据的大小
   *
   * @param menuId 编目id
   * @return 返回结果
   */
  @GetMapping("/size")
  public ResponseResult fileSizeCount(Integer menuId) {
    if(menuId==null){
      menuId=0;    //输入参数为空，默认为根节点0
    }
    DataMenu dataMenu = dataMenuService.queryById(menuId.longValue());
    if (dataMenu != null) {
      return ResponseResult.success(200,dataMenu.getMenuName() + "目录下的总文件大小为" + dataFileService.fileSizeCount(menuId),dataFileService.fileSizeCount(menuId));
    } else {
      return ResponseResult.error("请输入正确的编目id！");
    }
  }


  /**
   * 统计编目menuId下的所有文件个数
   *
   * @param menuId 编目id
   * @return 返回结果
   */
  @GetMapping("/filenum")
  public ResponseResult fileNum(Integer menuId) {
    if(menuId==null){
      menuId=0;    //输入参数为空，默认为根节点0
    }
    DataMenu dataMenu = dataMenuService.queryById(menuId.longValue());
    if (dataMenu != null) {
      return ResponseResult.success(200,dataMenu.getMenuName() + "目录下的总文件大小为" + dataFileService.fileNumCount(menuId),dataFileService.fileNumCount(menuId));
    } else {
      return ResponseResult.error("请输入正确的编目id！");
    }
  }

}


//    /**
//     * @param page 页码
//     * @param results 每页显示条数
//     * @return 分页数据
//     * */
//    @ApiOperation(value = "所有待审批的数据")
//    @PostMapping("/approves")
//    public PageDataResult myAprroves(@ApiParam(value = "页码") Integer page, @ApiParam(value = "每页数据条数") Integer results) {
//        if (null == page || page <= 0)
//            page = 1;
//        if (null == results || results <= 0)
//            results = 10;
//        DataSubmit ds = new DataSubmit();
//        ds.setStatus(0);
//        List<DataSubmit> list = new ArrayList<>();
//        list = dataSubmitService.selectReadyApprove(ds);
//        List<DataSubmit> list_new = new ArrayList<>();
//        for (int i = 0; i < list.size(); i++) {
//            String menuName = dataMenuService.queryById(list.get(i).getMenuid()).getMenuName();
//            list.get(i).setMenuName(menuName);
//        }
//        return new PageDataResult(dataSubmitService.countReadyApprove(ds),
//                list, (page - 1) * results);
//    }


//    /**
//     * 提交数据
//     //     * @param dataSubmit 数据信息
//     * @return 返回提交结果
//     * */
//    @ApiOperation(value = "提交数据", notes = "id不传参数")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "id", value = "记录id号(不传参)", paramType = "body", dataType = "Integer", required = false),
//            @ApiImplicitParam(name = "subtype", value = "提交类型", paramType = "body", dataType = "String", defaultValue = ""),
//            @ApiImplicitParam(name = "fileType", value = "文件类型", paramType = "body", dataType = "String", defaultValue = ""),
//            @ApiImplicitParam(name = "plevel", value = "文件等级", paramType = "body", dataType = "Integer", defaultValue = ""),
//            @ApiImplicitParam(name = "status", value = "文件状态", paramType = "body", dataType = "Integer", defaultValue = ""),
//            @ApiImplicitParam(name = "submitor", value = "提交者", paramType = "body", dataType = "String", defaultValue = ""),
//            @ApiImplicitParam(name = "approver", value = "审批人", paramType = "body", dataType = "String", defaultValue = ""),
//            @ApiImplicitParam(name = "menuid", value = "编目号", paramType = "body", dataType = "Integer", defaultValue = ""),
//            @ApiImplicitParam(name = "menuName", value = "编目名称", paramType = "body", dataType = "String", defaultValue = ""),
//            @ApiImplicitParam(name = "year", value = "年份", paramType = "body", dataType = "Integer", defaultValue = ""),
//            @ApiImplicitParam(name = "title", value = "标题", paramType = "body", dataType = "String", defaultValue = ""),
//            @ApiImplicitParam(name = "area", value = "区域", paramType = "body", dataType = "String", defaultValue = ""),
//            @ApiImplicitParam(name = "path", value = "提交路径(不传参)", paramType = "body", dataType = "String", defaultValue = ""),
//            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "body", dataType = "Integer"),
//    })
//    @PostMapping("/sub")
//    public ResponseResult DataSub(@RequestBody DataSubmit dataSubmit) {
//        String userId=dataSubmit.getUserId();
//        String upCachePath=upLoadPath+"_user"+userId;
//        System.out.println("文件类型"+dataSubmit.getFileType());
//        String fileName = null;
//        Integer count = 0;
//        dataSubmit.setSubtype("1");
//        dataSubmit.setStatus(0);
//        dataSubmit.setApprover("审批人");
//        if((adminUserService.getUserById(Integer.parseInt(userId)))!=null){
//            dataSubmit.setSubmitor(adminUserService.getUserById(Integer.parseInt(userId)).getSysUserName());
//        }
//        DataSubmit dataSubmit_new = dataSubmit;
//        DataSubmit dataSubmit_new2 = dataSubmit;
//        String path_move = dataMenuService.queryById(dataSubmit.getMenuid()).getUrl();
////        dataSubmit.setPath(path);
//        File baseFile = new File(upCachePath);
//        File[] files = baseFile.listFiles();
//        File file = null;
////        List<String> pathListNew = new ArrayList<>();
//        for (int i = 0; i < files.length; i++) {
//            file = files[i];
//            if (file.isDirectory()) {
//                dataSubmit.setFileNumbers(FileUtil.getAllFile(upCachePath +File.separator+ file.getName()).size());
//                dataSubmit.setFileSize(FileUtil.FormetFileSize(FileUtils.sizeOfDirectory(file)));
//                fileName = FileUtil.getFileName(file.getName());
//                file.renameTo(new File(upCachePath +File.separator+ fileName));
//            } else {
//                fileName = file.getName();
//                dataSubmit.setFileSize(FileUtil.FormetFileSize(file.length()));
//                dataSubmit.setFileNumbers(1);
//            }
//            dataSubmit.setTitle(fileName);
//            String path = dataMenuService.queryById(dataSubmit.getMenuid()).getUrl()+File.separator+fileName;
//            dataSubmit.setPath(path);
//            dataSubmit.setSubtime(new Date());
//            dataSubmit_new = dataSubmit;
//            if (dataSubmitService.insertDataSubmit(dataSubmit_new) >= 0) {
//                count++;
//            }
//            dataSubmit_new2 = dataSubmit_new;
//            dataSubmit_new2.setId(dataSubmit_new.getId() + i + 1);
//            dataSubmit_new.setId(dataSubmit_new2.getId());
//        }
//
//        if (FileUtil.copyFolder(upCachePath, path_move) && count == files.length) {
//            FileUtil.deleteDir(upCachePath);
//            return ResponseResult.success("表单信息提交成功！");
//        }
//
//        return ResponseResult.error("表单信息提交失败！");
//    }


//    /**
//     * 拒绝导入请求
//     * @param map
//     * @return 返回执行结果
//     * **/
//    @ApiOperation(value = "拒绝请求")
//    @PostMapping("/reject")
//    @ApiImplicitParam(name = "ids", value = "申请审批的记录id", dataType = "List", defaultValue = "")
//    public ResponseResult rejectSubmit(@RequestBody Map<String, Object> map) {
//        List<Integer> ids = new ArrayList<>();
//        ids = (List<Integer>) map.get("ids");
//        DataSubmit dataSubmit = null;
//        int sum = 0;
//        for (int i = 0; i < ids.size(); i++) {
//            Integer id = ids.get(i);
//            dataSubmit = dataSubmitService.queryById(id);
//            if (dataSubmitService.rejectSubmit(dataSubmit) > 0) {
////                String delFile = dataSubmit.getPath() + File.separator + dataSubmit.getTitle();
//                String delFile = dataSubmit.getPath() ;
//                System.out.println(delFile);
//                FileUtil.deleteDir(delFile);
//                sum = sum + 1;
//            }
//        }
//        if (sum == ids.size())
//            return ResponseResult.success("已审批拒绝");
//        return ResponseResult.error("审批失败");
//    }

//    /**
//     * 拒绝导入请求
//     * @return 返回执行结果
//     * **/
//    @ApiOperation(value = "审批通过")
//    @PostMapping("/accept")
//    @ApiImplicitParam(name = "ids", value = "申请审批的记录id", dataType = "List", defaultValue = "")
//    public ResponseResult acceptSubmit(@RequestBody Map<String, Object> map) {
//        System.out.println(map);
//        List<Integer> ids = new ArrayList<>();
//        ids = (List<Integer>) map.get("ids");
//        DataSubmit dataSubmit = null;
//        DataFile dataFile = new DataFile();
//        DataFile new_dataFile = null;
//        FileToMenu fileToMenu = new FileToMenu();
//        int sum = 0;
//        for (int i = 0; i < ids.size(); i++) {
//            Integer id = ids.get(i);
//            dataSubmit = dataSubmitService.queryById(id);
//            dataSubmit.setReviewTime(new Date());
//            System.out.println(dataSubmit);
//            dataFile.setFileName(dataSubmit.getTitle());
//            dataFile.setFilePath(dataSubmit.getPath());
//            dataFile.setRegion(dataSubmit.getArea());
////            dataFile.setFileYear(dataSubmit.getYear());
//            dataFile.setFileDiscription(dataSubmit.getFileDiscription());
//            dataFile.setFileNumbers(dataSubmit.getFileNumbers());
//            dataFile.setFileSize(dataSubmit.getFileSize());
//            dataFile.setFileSecurity(dataSubmit.getPlevel().toString());
//            dataFile.setApprover(dataSubmit.getApprover());
//            dataFile.setSubmitor(dataSubmit.getSubmitor());
//            dataFile.setFileType(dataSubmit.getFileType());
//            if (i != 0) {
//                dataFile.setId(dataFile.getId() + i);
//            }
//            int flag_file = dataFileService.insertDataFile(dataFile);
//            new_dataFile = dataFile;
//            System.out.println(new_dataFile);
//            fileToMenu.setFileId(new_dataFile.getId());
//            fileToMenu.setMenuId(dataSubmit.getMenuid());
//            int flag_filetomenu = fileToMenuService.insert(fileToMenu);
//            if (flag_file > 0 && flag_filetomenu > 0) {
//                if (dataSubmitService.acceptSubmit(dataSubmit) > 0) {
//                    sum = sum + 1;
//                }
//            }
//        }
//        if (sum == ids.size())
//            return ResponseResult.success("已审批通过");
//        return ResponseResult.error("审批失败");
//    }



