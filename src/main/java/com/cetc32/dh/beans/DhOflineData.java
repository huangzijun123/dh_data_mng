/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/6/7 下午2:30
 ******************************************************************************/
package com.cetc32.dh.beans;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class DhOflineData {
    String name;
    String directoryName;
    List<String> files;
    Long filesize;
    String versionDescription;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
        if(StringUtils.isNotBlank(directoryName)){
            name = directoryName.substring(directoryName.lastIndexOf("/"));
        }
    }

    public String getFiles() {
        if(null == files){
            return null;
        }
        return files.toString()
                .replace("[","")
                .replace("]","")
                .trim();
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public Long getFilesize() {
        return filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    public String getVersionDescription() {
        return versionDescription;
    }
    public void setFileSizes(long fileSizes){
        this.filesize =fileSizes;
    }
    public void setVersionDescription(String versionDescription) {
        this.versionDescription = versionDescription;
    }
}
