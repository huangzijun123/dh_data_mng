package com.cetc32.dh.beans;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReqSubmit {
    private String project;
    private String name;
    private String duedate;
    private String area;
    private String description;
    private String username;
    private Integer department;
    private String file;
    private String demandClassify;

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDuedate() {
        if(duedate == null)
            return null;
        SimpleDateFormat sdf =  new SimpleDateFormat( "yyyy-MM-dd" );
        try {
            return sdf.parse(duedate);
        } catch (ParseException e) {
            return null;
//            e.printStackTrace();
        }
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getDepartment() {
        return department;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getDemandClassify() {
        return demandClassify;
    }

    public void setDemandClassify(String demandClassify) {
        this.demandClassify = demandClassify;
    }

    @Override
    public String toString() {
        return "ReqSubmit{" +
                "project='" + project + '\'' +
                ", name='" + name + '\'' +
                ", duedate='" + duedate + '\'' +
                ", area='" + area + '\'' +
                ", description='" + description + '\'' +
                ", username='" + username + '\'' +
                ", department=" + department +
                ", file=" + file +
                '}';
    }
}
