package com.cetc32.dh.common.response;

import com.cetc32.dh.common.IStatusMessage;

public class ResponseData {

    private String code;
    private String message;
    private Object data;

    public ResponseData() {
        this.code = IStatusMessage.SystemStatus.SUCCESS.getCode();
        this.message = IStatusMessage.SystemStatus.SUCCESS.getMessage();
    }

    public ResponseData(IStatusMessage statusMessage){
        this.code = statusMessage.getCode();
        this.message = statusMessage.getMessage();

    }
    public ResponseData(String mg){
        this.code="200";
        this.message=mg;
    }
    public ResponseData(String mg,Object obj){
        this.code="200";
        this.data=obj;
        this.message=mg;
    }
    public ResponseData(String code ,String mg,Object obj){
        this.code=code;
        this.data=obj;
        this.message=mg;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object obj) {
        this.data = obj;
    }

    public static ResponseData error(){
        return new ResponseData("-1","error",null);
    }
    public static ResponseData error(String msg){
        return new ResponseData("-1",msg,null);
    }
    public static ResponseData success(){
        return new ResponseData("success");
    }
    public static ResponseData success(int code,String message,Object data){
        return new ResponseData(code+"",message,data);
    }
    public static ResponseData success(String msg){
        return new ResponseData(msg);
    }
    public static ResponseData success(Object obj){
        return new ResponseData("success",obj);
    }

    public static ResponseData fail(String msg) {
        return fail(400, msg, null);
    }

    public static ResponseData fail(String msg, Object data) {
        return fail(400, msg, data);
    }

    /**
     * 指定缓存失效时间
     * @param key 键
     * @param time 时间(秒)
     * @return
     * 修改信息：
     */
    public static ResponseData fail(int code, String msg, Object data) {
        ResponseData r = new ResponseData();
        r.setCode(code+"");
        r.setMessage(msg);
        r.setData(data);
        return r;
    }


    @Override
    public String toString() {
        return "ResponseData{" + "code='" + code + '\'' + ", message='"
                + message + '\'' + ", data=" + data + '}';
    }
}

