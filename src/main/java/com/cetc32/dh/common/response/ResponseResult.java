/**
 * @Title: ResponseResult
 * @Description: 前端请求响应结果,code:编码,message:描述,obj对象，可以是单个数据对象，数据列表或者PageInfo
 * @author: youqing
 * @version: 1.0
 * @date: 2020/9/11 10:55
 * 更改描述：
 */

package com.cetc32.dh.common.response;


import com.cetc32.dh.common.IStatusMessage;
import java.io.Serializable;
/**
 * Token检查类
 * @Title: FormFilter
 * @version: 1.0
 * @date: 2020/9/11 10:55
 **/
public class ResponseResult implements Serializable{

    private String code;
    private String message;
    private Object obj;

    public ResponseResult() {
        this.code = IStatusMessage.SystemStatus.SUCCESS.getCode();
        this.message = IStatusMessage.SystemStatus.SUCCESS.getMessage();
    }

    public ResponseResult(IStatusMessage statusMessage){
        this.code = statusMessage.getCode();
        this.message = statusMessage.getMessage();

    }
    public ResponseResult(String mg){
        this.code="200";
        this.message=mg;
    }
    public ResponseResult(String mg,Object obj){
        this.code="200";
        this.obj=obj;
        this.message=mg;
    }
    public ResponseResult(String code ,String mg,Object obj){
        this.code=code;
        this.obj=obj;
        this.message=mg;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public static ResponseResult error(){
        return new ResponseResult("-1","错误",null);
    }
    public static ResponseResult error(String msg){
        return new ResponseResult("-1",msg,null);
    }
    public static ResponseResult success(){
        return new ResponseResult("成功");
    }
    public static ResponseResult success(int code,String message,Object data){
        return new ResponseResult(code+"",message,data);
    }
    public static ResponseResult success(String msg){
        return new ResponseResult(msg);
    }
    public static ResponseResult success(Object obj){
        return new ResponseResult("成功",obj);
    }

    public static ResponseResult fail(String msg) {
        return fail(400, msg, null);
    }

    public static ResponseResult fail(String msg, Object data) {
        return fail(400, msg, data);
    }

    /**
     * 指定缓存失效时间
     * @param key 键
     * @param time 时间(秒)
     * @return
     * 修改信息：
     */
    public static ResponseResult fail(int code, String msg, Object data) {
        ResponseResult r = new ResponseResult();
        r.setCode(code+"");
        r.setMessage(msg);
        r.setObj(data);
        return r;
    }

    /**
     * 指定缓存失效时间
     * @param key 键
     * @param time 时间(秒)
     * @return
     * 修改信息：
     */
    @Override public String toString() {
        return "ResponseResult{" + "code='" + code + '\'' + ", message='"
                + message + '\'' + ", obj=" + obj + '}';
    }

}
