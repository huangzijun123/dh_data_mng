/**
 * @Title: PageDataResult
 * @Description: 封装DTO分页数据（记录数和所有记录）
 * @author: youqing
 * @version: 1.0
 * @date: 2020/9/11 10:55
 * 更改描述：2020/10/16 修改List 内类为NumberS子类
 */

package com.cetc32.dh.common.response;

import com.cetc32.dh.entity.NumberS;

import java.util.List;

/**
 * Token检查类
 * @Title: FormFilter
 * @version: 1.0
 * @date: 2020/9/11 10:55
 */
public class PageDataResult {

    private Integer code=200;

    //总记录数量
    private Long totals;

    private List<? extends NumberS> list;

    public List<?> getList_new() {
        return list_new;
    }

    public void setList_new(List<?> list_new) {
        this.list_new = list_new;
    }

    private List<?> list_new;

    public PageDataResult(long totals,List<? extends NumberS> list){
        this.totals=totals;
        this.list=list;
    }

    public PageDataResult(List<?> list_new,long totals,int code){
        this.code=code;
        this.totals=totals;
        this.list_new=list_new;
    }

    public PageDataResult(long totals,List<? extends NumberS> list,Integer offset){
        this.totals=totals;
        this.list=list;
        for(int i=0;i<this.list.size();i++){
            this.list.get(i).setNumberId(offset +i +1);
        }
    }
    public static PageDataResult success(int totals,List<?extends NumberS>list,Integer offset){
        return new PageDataResult(totals,list,offset);
    }
    public PageDataResult() {

    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Long getTotals() {
        return totals;
    }

    public void setTotals(Long totals) {
        this.totals = totals;
    }
    public void setTotals(Integer totals) {
        this.totals = Long.valueOf(totals);
    }

    public List <?> getList() {
        return list;
    }

    public void setList(List <? extends NumberS> list) {
        this.list = list;
    }

    /**
     * 指定缓存失效时间
     * @return
     * 修改信息：
     */
    @Override
    public String toString() {
        return "PageDataResult{" +
                "code=" + code +
                ", totals=" + totals +
                ", list=" + list +
                '}';
    }
}
