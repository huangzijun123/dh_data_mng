package com.cetc32.dh.common.utils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Comparator;

public class Tools {
    /**
     * 去除字符串首尾两端指定的字符
     * */
    public static String trimBothEndsChars(String srcStr, String splitter) {
        String regex = "^" + splitter + "*|" + splitter + "*$";
        return srcStr.replaceAll(regex, "");
    }

    /**
     * 带有","分割的字符串转化成以","分割的整形数组
     * @param str
     * @return
     */
    public static List<Integer> str2intList(String str)
    {
        if(str==null || str.isEmpty())
        {
            return new ArrayList<>();
        }
        String s= trimBothEndsChars(str,",");
        return Stream.of(s.split(",")).map(Integer::parseInt).collect(Collectors.toList());
    }
    /**
     * 带有","分割的字符串转化成以","分割的字符串数组
     * @param str
     * @return
     */
    public static List<String> str2StringList(String str)
    {
        if(str==null || str.isEmpty())
        {
            return new ArrayList<>();
        }
        String s= trimBothEndsChars(str,",");
        return Stream.of(s.split(",")).collect(Collectors.toList());
    }

    /**
     * 判断两个列表是否相等
     * @param list1
     * @param list2
     * @param <E>
     * @return
     */
    public static <E>boolean isListEqual(List<E> list1, List<E> list2) {
        // 两个list引用相同（包括两者都为空指针的情况）
        if (list1 == list2) {
            return true;
        }

        // 两个list都为空（包括空指针、元素个数为0）
        if ((list1 == null && list2 != null && list2.size() == 0)
                || (list2 == null && list1 != null && list1.size() == 0)) {
            return true;
        }

        // 两个list元素个数不相同
        if (list1.size() != list2.size()) {
            return false;
        }

        // 两个list元素个数已经相同，再比较两者内容
        // 采用这种可以忽略list中的元素的顺序
        // 涉及到对象的比较是否相同时，确保实现了equals()方法
        if (!list1.containsAll(list2)) {
            return false;
        }

        return true;
    }

    /**
     * 判断字符串是否包含中文字符
     * @param str
     * @return
     */
    public static boolean isContainChinese(String str) {
        if(str==null || str.isEmpty())
        {
            return false;
        }
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        return m.find();
    }

//    public static List ObjectToList(Object object,Class tClass)
//    {
//        List rObject=new ArrayList<>();
//        if(object.getClass()==List.class || object.getClass()== ArrayList.class)
//        {
//            rObject = (List<?>)object;
//        }
//        else if(object.getClass()==String.class)
//        {
//            rObject = new ArrayList<String>();
//            if(tClass.getName().equals(String.class.getName()))
//            {
//                rObject.add(object);
//            }
//
//            if(isNumber((String) object))
//            {
//
//
//            }
//        }
//        else if(object.getClass()==Integer.class)
//        {
//            this.role = new ArrayList<>();
//            this.role.add(Integer.parseInt((String) role));
//        }
//        else
//        {
//            this.role=new ArrayList<>();
//        }
//    }

    /**
     * 判断字符是否为纯数字
     * @param num
     * @return
     */
    public static boolean isNumber(String num)
    {
        if(num==null || num.isEmpty())
        {
            return false;
        }
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(num).matches();
    }

    /**
     * 去重排序(整形 字符串)
     * @param originalList
     * @param tClass
     * @param <T>
     * @return
     */
    public static <T>List<T> SortAndRemoveRepeat(List originalList,Class<T> tClass)
    {
        Collections.sort(originalList);
        Set set = new HashSet<T>(originalList);
        return new ArrayList<T>(set);
    }

    /**
     * 从map中获取Integer类型数据
     * @param map
     * @param mapName
     * @return
     */
    public static Integer getMapValueInt(Map<String,Object> map, String mapName)
    {
        Object obj= map.getOrDefault(mapName,null);
        if(obj==null)
        {
            return null;
        }
        if(obj.getClass()==String.class)
        {
            return Integer.parseInt((String) obj);
        }
        return (Integer) obj;
    }

    /**
     * 从map中获取 String 类型数据
     * @param map
     * @param mapName
     * @return
     */
    public static String getMapValueStr(Map<String,Object> map, String mapName)
    {
        Object obj= map.getOrDefault(mapName,null);
        if(obj==null)
        {
            return null;
        }
        if(obj.getClass()==Integer.class)
        {
            return String.valueOf(obj);
        }
        return (String) obj;
    }

    /**
     * 从map中获取 boolean 类型数据
     * @param map
     * @param mapName
     * @return
     */
    public static boolean getMapValueBool(Map<String,Object> map, String mapName)
    {
        Object obj= map.getOrDefault(mapName,null);
        if(obj==null)
        {
            return false;
        }
        if(obj.getClass()==Integer.class)
        {
            return (Integer) obj != 0;
        }
        if(obj.getClass()==String.class)
        {
            if(((String)obj).equalsIgnoreCase("true"))
            {
                return true;
            }
            else if(((String)obj).equalsIgnoreCase("false"))
            {
                return false;
            }
            else if(((String)obj).isEmpty())
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        if(obj.getClass()==Boolean.class)
        {
            return (boolean) obj;
        }
        return false;

    }
}

