/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/9/30 下午12:00
 ******************************************************************************/
package com.cetc32.dh.common.bean;

public interface TreeNode {
    String getKey();
    String getValue();
    String getTitle();
    String getParentKey();
}
