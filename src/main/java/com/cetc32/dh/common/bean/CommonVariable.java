/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/9/30 下午1:20
 ******************************************************************************/
package com.cetc32.dh.common.bean;

import com.google.common.collect.Sets;

import java.util.Arrays;
import java.util.HashSet;

public  final  class CommonVariable {
    public static final String EAST_ZQ_CODE="01";
    public static final String WEST_ZQ_CODE="02";
    public static final String SOUTH_ZQ_CODE="03";
    public static final String NORTH_ZQ_CODE="04";
    public static final String CENTRAL_ZQ_CODE="05";
    public static final String EAST_ZQ_NAME="EastZQ";
    public static final String WEST_ZQ_NAME="WestZQ";
    public static final String SOUTH_ZQ_NAME="SouthZQ";
    public static final String NORTH_ZQ_NAME="NorthZQ";
    public static final String CENTRAL_ZQ_NAME="CentralZQ";
    public static final HashSet<String> sets = new HashSet<>( Arrays.asList(new String[]{EAST_ZQ_CODE, WEST_ZQ_CODE, NORTH_ZQ_CODE, SOUTH_ZQ_CODE, CENTRAL_ZQ_CODE}));
}
