/*******************************************************************************
 * Copyright（C） CETC-32
 * @Description：
 * @Author ：徐文远
 * @version：1.0
 * @date ： 2021/9/30 下午12:01
 ******************************************************************************/
package com.cetc32.dh.common.bean;

import com.cetc32.webutil.common.bean.LoginUser;

import java.util.List;
import java.util.Random;

public class UserAreaPermision{
    List<String> polygon;
    List<String>areaCode;
    public UserAreaPermision(){}

    /*public UserAreaPermision(LoginUser user){
        setAreaCode(user.getAreacode());
    }*/

    public List<String> getPolygon() {
        return polygon;
    }

    public void setPolygon(List<String> polygon) {
        this.polygon = polygon;
    }

    public List<String> getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(List<String> areaCode) {
        if(areaCode!=null && areaCode.size()>0) {
            if (!CommonVariable.sets.contains(areaCode.get(new Random().nextInt(areaCode.size())))) {
                this.areaCode =areaCode;
            }
        }
    }
}
